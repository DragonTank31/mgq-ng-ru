screen cg_gm():
    on "show" action EndReplay(confirm=False)

init python:
    def get_hero_img():
        if player == 0:
            if store.skillset == 1:
                hero_img = ("luka st02", xy(130, 25))
            elif store.skillset == 2:
                hero_img = ("luka st03", xy(20, 25))
        elif player == 1:
            hero_img = ("alice st01", xy(250, 25))
        elif player == 2:
            hero_img = ("alma_elma st21", xy(100, 0))
        elif player == 3:
            hero_img = ("erubetie st01", xy(325, 0))
        elif player == 4:
            hero_img = ("tamamo st04", xy(100, 0))
        elif player == 5:
            hero_img = ("granberia st01", xy(100, 0))

        return hero_img

    def get_stats():

        if player == 0 or gm_char == 0:
            nm = "Лука"
            stats = (
                mylv,
                max_mylife,
                max_mp,
                mylv*5,
                5 if item01 < 4 and ngitem != 1 else 25
                )
        elif player == 1 or gm_char == 1:
            nm = "Алиса"
            stats = (145, 32000, 28, 390, 350)
        elif player == 2 or gm_char == 2:
            nm = "Альма Эльма"
            stats = (125, 15000, 13, 320, 300)
        elif player == 3 or gm_char == 3:
            nm = "Эрубети"
            stats = (125, 22000, 10, 340, 380)
        elif player == 4 or gm_char == 4:
            if word_dispel < 1:
                nm = "Тамамо"
                stats = (128, 22000, 15, 350, 340)
            else:
                nm = "Тамамо"
                stats = (300, 68000, 32, 900, 680)
        elif player == 5 or gm_char == 5:
            nm = "Гранберия"
            stats = (126, 24000, 13, 370, 340)
        elif player == 6 or gm_char == 6:
            nm = "Ипея"
            stats = (6, 150, 2, 15, 14)
        elif player == 7 or gm_char == 7:
            nm = "Ниби"
            stats = (12, 350, 3, 32, 31)

        return nm, stats


    def get_equip():

        if player == 0 or gm_char == 0:
            equip = (
                "Оружие: Железный меч" if store.item01 < 5 and store.ngitem != 1 or luka_weapon == 0 else "Оружие: Сияние Ангела",
                "Тело: Тканевая рубашка" if store.item01 < 4 and store.ngitem != 1 else "Тело: Энрикийская рубашка",
                "Прочее: Пусто" if store.skill01 > 18 and store.skillset != 1 else "Прочее: Памятное кольцо"
                )
        elif player == 1 or gm_char == 1:
            equip = (
                "Оружие: Гекатонхейры",
                "Тело: Накидка для Ламий"
                )
        elif player == 2 or gm_char == 2:
            equip = (
                "Оружие: Аморальная Слава",
                "Тело: Наряд Лилит"
                )
        elif player == 3 or gm_char == 3:
            equip = (
                "Оружие: Нет",
                "Тело: Нет"
                )
        elif player == 4 or gm_char == 4:
            equip = (
                "Оружие: Веер Аматэрасу",
                "Тело: Кимоно Кьюби"
                )
        elif player == 5 or gm_char == 5:
            equip = (
                "Оружие: Арес",
                "Тело: Превос. драконья броня"
                )
        elif player == 6 or gm_char == 6:
            equip = (
                "Оружие: Нет",
                "Тело: Ленты"
                )
        elif player == 7 or gm_char == 7:
            equip = (
                "Оружие: Ниндзято",
                "Тело: Розовый фурисодэ"
                )

        return equip


screen custom_gm():
    $ gm, stats = get_stats()
    $ equip = get_equip()

    tag menu

    add renpy.get_screen("bg", layer='master')
    add "gm_bg"
    add "gm char"

    style_group "gm"

    frame:
        pos (10, 20)
        ypadding 5

        vbox:
            spacing 5

            textbutton "Предметы" action If(not _in_replay, ShowMenu("menu_skit", var="item"))
            if gm_char == 7:
                textbutton "Навыки" action None
            else:
                textbutton "Навыки" action If(not _in_replay, ShowMenu("menu_skit", var="skill"))
            textbutton "Дневник" action If(not _in_replay, ShowMenu("record"))
            textbutton "Монстропедия" action If(_in_replay, true=EndReplay(confirm=False), false=ShowMenu("pedia_%s" % persistent.pedia_view, enter="gm"))
            textbutton "Журнал" action ShowMenu("history", enter="gm")
            textbutton "Настройки" action ShowMenu("prefs", enter="gm")
            textbutton "Доп. настройки" action ShowMenu("special_config", enter="gm")
            textbutton "Сохранить" action If(saveback >0, ShowMenu("save", enter="gm"))
            textbutton "Загрузить" action ShowMenu("load", enter="gm")
            textbutton "В главное меню" action [SetField(persistent, "show_extras_menu", False), MainMenu()]
            textbutton "Выйти из игры" action Quit()
            textbutton "\nВерсия перевода: [config.version]" action ShowMenu("pearl", enter="gm") style "hidden_button"

    frame:
        align (.5, 1.0)
        yoffset -20
        xoffset -50
        xpadding 10

        has vbox
        text "[gm!t]" xalign (.5) size 25

        grid 2 5:
            transpose True

            text "Уровень:"
            text "Макс. HP:"
            text "Макс. SP:"
            text "Атака:"
            text "Защита:"

            for t in stats:
                text "[t]" xalign 1.0

    frame:
        align (1.0, 1.0)
        xoffset -20
        yoffset -100
        xpadding 10
        xminimum 250

        vbox:
            text "Экипировка{#gm}" xalign (.5) size 25
            for e in equip:
                text e

    if part3 == 1:
        frame:
            align (1.0, 1.0)
            xoffset -20
            yoffset -250
            xpadding 10
            
            vbox:
                imagebutton:
                    yalign 0.5
                    xalign 0
                    auto "left_%s"
                    at xyzoom(1)

                    action If(gm_char > 0, true=[SetVariable('gm_char', gm_char-1), Function(gm_party_prev), SetVariable('player', -1)], false=None)

                imagebutton:
                    yalign 0.5
                    xalign 1
                    auto "right_%s"
                    at xyzoom(1)

                    action If(gm_char < 7, true=[SetVariable('gm_char', gm_char+1), Function(gm_party_next), SetVariable('player', -1)], false=None)

    
    use return_but(" ")


style gm_frame:
    is frame
    background Frame('images/System/win_bg_0.webp', Borders(10, 10, 10, 10))

style gm_button:
    is button
    background None
    xalign .5
    ypadding 0
    ymargin 0

style gm_button_text:
    is default
    xalign .5
    size 25
    line_spacing -2
    line_leading -2
    outlines [(2, '#000c')]
    insensitive_outlines [(2, '#8888')]
    insensitive_color '#808080'
    font "fonts/archiform.otf"
    color "#fff000cc"
    hover_color "#000fffcc"

style hidden_button:
    is button
    background None
    ypadding 0
    ymargin 0

style hidden_button_text:
    is default
    size 18
    line_spacing -2
    line_leading -2
    outlines [(2, '#000c')]
    insensitive_outlines [(2, '#8888')]
    insensitive_color '#808080'
    font "fonts/archiform.otf"
    color "#fffacd"
    hover_color "#fffacd"

style gm_text:
    is default
    size 22
    outlines [(2, '#000c')]
    font "fonts/archiform.otf"
    color "#fffacd"


##############################################################################
# Save, Load
#
# Screens that allow the user to save and load the game.
screen save(enter='gm'):
    default page = persistent.save_page

    tag menu

    use file_slots(page)
    use title("Сохранить")
    use pages(page, 50)
    use return_but(enter)


screen load(enter='gm'):
    default page = persistent.save_page

    tag menu

    use file_slots(page)
    use title("Загрузить")
    use pages(page, 50)
    use return_but(enter)


screen file_slots(page):

    if enter == 'mm':
        if persistent.game_clear == 3:
            add "bg 140"
        else:
            add "bg 001"
    else:
        add renpy.get_screen("bg", layer='master')

    add Solid("#000a", ysize=500) yalign .5
    add "gm_bg"

    frame:
        style_group "slots"

        grid 3 2:

            for i in xrange(1, 7):
                $ slot = i + 6*(page-1)
                $ slot_img = FileScreenshot(slot)
                python:
                    if FileLoadable(slot, page=None):
                        surf = im.cache.get(slot_img)
                        x, y = surf.get_size()

                        if (x, y) != (217, 158):
                            slot_img = At(slot_img, xyzoom(1.97))

                button:
                    style 'slot'

                    action [SetField(persistent, 'save_page', page), FileAction(slot)]

                    text "[slot]" style "slot_time_text" size 17 align (.5, 0)

                    add LiveComposite((219, 160),
                        (0, 0), Solid("#fffc"),
                        (0, 0), "NGDATA/images/system/saveload_dummy.webp",
                        (1, 1), slot_img):
                        align (.5, .5)

                    # text FileTime(slot, format=_("{#file_time}%A, %d %B %Y, %H:%M"), empty=_("empty slot")):
                    text FileTime(slot, format=_("{#file_time}%H:%M %A,\n %d %B %Y"), empty=_("Empty Slot")):
                        style "slot_time_text"

                    text FileSaveName(slot):
                        style "slot_name_text"

                    key "save_delete" action FileDelete(slot)

style slots_frame:
    background None
    xfill True
    yfill True
    margin (40, 80)
    padding (0, 0)

style slots_grid:
    spacing 15
    xfill True
    yfill True

style slot:
    background Frame('slot_bg', Borders(6, 6, 6, 6))
    hover_background Frame('slot_bg_hover', Borders(6, 6, 6, 6))
    padding (5, 5)

style slot_time_text:
    is text
    size 15
    font "fonts/archiform.otf"
    color "#fff"
    hover_color "#f00"
    outlines [(1, "#000e")]
    hover_outlines [(1, "#000e")]
    align (.5, 1.0)

style slot_name_text:
    is text
    size 15
    font "fonts/archiform.otf"
    color "#fff"
    hover_color "#f00"
    outlines [(1, "#000e")]
    align (.5, 1.0)



##############################################################################
# Preferences
#
# Screen that allows the user to change the preferences.
# http://www.renpy.org/doc/html/screen_special.html#prefereces
screen prefs(enter):
    tag menu

    if enter == 'mm':
        add "bg 001"
    else:
        add renpy.get_screen("bg", layer='master')

    add "gm_bg"

    # Put the navigation columns in a two-wide grid.
    frame:
        background None
        xfill True
        yfill True
        margin (30, 50)

        grid 2 1:
            style_group "prefs"

            # The left column.
            frame:
                has vbox
                style_group "pref"

                frame:
                    has vbox

                    label "Режим экрана"
                    grid 2 1:
                        xalign .5
                        textbutton "Оконный":
                            xalign .5
                            action Preference("display", "window")
                        textbutton "Полноэкранный":
                            xalign .5
                            action Preference("display", "fullscreen")

                frame:
                    has vbox
                    label "Эффекты"

                    grid 2 1:
                        xalign .5
                        textbutton "Включить":
                            xalign .5
                            action Preference("transitions", "all")
                        textbutton "Отключить":
                            xalign .5
                            action Preference("transitions", "none")

                frame:
                    has vbox

                    label "Пропуск текста"
                    grid 2 1:
                        xalign .5

                        textbutton "Уже увиденного":
                            xalign .5
                            action Preference("skip", "seen")
                        textbutton "Всего":
                            xalign .5
                            action Preference("skip", "all")

                frame:
                    has vbox

                    textbutton "Настройка стиля" action ShowMenu('c_styles', enter)

                frame:
                    has vbox

                    textbutton "Джойстик" action Preference("joystick")

            frame:
                has vbox
                style_group "pref"

                frame:
                    has vbox

                    label "Скорость текста"
                    bar value Preference("text speed") style "pref_scrollbar"

                frame:
                    has vbox

                    label "Авточтение"
                    bar value Preference("auto-forward time") style "pref_scrollbar"

                    if config.has_voice:
                        textbutton "Ждать голос" action Preference("wait for voice", "toggle")


                frame:
                    has vbox

                    label "Громкость музыки"
                    bar value Preference("music volume") style "pref_scrollbar"

                frame:
                    has vbox

                    label "Громкость звуков"
                    bar value Preference("sound volume") style "pref_scrollbar"

                frame:
                    has vbox

                    label "Громкость голоса"
                    bar value Preference("voice volume") style "pref_scrollbar"


    use title("Настройки")
    use return_but(enter)

##############################################################################
# Preferences Android
#
# Screen that allows the user to change the preferences.
# http://www.renpy.org/doc/html/screen_special.html#prefereces
screen prefs(enter):
    tag menu
    variant "touch"

    if enter == 'mm':
        add "bg 001"
    else:
        add renpy.get_screen("bg", layer='master')

    add "gm_bg"

    # Include the navigation.

    # Put the navigation columns in a two-wide grid.
    frame:
        background Frame("images/System/win_bg_0.webp", Borders(5, 5, 5, 5))
        xfill True
        yfill True
        margin (50, 60)
        style_group "prefs"

        vpgrid:
            xfill True
            rows 11
            scrollbars "vertical"
            draggable True
            mousewheel True
            spacing 5
            style_group "pref"

            frame:
                has hbox
                label "Эффекты"

                grid 2 1:
                    textbutton "Включить":
                        action Preference("transitions", "all")
                    textbutton "Отключить":
                        action Preference("transitions", "none")

            frame:
                has hbox

                label "Пропуск текста"
                grid 2 1:

                    textbutton "Уже увиденного":
                        action Preference("skip", "seen")
                    textbutton "Всего":
                        action Preference("skip", "all")

            frame:
                has hbox

                textbutton "Настройка стиля" action ShowMenu('c_styles', enter) xalign 0.1

            frame:
                has hbox

                textbutton "Джойстик..." action Preference("joystick") xalign 0.1


            frame:
                has hbox

                label "Скорость текста"
                bar value Preference("text speed") style "pref_scrollbar"

            frame:
                has hbox

                label "Скорость авточтения"
                bar value Preference("auto-forward time") style "pref_scrollbar"

            frame:
                has hbox

                if config.has_voice:
                    textbutton "Ждать голос" action Preference("wait for voice", "toggle") xalign 0.1

            frame:
                has hbox

                label "Громкость музыки"
                bar value Preference("music volume") style "pref_scrollbar"

            frame:
                has hbox

                label "Громкость звука"
                bar value Preference("sound volume") style "pref_scrollbar"

            frame:
                has hbox

                label "Громкость голоса"
                bar value Preference("voice volume") style "pref_scrollbar"

    use title("Preferences")
    use return_but(enter)


style prefs_frame:
    is default
    background Frame("images/System/win_bg_0.webp", 5, 5)
    xfill True
    yfill True
    padding (10, 10)

style prefs_grid:
    xfill True
    spacing 5

style pref_grid:
    xfill True
    xalign .5

style pref_grid:
    variant "touch"
    xsize 400
    xalign 1.0

style pref_frame:
    background None
    xfill True
    margin (0, 0)

style pref_frame:
    variant "touch"
    background None
    xfill True
    padding (10, 10)

style pref_label:
    variant "touch"
    xalign 0

style pref_label_text:
    color '#ffcc33'
    outlines [(2, "#000c")]
    font "fonts/archiform.otf"
    size 24

style pref_label:
    variant "touch"
    size 32

style pref_hbox:
    variant "touch"
    xfill True

style pref_button:
    xpadding 5
    xmargin 0
    background None

style pref_button:
    variant "touch"
    xalign .5
    size_group "pref"

style pref_button_text:
    outlines [(2, "#000c")]
    selected_color "#3ccd7a"
    hover_color "#000fff"
    font "fonts/archiform.otf"
    size 18
    insensitive_color "#8888"
    insensitive_outlines [(2, "#303030")]

style pref_button_text:
    variant "touch"
    xalign .5

style pref_scrollbar is scrollbar:
    base_bar Frame("images/System/win_bg_0.webp", Borders(5, 5, 5, 5))
    thumb LiveComposite((25, 12),
        (0, 1), Solid("#fff", xysize=(25, 10)))
    top_gutter 3
    bottom_gutter 3
    left_gutter 3
    right_gutter 3
    thumb_offset 0
    # xmaximum 300
    xfill True
    xalign .5

style pref_scrollbar:
    variant "touch"
    thumb LiveComposite((25, 14),
        (0, 1), Solid("#fff", xysize=(25, 13)))
    align (1.0, .5)
    xsize 400
    ysize 15

style pref_vscrollbar is vscrollbar:
    base_bar Frame("images/System/win_bg_0.webp", Borders(5, 5, 5, 5))
    bar_vertical True
    thumb LiveComposite((12, 25),
        (1, 0), Solid("#fff", xysize=(10, 25)))
    yfill True


###################################################################################
screen record(enter="gm"):
    default page = 1

    python:
        if persistent.count_zukan1 != 1 and persistent.defeat["slime"] > 0 and persistent.defeat["slug"] > 0 and persistent.defeat["mdg"] > 0 and persistent.defeat["granberia1"] > 0 and persistent.defeat["mimizu"] > 0 and persistent.defeat["gob"] > 0 and persistent.defeat["pramia"] > 0 and persistent.defeat["vgirl"] > 0 and persistent.defeat["dragonp"] > 0 and persistent.defeat["mitubati"] > 0 and persistent.defeat["hapy_a"] > 0 and persistent.defeat["hapy_bc"] > 0 and persistent.defeat["queenharpy"] > 0 and persistent.defeat["delf_a"] > 0 and persistent.defeat["delf_b"] > 0 and persistent.defeat["hiru"] > 0 and persistent.defeat["rahure"] > 0 and persistent.defeat["ropa"] > 0 and persistent.defeat["youko"] > 0 and persistent.defeat["meda"] > 0 and persistent.defeat["kumo"] > 0 and persistent.defeat["mimic"] > 0 and persistent.defeat["nanabi"] > 0 and persistent.defeat["tamamo1"] > 0 and persistent.defeat["alma_elma1"] > 0 and persistent.defeat["namako"] > 0 and persistent.defeat["kai"] > 0 and persistent.defeat["lamia"] > 0 and persistent.defeat["granberia2"] > 0 and persistent.defeat["page17"] > 0 and persistent.defeat["page257"] > 0 and persistent.defeat["page65537"] > 0 and persistent.defeat["kani"] > 0 and persistent.defeat["kurage"] > 0 and persistent.defeat["iso"] > 0 and persistent.defeat["ankou"] > 0 and persistent.defeat["kraken"] > 0 and persistent.defeat["ghost"] > 0 and persistent.defeat["doll"] > 0 and persistent.defeat["zonbe"] > 0 and persistent.defeat["zonbes"] > 0 and persistent.defeat["frederika"] > 0 and persistent.defeat["chrom"] > 0 and persistent.defeat["fairy"] > 0 and persistent.defeat["elf"] > 0 and persistent.defeat["tfairy"] > 0 and persistent.defeat["fairys"] > 0 and persistent.defeat["sylph"] > 0 and persistent.defeat["c_dryad"] > 0 and persistent.defeat["alice"] > 0 and persistent.defeat["taran"] > 0 and persistent.defeat["mino"] > 0 and persistent.defeat["sasori"] > 0 and persistent.defeat["lamp"] > 0 and persistent.defeat["mummy"] > 0 and persistent.defeat["kobura"] > 0 and persistent.defeat["lamias"] > 0 and persistent.defeat["sphinx"] > 0 and persistent.defeat["suckvore"] > 0 and persistent.defeat["wormv"] > 0 and persistent.defeat["ironmaiden"] > 0 and persistent.defeat["lily"] > 0 and persistent.defeat["arizigoku"] > 0 and persistent.defeat["sandw"] > 0 and persistent.defeat["gnome"] > 0 and renpy.seen_label("mdg_ha") and renpy.seen_label("mdg_hb") and renpy.seen_label("vgirl_ha") and renpy.seen_label("vgirl_hb") and renpy.seen_label("mitubati_ha") and renpy.seen_label("mitubati_hb") and renpy.seen_label("kumo_ha") and renpy.seen_label("kumo_hb") and renpy.seen_label("mimic_ha") and renpy.seen_label("mimic_hb") and renpy.seen_label("nanabi_ha") and renpy.seen_label("nanabi_hb") and renpy.seen_label("lamia_ha") and renpy.seen_label("lamia_hb") and renpy.seen_label("lamiab_h") and renpy.seen_label("doll_ha") and renpy.seen_label("doll_hb") and renpy.seen_label("c_dryad_ha") and renpy.seen_label("c_dryad_hb") and persistent.game_clear > 0 and persistent.hsean_meia1 == 1 and persistent.hsean_sara1 == 1 and persistent.count_hanseikai > 49:
            persistent.count_zukan1 = 1

        if persistent.count_zukan2 != 1 and persistent.defeat["centa"] > 0 and persistent.defeat["kaeru"] > 0 and persistent.defeat["alraune"] > 0 and persistent.defeat["dullahan"] > 0 and persistent.defeat["cerberus"] > 0 and persistent.defeat["alma_elma2"] > 0 and persistent.defeat["yukionna"] > 0 and persistent.defeat["nekomata"] > 0 and persistent.defeat["samuraielf"] > 0 and persistent.defeat["yamatanooroti"] > 0 and persistent.defeat["moss"] > 0 and persistent.defeat["mukade"] > 0 and persistent.defeat["kaiko"] > 0 and persistent.defeat["suzumebati"] > 0 and persistent.defeat["queenbee"] > 0 and persistent.defeat["a_alm"] > 0 and persistent.defeat["a_vore"] > 0 and persistent.defeat["a_emp"] > 0 and persistent.defeat["dorothy"] > 0 and persistent.defeat["rafi"] > 0 and persistent.defeat["dina"] > 0 and persistent.defeat["jelly"] > 0 and persistent.defeat["blob"] > 0 and persistent.defeat["slime_green"] > 0 and persistent.defeat["erubetie1"] > 0 and persistent.defeat["undine"] > 0 and persistent.defeat["kamakiri"] > 0 and persistent.defeat["scylla"] > 0 and persistent.defeat["medusa"] > 0 and persistent.defeat["golem"] > 0 and persistent.defeat["madgolem"] > 0 and persistent.defeat["artm"] > 0 and persistent.defeat["ant"] > 0 and persistent.defeat["queenant"] > 0 and persistent.defeat["maccubus"] > 0 and persistent.defeat["minccubus"] > 0 and persistent.defeat["renccubus"] > 0 and persistent.defeat["succubus"] > 0 and persistent.defeat["witchs"] > 0 and persistent.defeat["lilith"] > 0 and persistent.defeat["madaminsect"] > 0 and persistent.defeat["madamumbrella"] > 0 and persistent.defeat["maidscyulla"] > 0 and persistent.defeat["emily"] > 0 and persistent.defeat["cassandra"] > 0 and persistent.defeat["yougan"] > 0 and persistent.defeat["basilisk"] > 0 and persistent.defeat["dragon"] > 0 and persistent.defeat["salamander"] > 0 and persistent.defeat["granberia3"] > 0 and persistent.defeat["kani2"] > 0 and persistent.defeat["hitode"] > 0 and persistent.defeat["dagon"] > 0 and persistent.defeat["hatibi"] > 0 and persistent.defeat["poseidones"] > 0 and persistent.defeat["seiren"] > 0 and persistent.defeat["inp"] > 0 and persistent.defeat["beelzebub"] > 0 and persistent.defeat["trickfairy"] > 0 and persistent.defeat["queenfairy"] > 0 and persistent.defeat["queenelf"] > 0 and persistent.defeat["saraevil"] > 0 and persistent.defeat["wyvern"] > 0 and persistent.defeat["kyoryuu"] > 0 and persistent.defeat["c_beast"] > 0 and persistent.defeat["c_dryad_vore"] > 0 and persistent.defeat["behemoth"] > 0 and persistent.defeat["vampire"] > 0 and persistent.defeat["esuccubus"] > 0 and persistent.defeat["gigantweapon"] > 0 and persistent.defeat["alma_elma3"] > 0 and persistent.defeat["tamamo2"] > 0 and persistent.defeat["erubetie2"] > 0 and persistent.defeat["granberia4"] > 0 and persistent.defeat["alice3"] > 0 and persistent.defeat["kunoitielf"] > 0 and persistent.defeat["mosquito"] > 0 and persistent.defeat["imomusi"] > 0 and persistent.defeat["a_looty"] > 0 and persistent.defeat["a_parasol"] > 0 and persistent.defeat["a_prison"] > 0 and persistent.defeat["slime_blue"] > 0 and renpy.seen_label("kaeru_ha") and renpy.seen_label("kaeru_hb") and renpy.seen_label("cerberus_ha") and renpy.seen_label("cerberus_hb") and renpy.seen_label("alma_elma2_ha") and renpy.seen_label("alma_elma2_hb") and renpy.seen_label("yukionna_ha") and renpy.seen_label("yukionna_hb") and renpy.seen_label("witchs_ha") and renpy.seen_label("witchs_hb") and renpy.seen_label("cassandra_ha") and renpy.seen_label("cassandra_hb") and renpy.seen_label("c_dryad_vore_ha") and renpy.seen_label("c_dryad_vore_hb") and renpy.seen_label("c_dryad_vore_hc") and renpy.seen_label("hatibi_ha") and renpy.seen_label("hatibi_hb") and renpy.seen_label("inp_ha") and renpy.seen_label("inp_hb") and persistent.game_clear > 1 and persistent.hsean_irias1 == 1:
            persistent.count_zukan2 = 1

        if persistent.count_zukan3 != 1 and persistent.defeat["cupid"] > 0 and persistent.defeat["valkyrie"] > 0 and persistent.defeat["ariel"] > 0 and persistent.defeat["c_s2"] > 0 and persistent.defeat["c_a3"] > 0 and persistent.defeat["stein1"] > 0 and persistent.defeat["trinity"] > 0 and persistent.defeat["ranael"] > 0 and persistent.defeat["c_tangh"] > 0 and persistent.defeat["nagael"] > 0 and persistent.defeat["mariel"] > 0 and persistent.defeat["angels"] > 0 and persistent.defeat["c_tentacle"] > 0 and persistent.defeat["c_medulahan"] > 0 and persistent.defeat["sisterlamia"] > 0 and persistent.defeat["c_bug"] > 0 and persistent.defeat["muzukiel"] > 0 and persistent.defeat["mermaid"] > 0 and persistent.defeat["g_mermaid"] > 0 and persistent.defeat["ningyohime"] > 0 and persistent.defeat["queenmermaid"] > 0 and persistent.defeat["traptemis"] > 0 and persistent.defeat["shadow"] > 0 and persistent.defeat["gaistvine"] > 0 and persistent.defeat["chrom2"] > 0 and persistent.defeat["berryel"] > 0 and persistent.defeat["revel"] > 0 and persistent.defeat["alakneload"] > 0 and persistent.defeat["tutigumo"] > 0 and persistent.defeat["kumonomiko"] > 0 and persistent.defeat["c_homunculus"] > 0 and persistent.defeat["ironmaiden_k"] > 0 and persistent.defeat["lusia"] > 0 and persistent.defeat["silkiel"] > 0 and persistent.defeat["endiel"] > 0 and persistent.defeat["trooperloid"] > 0 and persistent.defeat["knightloid"] > 0 and persistent.defeat["akaname"] > 0 and persistent.defeat["mikolamia"] > 0 and persistent.defeat["sirohebisama"] > 0 and persistent.defeat["walraune"] > 0 and persistent.defeat["dryad"] > 0 and persistent.defeat["queenalraune"] > 0 and persistent.defeat["slimelord"] > 0 and persistent.defeat["eggel"] > 0 and persistent.defeat["carmilla"] > 0 and persistent.defeat["elisabeth"] > 0 and persistent.defeat["queenvanpire"] > 0 and persistent.defeat["inps"] > 0 and persistent.defeat["narcubus"] > 0 and persistent.defeat["eva"] > 0 and persistent.defeat["lamianloid"] > 0 and persistent.defeat["assassinloid"] > 0 and persistent.defeat["yomotu"] > 0 and persistent.defeat["wormiel"] > 0 and persistent.defeat["fermesara"] > 0 and persistent.defeat["angelghoul"] > 0 and persistent.defeat["dragonzonbe"] > 0 and persistent.defeat["doppele"] > 0 and persistent.defeat["shirom"] > 0 and persistent.defeat["drainplant"] > 0 and persistent.defeat["drainloid"] > 0 and persistent.defeat["replicant"] > 0 and persistent.defeat["laplace"] > 0 and persistent.defeat["alice8th1"] > 0 and persistent.defeat["gargoyle"] > 0 and persistent.defeat["hainu"] > 0 and persistent.defeat["amphis"] > 0 and persistent.defeat["tukuyomi"] > 0 and persistent.defeat["arcen"] > 0 and persistent.defeat["rapun"] > 0 and persistent.defeat["heavensgate"] > 0 and persistent.defeat["eden"] > 0 and persistent.defeat["stein2"] > 0 and persistent.defeat["alice8th2"] > 0 and persistent.defeat["alice8th3"] > 0 and persistent.defeat["alice8th4"] > 0 and persistent.defeat["irias3"] > 0 and persistent.defeat["irias4"] > 0 and persistent.defeat["kezyorou"] > 0 and persistent.defeat["cirque1"] > 0 and persistent.defeat["alice15th"] > 0 and renpy.seen_label("dryad_ha") and renpy.seen_label("dryad_hb") and renpy.seen_label("inps_ha") and renpy.seen_label("inps_hb") and renpy.seen_label("ilias2_ha") and renpy.seen_label("ilias2_hb") and renpy.seen_label("mariel_ha") and renpy.seen_label("mariel_hb") and renpy.seen_label("eden_ha") and renpy.seen_label("eden_hb") and renpy.seen_label("alakneload_ha") and renpy.seen_label("alakneload_hb") and renpy.seen_label("alice8th1_ha") and renpy.seen_label("alice8th1_hb") and renpy.seen_label("g_mermaid_ha") and renpy.seen_label("g_mermaid_hb") and renpy.seen_label("lamianloid_ha") and renpy.seen_label("lamianloid_hb") and renpy.seen_label("queenmermaid_ha") and renpy.seen_label("queenmermaid_hb") and persistent.hsean_shitenno1 > 0 and persistent.hsean_shitenno2 > 0 and persistent.hsean_shitenno3 > 0 and persistent.hsean_shitenno4 > 0 and persistent.hsean_shitenno5 > 0 and persistent.hsean_end1 > 0 and persistent.hsean_end2 > 0 and persistent.hsean_end3 > 0 and persistent.hsean_end4 > 0 and persistent.hsean_end5 > 0 and persistent.hsean_end6 > 0 and persistent.hsean_end7 > 0 and persistent.hsean_labo > 0 and persistent.game_clear == 3:
            persistent.count_zukan3 = 1

    tag menu

    if enter == "ex":
        add Solid("#000")
    else:
        add renpy.get_screen("bg", layer='master')

    add "gm_bg"

    frame:
        style_group 'record'
        has vbox
        if page == 1:
            python:
                EVAL = str(persistent.count_hanseikai + persistent.count_hanseikai2)

            text "Врагов побеждено: [persistent.count_ko]"
            text "Оргазмов: [persistent.count_end]"
            text "Капитуляций: [persistent.count_kousan]"
            text "Запросов: [persistent.count_onedari]"
            text "Получено наставлений: [EVAL]"
            text "HP поглощено при атаке: [persistent.count_drainhp]"
            text "Уровней поглощено: [persistent.count_drainlv]"
            text "Количество спонтанных оргазмов: [persistent.count_bouhatu]"

            if not persistent.count_mostname:
                text "Больше всего проигрышей: Никому"
            else:
                text "Больше всего проигрышей: [persistent.count_mostname] ([persistent.count_most])"

            if persistent.count_wind > 0:
                text "Сильфа призвана: [persistent.count_wind] раз"

            if persistent.count_earth > 0:
                text "Гнома призвана: [persistent.count_earth] раз"

            if persistent.count_aqua > 0:
                text "Ундина призвана: [persistent.count_aqua] раз"

            if persistent.count_fire > 0:
                text "Саламандра призвана: [persistent.count_fire] раз"

        elif page == 2:
            text "Вагинальных эякуляций: [persistent.count_nakadashi]"
            text "Эякуляций во время особых услуг ротиком: [persistent.count_sakuseikikan]"
            text "Эякуляций меж бёдрами: [persistent.count_sumata]"
            text "Эякуляций от минета: [persistent.count_fera]"
            text "Эякуляций от пайзури: [persistent.count_paizuri]"
            text "Эякуляций от ласк ручками: [persistent.count_tekoki]"
            text "Эякуляций от ласк ножками: [persistent.count_ashikoki]"
            text "Эякуляций от ласк волосами: [persistent.count_kami]"
            text "Эякуляций от ласк телом: [persistent.count_zenshin]"
            text "Эякуляций от поцелуев: [persistent.count_kiss]"
            text "Эякуляций от анального секса: [persistent.count_analsex]"
            text "Эякуляций от пыток и издевательств над сосками: [persistent.count_tikubi]"
            text "Эякуляций от массажа простаты: [persistent.count_analseme]"

        elif page == 3:
            text "Эякуляций от массажа всего тела: [persistent.count_zensin]"
            text "Эякуляций от щекотки: [persistent.count_kusuguri]"
            text "Эякуляций от покатушек на лице: [persistent.count_ganmenkizyou]"
            text "Эякуляций от удушья: [persistent.count_tissoku]"
            text "Эякуляций от принудительной мастурбации: [persistent.count_onani]"
            text "Эякуляций от атак щупальцами: [persistent.count_syokusyu]"
            text "Эякуляций от атак слизью: [persistent.count_slime]"
            text "Эякуляций от атак слизистых мембран: [persistent.count_nenmaku]"
            text "Эякуляций от игрищ и дразнений хвостиками: [persistent.count_sipppo]"
            text "Эякуляций от атак верёвками: [persistent.count_ito]"
            text "Эякуляций от атак сковывания: [persistent.count_role]"
            text "Эякуляций от атак всякой растительностью: [persistent.count_plant]"
            text "Эякуляций от атак крыльями: [persistent.count_wing]"

        elif page == 4:
            text "Эякуляций от атак одеждой: [persistent.count_ihuku]"
            text "Эякуляций от атак ветром: [persistent.count_kaze]"
            text "Эякуляций от атак пожирания и поглощения: [persistent.count_marunomi]"
            text "Количество поражений от физических атак: [persistent.count_buturi]"
            text "Количество самопоражений в замешательстве: [persistent.count_yuuwaku]"
            text "Эякуляций от иных атак: [persistent.count_sonota]"
            text "Количество оргазмов, будучи скованным: [persistent.count_kousoku]"
            text "Количество оргазмов в трансе: [persistent.count_koukotu]"
            text "Количество оргазмов, будучи парализованным: [persistent.count_mahi]"
            text "Количество оргазмов во время искушений: [persistent.count_kuppuku]"
            text "Количество оргазмов во время принуждения к капитуляции: [persistent.count_yuwaku]"
            text "Количество оргазмов во время окаменения: [persistent.count_sekika]"
            text "Количество оргазмов во время замешательства: [persistent.count_konran]"

        elif page == 5:
            text "Количество раз, когда оставлен как партнёр для размножения: [persistent.count_b001]"
            text "Количество спариваний для размножения и смертей после этого: [persistent.count_b002]"
            text "Количество раз, когда оставался служить бесконечной кормёжкой: [persistent.count_b003]"
            text "Количество смертей от высасывания досуха: [persistent.count_b004]"
            text "Количество превращений в игрушку: [persistent.count_b005]"
            text "Количество игрушек-веселушек и следующих за ними смертей: [persistent.count_b006]"
            text "Количество превращений в групповую игрушку: [persistent.count_b007]"
            text "Количество смертей от группового иссушения до костей: [persistent.count_b008]"
            text "Количество превращений в раба для монстра: [persistent.count_b009]"
            text "Количество принудительных свадеб: [persistent.count_b010]"
            text "Количество пожираний монстром: [persistent.count_b011]"
            text "Количество превращений в подопытного кролика: [persistent.count_b013]"
            text "Количество принятых приглашений к смерти: [persistent.count_b014]"

        elif page == 6:
            text "Количество помещений в Монстролечебницу: [persistent.count_b012]"
            text "Количество перерождений в совершенно новое бытие: [persistent.count_b015]"
            text "Количество наказаний в виде беспрестанного высасывания спермы: [persistent.count_b016]"
            text "Количество спермовыжимающих наказаний и смертей после этого: [persistent.count_b017]"

        elif page == 7:
            text "Завершить первую главу":
                if persistent.game_clear == 0 or persistent.gnome_unlock != 1:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Завершить вторую главу":
                if persistent.game_clear < 2 or persistent.alice2_unlock != 1:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Завершить третью главу":
                if persistent.game_clear < 3:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Заполнить первую главу монстропедии":
                if not persistent.count_zukan1:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Заполнить вторую главу монстропедии":
                if not persistent.count_zukan2:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Заполнить третью главу монстропедии":
                if not persistent.count_zukan3:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Получил удар всеми запрашиваемыми приёмами первой главы":
                if persistent.count_oskill1 != 262:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Получил удар всеми запрашиваемыми приёмами второй главы":
                if persistent.count_oskill2 != 402:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Получил удар всеми запрашиваемыми приёмами третьей главы":
                if persistent.count_oskill3 != 443:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Монстропедия полностью заполнена":
                if persistent.count_zukan1 != 1 or persistent.count_zukan2 != 1 or persistent.count_zukan3 == 1 or persistent.count_oskill1 + persistent.count_oskill2 + persistent.count_oskill3 <= 1107:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил, не получив урона":
                if not persistent.count_hpfull:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён в первый же ход":
                if not persistent.count_onekill:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил, будучи в это время в замешательстве":
                if not persistent.count_vickonran:
                    color "#8886"
                    outlines [(2, "#0008")]

        elif page == 8:
            text "Победил в битве на АДСКОМ режиме сложности":
                if not persistent.count_hellvic:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Иссушён до самого 1-го уровня":
                if not persistent.count_mylv0:
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.count_kadora == 1:
                text "Заставил противника вкусить всю мощь Четырёхкратного Гига-удара"
            elif persistent.count_kadora == 0:
                text "Заставил противника вкусить всю мощь Четырёхкратного Гига-удара":
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.g_sylph == 1:
                text "Сильфа призвана уставшей"
            elif persistent.g_sylph == 0 and persistent.game_clear == 0:
                text "?????? призвана уставшей":
                    color "#8886"
                    outlines [(2, "#0008")]
            elif persistent.g_sylph == 0 and persistent.game_clear > 0:
                text "Сильфа призвана уставшей":
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.z_gnome == 1:
                text "Гнома призвана спящей"
            elif persistent.z_gnome == 0 and persistent.game_clear < 2:
                text "?????? призвана спящей":
                    color "#8886"
                    outlines [(2, "#0008")]
            elif persistent.z_gnome == 0 and persistent.game_clear > 1:
                text "Гнома призвана спящей":
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён противником, усиленным Зильфой.":
                if not persistent.count_enemy_wind:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён противником, усиленным Гномарен.":
                if not persistent.count_enemy_earth:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён противником, усиленным Грандиной.":
                if not persistent.count_enemy_aqua:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён противником, усиленным Гигамандрой.":
                if not persistent.count_enemy_fire:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил противника контратакой.":
                if not persistent.counterk:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Контратаковал заряженный приём противника.":
                if not persistent.tamek:
                    color "#8886"
                    outlines [(2, "#0008")]

        elif page == 9:
            text "Победил Гранберию на адской сложности.":
                if not persistent.granberia_hell:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил Королеву Гарпий на адской сложности.":
                if not persistent.queenhapy_hell:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил Тамамо на адской сложности.":
                if not persistent.tamamo_hell:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Отношения с Гранберией на максимуме (Конец 1 части)":
                if not persistent.granberia_maxrep:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Отношения с Тамамо на максимуме (Конец 1 части)":
                if not persistent.tamamo_maxrep:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Отношения с Гранберией на минимуме (Конец 1 части)":
                if not persistent.granberia_minrep:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Словил Звёздный Клинок Гранберии":
                if not persistent.starblade:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Словил Девять Лун Тамамо и победил":
                if not persistent.nine_moons_won:
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.tamamo_knows == 1:
                text "Тамамо что-то знает..."
            elif persistent.tamamo_knows == 0:
                text "????? что-то знает...":
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Получил все достижения 1 главы NG+":
                if not persistent.ng1_achi:
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.lilith_hell == 1:
                text "Победил Лилит и Лилим на адской сложности"
            elif persistent.lilith_hell == 0:
                text "????? на адской сложности":
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Дебьют купидона-суккуба!":
                if not persistent.cupidsuccubus_debut:
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.lily_generator == 1:
                text "Перегрузил генератор барьера Лили"
            elif persistent.lily_generator == 0:
                text "Перегрузил генератор ????? ?????":
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.koakgigacount == 1:
                text "Заставить Коакуму вкусить всю мощь Четырёхкратного Гига-удара"
            elif persistent.koakgigacount == 0:
                text "Заставить ????? вкусить всю мощь Четырёхкратного Гига-удара":
                    color "#8886"
                    outlines [(2, "#0008")]

        elif page == 10 and in_game == 1:
            if party_member[0] == 1:
                text "Отношения с Алисой: [alice_rep]"
            else:
                text "?????"
            if party_member[1] == 2:
                text "Отношения с Гранберией: [granberia_rep]"
            elif party_member[1] == 3:
                text "Отношения с Тамамо: [tamamo_rep]"
            elif party_member[1] == 4:
                text "Отношения с Альма Эльмой: [alma_rep]"
            elif party_member[1] == 5:
                text "Отношения с Эрубети: [erubetie_rep]"
            else:
                text "?????"
            if skill02 > 0:
                text "Отношения с Сильфой: [sylph_rep]"
            else:
                text "?????"
            if skill02 > 0:
                text "Отношения с Гномой: [gnome_rep]"
            else:
                text "?????"
            if skill02 > 0:
                text "Отношения с Ундиной: [undine_rep]"
            else:
                text "?????"
            if skill02 > 0:
                text "Отношения с Саламандрой: [salamander_rep]"
            else:
                text "?????"
            if party_member[2] == 6:
                text "Отношения с кицунэ: [kitsune_rep]"
            elif party_member[2] == 7:
                text "Отношения с гарпией: [harpy_rep]"
            elif party_member[2] == 8:
                text "Отношения с Курому: [chrome_rep]"
            elif party_member[2] == 9:
                text "Отношения с Сарой: [sara_rep]"
            else:
                text "?????"
            if party_member[3] == 6:
                text "Отношения с кицунэ: [kitsune_rep]"
            elif party_member[3] == 7:
                text "Отношения с гарпией: [harpy_rep]"
            elif party_member[3] == 8:
                text "Отношения с Курому: [chrome_rep]"
            elif party_member[3] == 9:
                text "Отношения с Сарой: [sara_rep]"
            else:
                text "?????"

    if in_game == 1:
        use pages(page, 10)
    else:
        use pages(page, 9)
    use title("Походный дневник")
    use return_but(enter)

style record_button:
    is default
    background None

style record_frame:
    background Frame("images/System/win_bg_0.webp", 5, 5)
    xfill True
    yfill True
    margin (30, 60)
    padding (5, 20)

style record_frame:
    variant "touch"
    xmargin 50

style record_text:
    is item_text
    font "fonts/archiform.otf"
    outlines [(2, '#000a')]
    size 19

style record_text:
    variant "touch"
    size 18

##############################################################################
#
# Pearl Screen
#
screen pearl(enter="gm"):
    default yvalue = 1.0
    tag menu

    # Avoid predicting this screen. It's big, and all the images should be
    # predicted by one of the other screens.
    add renpy.get_screen("bg", layer='master')
    add "gm_bg"

    predict False

    frame:
        style_group "history"
        frame:
            background None
            margin (0, 0)

            has viewport:
                mousewheel True
                draggable True
                yinitial yvalue
                scrollbars "vertical"

            vbox:
                spacing 10

                text "1. Синии Ангела. (Видимо вместе с синим Лукой)." size 20
                text "2. Перенёссе. (Указывающее на место проживания переводчика)." size 20
                text "3.  Рубашка, которая на мне надета сейчас. (Шедевр который не нуждается в пояснении)." size 20
                text "4. Провернь. (Это как прорубь, но на проверку. Так же это французкая провинция)." size 20
                text "5. Заикающийся Лука. (НГ+ воистину страшная игра)." size 20
                text "6. Что я мне делать? (Гранберия весьма озадачена тем, что проиграла Луке)." size 20
                text "7. Покинем из город. (Илиасбург стал оградой)." size 20
                text "8. Обижжать. (Совмещение обиды и самосожжения)." size 20
                text "9. Посмотри смотри за мной! (За Лукой нужен глаз да глаз!)." size 20
                text "10. ПОЛУЧАЙ СУКА! (Лучшая фраза при крите)." size 20
                text "11. Бессмысленная сайдквесты. (Жи есть, брат)." size 20
                text "12. Полнуешь. (Крайняя степень волнения. aka полное волнение)." size 20
                text "13. Оружейный магазиг. (Берегитесь вооруженных магических зиг!)." size 20
                text "14. Тамаом. (Закон Тамаома - эмпирический физический закон, определяющий связь электродвижущей силы источника с силой тока, протекающего в проводнике, и сопротивлением проводника. Открыт ученой Тамаомо но Маэ в 14хх году по календарю Йохана)." size 20
                text "15. Шторм начинался еще в прошлой когда. (Удивительная аномалия, пришедшая к нам из чертзнаетоткуда. Этот перл так-же легендарен как и рубашка, которая на мне надета сейчас)." size 20
                text "16. Я... Я правда не хочешь говорить об этом... (Лучок говорит о себе в третьем лице. Опять)." size 20
                text "17. Если собираешься завтра утром рано уходить. (Еще один легендарный перл, подобный рубашке)." size 20
                text "18. Азазазазавтраки. (Алиса и еда. Классика!)." size 20
                text "19. Сильфа Сильфы. (Безумие и Чи-Па-Па в чистом виде)." size 20
                text "20. Спасибо, Ундина. (Лука заглянул в будущее и поблагодарил Ундину заранее)." size 20
                text "21. Очень жально. (Жалко, но жалко у пчелки :с)." size 20
                text "22. Явно он не так сражался! (Гарпия крайне впечатлена умениями Луки.)" size 20
                text "23. Как бы сильно я не взглядывался... (Взглязы не сильно то и помогают, верно?)" size 20
                text "24. Лучшая отсылка всех времен и народов! (Какой-то порт, какой-то персонаж.)" size 20

    use title("Книга пёрлов Максима Михайлова")
    use return_but(enter)

screen history(enter="gm"):
    default yvalue = 1.0
    tag menu

    # Avoid predicting this screen. It's big, and all the images should be
    # predicted by one of the other screens.
    add renpy.get_screen("bg", layer='master')
    add "gm_bg"

    predict False

    frame:
        style_group "history"

        if not _history_list:
            label "Нет истории диалогов.":
                align(.5, .5)
        else:
            side "c r":

                frame:
                    background None
                    margin (0, 0)

                    has viewport:
                        mousewheel True
                        draggable True
                        yinitial yvalue
                        scrollbars "vertical"

                    vbox:
                        spacing 10

                        for h in _history_list:
                            text (h.who or " ") style "history_who"

                            hbox:
                                box_wrap True
                                spacing 10

                                if h.voice.filename:
                                    imagebutton:
                                        idle "idle_backlog_voice_butt"
                                        hover "hover_backlog_voice_butt"
                                        selected "hover_backlog_voice_butt"

                                        action Play("voice", h.voice.filename)

                                text h.what size 20

                            null height 10

    use title("Журнал")
    use return_but(enter)

style history_frame:
    background Frame("images/System/win_bg_0.webp", 5, 5)
    xfill True
    yfill True
    margin (50, 60)

style history_who:
    text_align 0.0
    color '#98FB98'
    font "fonts/archiform.otf"
    outlines [(2, '#000a')]
    size 28

style history_text:
    outlines [(2, '#000a')]
    font "fonts/archiform.otf"
    size 25

style history_label:
    xfill True
    align (.5, .5)

style history_label_text:
    font "fonts/archiform.otf"
    size 28

style history_vscrollbar:
    is pref_vscrollbar

##############################################################################
#
# Return Button
#

screen return_but(enter="gm"):
    textbutton "Назад":
        align (.97, .98)
        style "ret_button"

        if enter == "gm":
            action ShowMenu("custom_gm")
        elif enter == "ex" and main_menu:
            action Return()
        elif enter == "ex" and not main_menu:
            action MainMenu()
        elif enter == "spinoff_menu":
            action MainMenu(confirm=False)
        else:
            action [SetVariable('gm_char', 0), SetVariable('player', 0), Return()]

    if enter == "gm":
        key "game_menu" action ShowMenu("custom_gm")
    elif enter == "ex" and main_menu:
        key "game_menu" action Return()
    elif enter == "ex" and not main_menu:
        key "game_menu" action MainMenu()
    elif enter == "spinoff_menu":
        key "game_menu" action MainMenu(confirm=False)
    else:
        key "game_menu" action [SetVariable('gm_char', 0), SetVariable('player', 0), Return()]


screen return_but(enter="gm"):
    variant "touch"

    imagebutton:
        align (1.0, 1.0)
        at xyzoom(.9)
        auto "back_%s"
        margin (5, 3)

        if enter == "gm":
            action ShowMenu()
        elif enter == "ex" and main_menu:
            action Return()
        elif enter == "ex" and not main_menu:
            action MainMenu()
        elif enter == "spinoff_menu":
            action MainMenu(confirm=False)
        else:
            action Return()

    if enter == "gm":
        key "game_menu" action ShowMenu()
    elif enter == "ex" and main_menu:
        key "game_menu" action Return()
    elif enter == "ex" and not main_menu:
        key "game_menu" action MainMenu()
    elif enter == "spinoff_menu":
        key "game_menu" action MainMenu(confirm=False)
    else:
        key "game_menu" action Return()

style ret_button:
    is default
    background "#330000"
    padding (2, 0)

style ret_button_text:
    is default
    size 34
    line_spacing -6
    line_leading -6
    font 'fonts/taurus.ttf'
    idle_color "#fff000cc"
    hover_color "#000fffcc"


##############################################################################
#
# Title Menu
#

screen title(title):
    label title style "title"

style title:
    is label
    background "#330000"
    padding (0, 0, -10, 0)
    margin (0, 0)
    anchor (0, 0)
    pos (20, 10)

style title_text:
    size 40
    align (.5, .5)
    color "#fff000"
    font "fonts/taurus.ttf"
    line_leading -10
    line_spacing -10
    outlines [(1, "#330000"), (2, "#cc0000")]
    bold True

screen choice_lang():
    frame:
        background None
        align (.5, .5)

        hbox:
            spacing 50

            imagebutton:
                idle "images/Effects/british-flag/british-flag-small-orig.webp"
                hover "british_flag_wave"
                action [Language(None), Return()]

            imagebutton:
                idle "images/Effects/russian-flag/russian-flag-small-orig.webp"
                hover "russian_flag_wave"
                action [Language("russian"), Return()]


screen pages(page, max_page):
    if renpy.variant('touch'):
        $ zoom_factor = 0.9
        $ YALIGN = 1.0
        $ ADD = "_n"
    else:
        $ zoom_factor = 1.0
        $ YALIGN = 0.5
        $ ADD = ""

    frame:
        style_group 'pages'

        side "l c r":
            xfill True

            imagebutton:
                yalign YALIGN
                auto "left_%s" + ADD
                at xyzoom(zoom_factor)

                action If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page))

            label "[page]/[max_page]"

            imagebutton:
                yalign YALIGN
                auto "right_%s" + ADD
                at xyzoom(zoom_factor)

                action If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1))

    key "K_PAGEDOWN" action If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page))
    key "K_PAGEUP" action If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1))


style pages_frame:
    background None
    xsize 200
    margin (3, 0)
    align (.5, 1.0)

style pages_frame:
    variant "touch"
    xsize 250

style pages_label:
    background Solid("#330000")
    xfill True
    align (.5, 1.0)

style pages_label_text:
    align (.5, .5)
    color "#fffa"
    outlines [(2, "#000e")]
    size 24
    font "fonts/archiform.otf"


##########################################################
#
#
#
##########################################################
screen kitsune():
    imagebutton:
        idle "youko st02"
        hover "youko st02 hover"
        focus_mask True
        pos (-230, 0)
        action Return(1)

    imagebutton:
        idle "youko st01"
        hover "youko st01 hover"
        focus_mask True
        pos (0, 0)
        action Return(2)

    imagebutton:
        idle "youko st01"
        hover "youko st01 hover"
        focus_mask True
        pos (230, 0)
        action Return(3)


screen kitsune():
    variant "touch"

    default k = 2

    if k == 1:
        add "youko st02 hover" pos (-230, 0)
    elif k == 2:
        add "youko st01 hover" pos (0, 0)
    elif k == 3:
        add "youko st01 hover" pos (230, 0)

    imagebutton:
        align (0, .5)
        auto "left_%s_n"
        margin (2, 5)
        action [If(k > 1, true=SetScreenVariable('k', k-1), false=SetScreenVariable('k', 3))]

    imagebutton:
        align (1.0, .5)
        auto "right_%s_n"
        margin (2, 5)
        action [If(k < 3, true=SetScreenVariable('k', k+1), false=SetScreenVariable('k', 1))]

    textbutton "Confirm":
        action Return(k)
        style "gm_button"
        background Frame('images/System/win_bg_0.webp', Borders(10, 10, 10, 10))
        text_size 30
        align (.5, .9)
