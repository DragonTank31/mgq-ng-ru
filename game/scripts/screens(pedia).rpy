screen pedia_old(enter):

    tag menu
    default page = 1
    default max_range = 26
    $ p_girls = get_girls(page, max_range)
    $ L = len(p_girls)

    if enter == "ex":
        add Solid("#000")
    else:
        add renpy.get_screen("bg", layer='master')

    add "gm_bg"

    frame:
        xmargin 20
        padding (5, 10, 5, 0)
        ysize 450
        align (.5, .5)
        background Frame('images/System/win_bg_0.webp', 5, 5)

        grid 2 13:
            transpose True
            xfill True
            spacing 5

            for nm, i_img, obj in p_girls:
                if obj:
                    textbutton "[nm!t]" style "pedia_tb":
                        action ShowMenu("pedia_base", girl=obj)
                else:
                    text "???" style "pedia_lock"

            if L < max_range:
                for i in xrange(max_range-L):
                    null


    use pages(page, 2)
    use title("Монстропедия")
    use return_but(enter)

style pedia_tb:
    is button
    background None
    xalign 0
    xmargin 0
    xpadding 0

style pedia_tb_text:
    size 19
    line_spacing -5
    line_leading -5
    outlines [(2, '#000c')]
    font "fonts/archiform.otf"
    idle_color "#fff000cc"
    hover_color "#000fffcc"

style pedia_lock:
    size 19
    line_spacing -5
    line_leading -5
    font "fonts/archiform.otf"
    color "#80808060"
    outlines [(2, "#0008")]


screen pedia_base(girl):
    modal True
    default page = 'info'
    predict False

    on "hide" action [
        SetField(persistent, "bk1", 0),
        SetField(persistent, "bk2", 0),
        SetField(persistent, "bk3", 0),
        SetField(persistent, "bk4", 0),
        SetField(persistent, "bk5", 0),
        SetField(persistent, "bk6", 0),
        SetField(persistent, "bk7", 0),
        SetField(persistent, "bk8", 0),
        SetField(persistent, "pose", 1),
        SetField(persistent, "face", 1),
        SetField(persistent, "battle", 0),
        SetField(persistent, "echi", "")]

    python:
        img_list = girl.m_pose(girl.x0)
        scope = girl.scope
        scope["_game_menu_screen"] = "custom_gm"
        scope["monster_name"] = girl.name
        scope["_window_auto"] = True
        scope["addon_on"] = 0

    add "book"

    for IMG, POS in img_list:
        add IMG pos POS

    add "book_f" align (0, 0)

    if page == 'info' and _preferences.language == "russian":
        add girl.info_rus_img + "(rus)" align (1.0, 0)

    fixed:
        style_group 'pedia_base'

        vbox:
            xfill True

            text "[girl.pedia_name!t]{#pedia_base}":
                xalign .5
                size 24
                kerning -0.5
                color '#ff0000'
                outlines [(1, '#f3ede180')]
                line_spacing -5
                line_leading -1

            # null height 5

            if page == 'info' and _preferences.language != "russian":
                text "[girl.info!t]":
                    size 18
                    kerning -0.5
                    color '#000'
                    outlines [(1, '#f3ede180')]
                    line_spacing -2
                    line_leading -1


            elif page == 'data':
                text "Уровень: [girl.lv]":
                    size 20
                    color '#000'
                    outlines [(1, '#f3ede180')]
                text _('Hp: [girl.hp]{#pedia_base}'):
                    size 20
                    color '#000'
                    outlines [(1, '#f3ede180')]

                if girl.zukan_ko:
                    $ var = persistent.defeat[girl.zukan_ko]
                    text "Проигрышей: [var]":
                        size 20
                        color '#000'
                        outlines [(1, '#f3ede180')]
                else:
                    text "Проигрышей: Нет":
                        size 20
                        kerning -0.5
                        color '#000'
                        outlines [(1, '#f3ede180')]

                # null height 10

                if girl.skills is not None:
                    text "Умения:":
                        size 20
                        color '#000'
                        outlines [(1, '#f3ede180')]

                    for num, sk_name in girl.skills:
                        textbutton "[sk_name!t]{#pedia_base}":
                            xalign 0
                            left_margin 5
                            text_kerning -1
                            text_hover_color "#f00"
                            text_size 21
                            # text_outlines [(1, '#000a')]

                            action If(persistent.skills[num][0], ShowMenu("data2", c=persistent.skills[num]))

            elif page == 'recall':
                if in_game:
                    text "Вы не можете использовать воспоминания, проходя сюжет.":
                        size 20
                        # italic True
                        color '#000'
                        outlines [(1, '#f3ede180')]
                else:
                    if girl.zukan_ko:
                        textbutton "Battle {b}NORMAL{/b}":
                            left_margin 0
                            action [SetDict(scope, "Сложность", 1),
                                    SetDict(scope, "replay_h", False),
                                    Replay(girl.label + "_start", scope=scope)]
                        if persistent.battle != 1:
                            textbutton "Battle {b}HARD{/b}":
                                left_margin 0
                                action [SetDict(scope, "Сложность", 2),
                                        SetDict(scope, "replay_h", False),
                                        Replay(girl.label + "_start", scope=scope)]
                            textbutton "Battle {b}HELL{/b}":
                                left_margin 0
                                action [SetDict(scope, "Сложность", 3),
                                        SetDict(scope, "replay_h", False),
                                        Replay(girl.label + "_start", scope=scope)]

                    if len(girl.echi) == 1:
                        if girl.echi[0]:
                            $ hlb = girl.echi[0]
                        else:
                            $ hlb = girl.label + "_h"

                        textbutton _("Rape Scene"):
                            action [SetDict(scope, "replay_h", True),
                                    SetDict(scope, "monster_name", girl.name),
                                    Replay(hlb, scope=scope)]
                    else:
                        for num, replay_lb in enumerate(girl.echi, 1):

                            textbutton "Rape Scene [num]":
                                left_margin 0
                                action [SetDict(scope, "replay_h", True),
                                        SetDict(scope, "monster_name", girl.name),
                                        Replay(replay_lb , scope=scope)]


    frame:
        style_group 'pedia_nav'
        background None
        xsize 400
        align (1.0, 1.0)

        hbox:
            xalign .5
            textbutton "Описание" action If(page != "info", SetScreenVariable("page", 'info'))
            textbutton "Инфо" action If(page != "data", SetScreenVariable("page", 'data'))
            textbutton "Позы" action ShowMenu('pose', girl=girl)
            textbutton "CG" action If(girl.zukan_ko == None or persistent.defeat[girl.zukan_ko] != 0,
                                            Function(renpy.call_in_new_context, girl.label_cg)
                                            )
            textbutton "Вспомнить" action If(page != 'recall', true=SetScreenVariable("page", 'recall'))

    key "game_menu" action [Hide('pedia_base')]


style pedia_base_fixed:
    anchor (0, 0)
    pos (423, 10)
    xysize (352, 550)

style pedia_base_text:
    font 'fonts/cometa_w08.ttf'

style pedia_base_button:
    is button
    xalign 0
    background None
    xpadding 5
    xmargin 0

style pedia_base2_button:
    is pedia_base_button
    xalign 0.5

style pedia_nav_button:
    is pedia_base_button
    xalign 0.5

style pedia_base2_button_text:
    is pedia_base_button_text

style pedia_nav_button_text:
    is text
    xalign 0.0
    size 20
    line_spacing -2
    line_leading -2
    outlines [(2, '#000c', 0, 0)]
    font "fonts/taurus.ttf"
    color "#fff000cc"
    hover_color "#000fffcc"
    insensitive_color "#888c"

style pedia_base_button_text:
    is text
    xalign 0.0
    font 'fonts/cometa_w08.ttf'
    size 20
    outlines [(2, '#000c')]
    color "#fff000cc"
    hover_color "#000fffcc"
    insensitive_color "#888c"


############################################################
#
# Skills Data
#
############################################################
screen data2(c):
    modal True

    frame:
        style_group "data2"

        has vbox

        text "Taken: [c[0]]"
        text "Orgasms: [c[1]]"

    key "dismiss" action Hide("data2")
    key "game_menu" action Hide("data2")

style data2_frame:
    background Frame('images/System/win_bg_0.webp', 5, 5)
    # xsize (200, 100)
    align (.5, .5)
    padding (10, 10)

style data2_vbox:
    spacing 10

style data2_text:
    size 20

############################################################
#
# POSE
#
############################################################
screen pose(girl):
    default rm = 1

    modal True

    python:
        img_list = girl.m_pose(girl.x1)

    add "book"

    for IMG, POS in img_list:
        add IMG pos POS

    if rm == 1:
        frame:
            background Solid("8a01bd70")
            style_group "pose"
            align (1.0, 0)
            xysize (195, 600)
            ypadding 20

            frame:
                background None
                yalign 0
                xmargin 0
                xpadding 0
                has vbox
                textbutton "Лица" action If(girl.facenum > 1, SetField(persistent, "face", persistent.face+1))
                textbutton "Позы" action If(girl.posenum > 1, SetField(persistent, "pose", persistent.pose+1))

            frame:
                background None
                yalign .5
                xmargin 0
                xpadding 0
                has vbox

                if girl.bk_num > 0:
                    for i in xrange(1, girl.bk_num+1):
                        textbutton "Буккакэ [i]" action ToggleField(persistent, "bk" + str(i), true_value=True, false_value=False)
            frame:
                background None
                yalign 1.0
                xmargin 0
                xpadding 0
                has vbox
                textbutton "Очистить" action SetScreenVariable("rm", 0) yalign 1.0
                textbutton "Назад" action Hide('pose')

        key "game_menu" action Hide('pose')
    else:
        key "dismiss" action SetScreenVariable("rm", 1)
        key "game_menu" action SetScreenVariable("rm", 1)

style pose_button:
    is button
    background None

style pose_button_text:
    is text
    size 26
    font "fonts/cometa_w08.ttf"
    idle_color "#fff"
    outlines [(1, "#000a")]
    hover_color "#f00"
    selected_color "#3ccd7a"
    insensitive_color "#8888"
    insensitive_outlines [(1, "#303030")]
