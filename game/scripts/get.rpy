init python:

    def get_battle_item():
        s = []
        item_text = "Боевые"

        if store.item01 > 0 or store.ngitem == 1:
            s.append(("Железный меч",
                _("Легкий меч, выкованный у деревенского кузнеца.\nСпроектирован специально для невысокого роста Луки.")))
        if store.item01 > 4 or store.ngitem == 1:
            s.append(("Сияние Ангела",
                _("Жуткий меч, полученный от Алисы.\nСозданный из 666 поглощённых Ангелов, он способен запечатывать монстров и превращать в пыль ангелов.")))

        if store.item01 > 0:
            s.append(("Памятное кольцо",
                _("Памятный подарок моей матери.\nОно является дорогой памятью о моей матери, а также печатью, ограничивающую мою ангельскую силу.")))

        if store.item01 < 4 and store.ngitem != 1:
            s.append(("Тканевая рубашка",
                _("Одежда, сшитая Бетти.\nСделана добротно, но для боя не подходит.")))
        elif store.item01 > 3 or store.ngitem == 1:
            s.append(("Энрикийская рубашка",
                _("Защитная одежда из Энрики, зачарованная эльфийскими техниками.\nНесмотря на свою лёгкость она весьма прочная.")))
        if store.item21 > 0:
            s.append(("Запасная энрикийская рубашка",
                _("Защитная одежда из Энрики, зачарованная эльфийскими техниками.\nНесмотря на то, что такая у меня уже есть, сменная одежда никогда не повредит.")))

        return s

    def get_valuables_item():
        s = []
        item_text = "Ключевые"

        if store.red_orb > 0:
            s.append(("Красная сфера",
                _("Красная Сфера, необходимая для воскрешения Святых Крыльев.\nОна воскреснет, когда будут собраны все шесть сфер.")))

        if store.green_orb > 0:
            s.append(("Зелёная сфера",
                    _("Зелёная Сфера, необходимая для воскрешения Святых Крыльев.\nОна воскреснет, когда будут собраны все шесть сфер.")))

        if store.silver_orb > 0:
            s.append(("Серебрянная сфера",
                _("Серебрянная Сфера, необходимая для воскрешения Святых Крыльев.\nОна воскреснет, когда будут собраны все шесть сфер.")))

        if store.item01 > 5:
            s.append(("Колокольчик Посейдона",
                _("Сокровище Капитана Селины.\nПока оно находится на носу корабля, нам не страшен серый шторм.")))

        if store.item04 == 1:
            s.append(("Письменное обещание",
                _("Клятва от Меи и её мужа.\nПравда, не понимаю я этих их церемоний...")))
        elif store.item04 == 2:
            s.append(("Клятвенное кольцо",
                _("Парные кольца, подаренные Кракеном.\nЭто их доказательство женитьбы.")))

        if store.yellow_orb > 0:
            s.append(("Жёлтая сфера",
                _("Жёлтая Сфера, необходимая для воскрешения Святых Крыльев.\nПохоже, они воскреснут, когда будут собраны все шесть сфер.")))

        if store.blue_orb > 0:
            s.append(("Синяя сфера",
             _("Синяя Сфера, необходимая для воскрешения Святых Крыльев.\nПохоже, они воскреснут, когда будут собраны все шесть сфер.")))

        if store.purple_orb > 0:
            s.append(("Пурпурная сфера",
                _("Пурпурная Сфера, необходимая для воскрешения Святых Крыльев.\nПохоже, они воскреснут, когда будут собраны все шесть сфер.")))

        if store.item04 in (1, 2, 4):
            s.append(("Путеводный шар",
                _("Драгоценность, полученная от Меи.\nБлагодаря ей я могу спустится в Морской храм.")))

        return s


    def get_other_item():
        s = []
        item_text = "Остальные"

        if store.item01 > 1:
            s.append(("Всемирный путеводитель",
            _("Лучшая книга для искателей приключений и гурманов.\nОднако ей уже более 500 лет, так что сведения в ней несколько... устарели.\n... И почему я за неё таскаю её книгу?")))

        if store.item03 > 0:
            s.append(("Мёд Счастья",
            _("Сладкий мёд из Деревни Счастья.\nЕго можно есть как с хлебом, так и просто так, из банки.\nКак и ожидалось, Алиса заставила меня тащить и его.")))

        # if store.item01 > 7:
        #     s.append(("Четыре анимиста и их источники",
        #     _("Книга, {b}позаимствованная{/b} в замке Сан Илия.\nЯ не смог вернуть её вовремя, поэтому мне приходится носить её с собой.")))

        if store.item11 == 1:
            s.append(("Жареная актиния",
            _("Совершенно несъедобная еда, которую я был вынужден купить в Порту Наталия.\nОна выглядит ещё хуже, чем Жаренная Морская звезда.\nАлиса от неё в ужасе.")))

        if store.item01 > 8:
            s.append(("Корзина скорпионов",
            _("Алиса вновь наловила целую корзину скорпионов.\n... Почему я до сих пор держу это?")))

        if store.item14 == 1:
            s.append(("Цветок Алрауны",
            _("Странный цветок, купленный у Алрауны-цветочницы.\nУже начал увядать...\nПохоже, из-за большого отпечатка зубов Алисы на нём...")))

        if store.item16 == 1:
            s.append(("Тамамо-шар",
            _("Такой пуши-и-истый...")))

        if store.item18 == 1:
            s.append(("Драгоценность Победы",
            _("Драгоценность, полученная от Амиры.\nРаньше она даровала силу победы, но эта сила практически покинула этот камень.\nВыглядит бесполезным.")))

        if store.item01 > 2:
            s.append(("Ама-ама-данго",
            _("Настоящая роскошь из {b}Гостиницы Сазерленд{/b}.\nОни приготовлены с Мёдом Счастья и невероятно сладкие.\nУдивлён, что Алиса до сих пор не съела их прямо у меня из рук.")))

        if store.item01 > 6:
            s.append(("Жареная морская звезда",
            _("Совершенно несъедобная еда, которую я был вынужден купить в Порту Наталия.\nДаже Алиса не хочет её пробовать...")))

        if store.item10 == 1:
            s.append(("Святая антипризрачная вода",
            _("Противопризрачная вода, купленная в Сан Илия.\nПо-моему, это просто обычная вода.")))

        if store.item12 == 1:
            s.append(("Жёлудь Фей",
            _("Знак дружбы от милой Феи.\nКогда я держу его, я чувствую силу, текущую сквозь меня.\nОно является дорогим для меня сокровищем.")))
        elif store.item12 == 2:
            s.append(("Jewel of Friendship",
            _("Created by combining the Fairy Acorn and Nekomata's Bell\nHolding the power of friendship, I can see the Fairies and Nekomata\nIt's an important treasure to me.")))

        if store.item13 == 1:
            s.append(("Genie's Lamp",
            _("A lamp containing a Genie Girl.\nIt was dangerous to leave her there, so I kept it.\nI can't destroy it or dispose of it... How annoying.")))
        elif store.item13 == 2:
            s.append(("Lottery Ticket",
            _("A lottery ticket given with gold for the lamp.\nLooks like I can use it in Gold Port.")))
        elif store.item13 == 3:
            s.append(("Jewel of Fortune",
            _("The first prize from the lottery.\nIt used to have the power of luck, but it's mostly gone now.\nSeems worthless.")))

        if store.item15 == 1:
            s.append(("Nekomata's Bell",
            _("A bell from a Nekomata.\nThere was some power in it at first, but not any longer.\nI had to give food to get it. Alice might get angry if she finds out.")))

        if store.item17 == 1 or store.ngitem == 1:
            s.append(("Медаль Героя",
            _("Красивая медаль, которую может получить только настоящий Герой.\nАбсолютно бесполезная вещица.\nВсё, что я могу - так это просто смотреть на неё и ухмыляться.")))

        return s

    def get_skills():
        s = []

        if skill01 > 18 and store.skillset == 2:
            s.append(("Мгновенное Убийство (SP: 2)",
                _("Тайный приём падшего ангела, прорубающий насквозь само время и пространство.\nОчень большой урон за сравнительно малое количество очков навыков на его использование.\nПохоже, что он может сочетаться с силой ветра...")))
        elif skill01 > 12 and store.skillset == 1:
            s.append(("Безмятежный Демонический Клинок (SP: 2)",
                _("Приём Иайдо, используемый в состоянии безмятежности.\nНаносит гигантский урон, пока призвана Ундина.\nК тому же он может принудительно оборвать призыв Ундины.")))
        elif skill01 > 16 or 0 < skill01 < 13:
            s.append(("Демоническое Обезглавливание (SP: 2)",
                _("Прыжок на противника и резкий взмах мечом на его шею.\nЛёгкий и доступный приём, как раз подходящий под компактную стать Луки.")))

        if skill01 > 18 and store.skillset == 2:
            s.append(("Возрождение Небесного Демона (SP: 4)",
                _("Таинственный приём, являющий собой концентрацию магической энергии для уничтожения противников.\nНикому из тех, кто его видел, не посчастливилось дожить до момента, чтобы его описать.\nПохоже, что этот приём может сочетаться с силой земли...")))
        elif skill01 > 10 and store.skillset == 1:
            s.append(("Молниеносный Удар Клинка (SP: 2)",
                _("Искусство, использующее движение ветра как усиление для нанесения удара молниеносной скорости.\nНаносит дополнительный урон, если воспользоваться этим приёмом на первом ходу.\nСтановится ещё сильнее, если при этом вызвана Сильфа.")))
        elif skill01 > 16 or 2 < skill01 < 11:
            s.append(("Громовой выпад (SP: 2)",
                _("Сверхбыстрая атака, состоящая из скоростного выпада на противника.\nНаносит дополнительный урон, если воспользоваться этим приёмом на первом ходу.")))

        if skill01 > 18 and store.skillset == 2:
            s.append(("Девятиликий Ракшаса (SP: 6)",
                _("Целый шквал атак мечом, бушующий в танце смерти.\nЛегендарный приём, которым могут пользоваться лишь сильнейшие из ангелов.\nИ похоже, что он может сочетаться с силой огня...")))
        elif skill01 > 11 and store.skillset == 1:
            s.append(("Сотрясающее Землю Обезглавливание (SP: 3)",
                _("Приём, использующий силу земли для нанесения гибельного удара сверху.\nБезумно мощный.\nК тому же приём становится ещё мощнее, если при его использовании вызвана Гнома.")))
        elif skill01 > 16 or 4 < skill01 < 12:
            s.append(("Демонический Крушитель Черепов (SP: 3)",
                _("Смертоносная атака, включающая в себя прыжок откуда-нибудь свысока.\nНевероятно мощна, но использовать можно не везде.")))

        if skill01 > 18 and store.skillset == 2:
            s.append(("Денница (SP: 8)",
                _("Приём абсолютной мощи, созданный первородным падшим ангелом - Люцифиной.\nСлужит контратакой в ответ на удар противника, аннигилируя его.\nПохоже, что он, к тому же, может сочетаться с силой воды...")))
        elif skill01 > 15 and store.skillset == 1:
            s.append(("Неудержимый Испепеляющий Клинок (SP: 4)",
                _("Бесчисленное количество ударов, ещё и вдобавок усиленных очищающим пламенем.\nНевероятно могущественный приём, созданный Гранберией.\nЕго сила увеличивается ещё больше, если во время его выполнения вызвана Саламандра.")))
        elif skill01 > 16 or 5 < skill01 < 16:
            s.append(("Гибельный Клинок Звезды Хаоса (SP: 4)",
                _("Смертоносная атака мечом, наносящая по противнику множество ударов.\nЗа силу этого приёма, впрочем, приходится платить нехилым количеством очков навыков.")))
        elif 1 < skill01 < 6:
            s.append(("Беспорядочные удары (SP: 2)",
                _("Кошмарный и абсолютно бессмысленный приём, состоящий из случайных размахиваний мечом в разные стороны.\nПустая трата очков навыков, пустая трата сил и времени.")))

        if skill01 > 3:
            s.append(("Медитация (SP: 3)",
                _("Лука концентрирует силу своего духа, восстанавливая очки здоровья без использования магии.\nВосстанавливает половину максимального количества очков здоровья Луки.{w}\nЛегендарный навык, используемый падшими ангелами.")))

        if skill01 > 17:
            s.append(("Безмятежный разум (SP: 2)",
                _("Состояние Безмятежности достижимо даже без Ундины.\nПовышается сила атаки и шанс на уклонение.\nПохоже, что оно довольно-таки эффективно против монстров.")))

        if skill01 > 18 and store.skillset == 2:
            s.append(("Танец Падшего Ангела (SP: 2)",
                _("Тайный приём, заключающийся в единении со святым течением и уклонении от атак противника.\nПохоже, что он довольно эффективен в битве с ангелами и растениями.")))

        s.append((_("Крайность (SP: 0)"),
            _("Рискованный и отчаянный приём, резко снижающий количество очков здоровья Луки.\nЛегчайшего касания может оказаться достаточно, чтобы вывести Луку из себя, когда он на волоске от поражения.")))

        if skill01 > 25:
            s.append(("Разгон (SP: 0)",
                _("Демоническая техника, заключающаяся в концентрации своей темной энергии и извлечении из неё духовной энергии.{w}\nПри первом использовании даёт 2 SP. Прирост возрастает на единицу при каждом последовательном использовании.")))

        if skill01 > 19 and skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0:
            s.append(("Элементальная Спика (SP: 10)",
                _("Концентрация мощи всех Четырёх Духов в правой руке.\nПриём опустошительной мощи, изученный с помощью Генриха.")))

        if 14 < skill01 < 17 or skill01 > 20 and skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0:
            s.append(("Четырёхкратный Гига-удар (SP: 1)",
                _("Ультимативный приём абсолютной, опустошающей мощи, использующий всех четырёх духов для уничтожения противника.\nЕсли во время четырёх ходов зарядки противник наносит удар, то приём проваливается.\nБолее того, во время начала исполнения приёма все вызванные духи исчезают.")))

        if 6 < skill01 < 17 and skill02 == 1:
            s.append(("Сильфа (Lvl: 1) (SP:2)",
                _("Заимствование силы Сильфы для создания мощных порывов ветра.\nНебольшой шанс обезвредить атаки противника. Эффект длится до восьми ходов.")))
        elif skill01 > 6 and skill02 == 2:
            s.append(("Сильфа (Lvl: 2) (SP:2)",
                _("Манипуляция силой ветра для движения, словно ветряной шквал.\nВ дополнение к усилению уклонения от атак противника полностью устраняется шанс на промах.")))
        elif skill01 > 6 and skill02 == 3:
            s.append(("Сильфа (Lvl: 3) (SP:1)",
                _("Наделение себя могуществом опустошающего урагана для увеличения скорости движения.\nСпособность уклоняться от большинства атак и атаковать дважды.")))
        elif skill01 > 6 and skill02 == 4:
            s.append(("Сильфа (Lvl: 2) (SP:2)",
                _("Танец Падшего Ангела, усиленный силой ветра.\nВ дополнение к усилению уклонения от атак противника полностью устраняется шанс на промах.\nДемонстрирует наибольшую эффективность при встрече с ангелами.")))
        elif skill01 > 6 and skill02 == 5:
            s.append(("Сильфа (Lvl: 3) (SP:1)",
                _("Наделение себя могуществом Сильфы для максимального усиления Танца Падшего Ангела.\nСпособность уклоняться от большинства атак и атаковать дважды.\nНаибольший эффект очевиден при встрече с ангелами.")))

        if 7 < skill01 < 17 and skill03 == 1:
            s.append(("Gnome (Lvl: 1) (SP:2)",
                _("Borrows Gnome's power of the Earth.\nWith incredible strength, no bind is too strong.\nBut it's too difficult to control for attacking for now.")))
        elif skill01 > 7 and skill03 == 2:
            s.append(("Gnome (Lvl: 2) (SP:2)",
                _("The ability to take the breath of the earth itself into the body.\nDefense sharply rises, as does the chance for a critical hit.\nIn addition, binds are more easily broken.")))
        elif skill01 > 7 and skill03 == 3:
            s.append(("Gnome (Lvl: 3) (SP:1)",
                _("The ability to fill your own body with the power of the Earth.\nNearly immune to physical damage, and pleasure damage is reduced.\nAll attacks are critical hits, and binds can be broken easier.")))
        elif skill01 > 7 and skill03 == 4:
            s.append(("Gnome (Lvl: 2) (SP:2)",
                _("Due to being weakened, Gnome can't wield her previous powers.\nDefense improves, and so do the odds of delivering a critical strike.\nOf course, binds are more easily broken out of as well.")))
        elif skill01 > 7 and skill03 == 5:
            s.append(("Gnome (Lvl: 3) (SP:1)",
                _("The ability to fill your own body with the power of the Earth.\nNearly immune to physical damage, and pleasure damage is reduced.\nAll attacks are critical hits, and binds can be broken easier.")))

        if 9 < skill01 < 17 and skill04 == 1:
            s.append(("Undine (Lvl: 1) (SP:2)",
                _("Borrows Undine's power to create a barrier of water.\nThe damage reduction is not nearly as powerful as the Earth's.\nThis isn't the true power of water. Luka just can't use it right.")))
        elif skill01 > 9 and skill04 == 2:
            s.append(("Undine (Lvl: 2) (SP:ALL)",
                _("Entrust yourself to the flow, and attain a serene mind.\nOffense increased, and almost every attack is evaded.\nSP decreases by one every turn, until reaching zero.")))
        elif skill01 > 9 and skill04 == 3:
            s.append(("Undine (Lvl: 3) (SP:4)",
                _("Entrust yourself to the flow, and attain a completely serene mind.\nAttack power increased, and almost every attack is avoided.\nThere is no SP reduction, but it fades eventually.")))
        elif skill01 > 9 and skill04 == 4:
            s.append(("Undine (Lvl: 2) (SP:2)",
                _("Due to being weakened, Undine can't wield her previous powers.\nNormal attack power rises, and attacks can be evaded.\nVery effective against monsters.")))
        elif skill01 > 9 and skill04 == 5:
            s.append(("Undine (Lvl: 3) (SP:2)",
                _("Entrust yourself to the flow, and attain a completely serene mind.\nAttack power increased, and almost every attack is avoided.\nEffect is most visible against monsters.")))

        if 13 < skill01 < 17 and skill05 == 1:
            s.append(("Salamander (Lvl: 1) (SP:2)",
                _("Borrow Salamander's power to ignite your fighting spirit.\nPower is slightly increased, but you can't meditate.\nNot very useful, but isn't indicative of the true power of fire.")))
        elif skill01 > 13 and skill05 == 2:
            s.append(("Salamander (Lvl: 2) (SP:2)",
                _("A skill to ignite your fighting spirit to its max.\nSP is maxed out, and attack power is increased.\nWhen the effect fades, SP goes to zero.")))
        elif skill01 > 13 and skill05 == 3:
            s.append(("Salamander (Lvl: 3) (SP:4)",
                _("Infuse self with the purifying flames of purgatory.\nSP completely recovers, and attack power increases.\nMeditation is usable, and SP isn't drained after it fades.")))
        elif skill01 > 13 and skill05 == 4:
            s.append(("Salamander (Lvl: 2) (SP:4)",
                _("Due to being weakened, Salamander can't wield her previous powers.\nSP is maxed out, and attack power is increased.\nWhen the effect fades, SP goes to zero.")))
        elif skill01 > 13 and skill05 == 5:
            s.append(("Salamander (Lvl: 3) (SP:4)",
                _("Infuse self with the purifying flames of purgatory.\nSP completely recovers, and attack power increases.\nMeditation is usable, and SP isn't drained after it fades.")))

        if skill01 > 21 and skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0:
            s.append(("Призыв Четырёх Духов (SP:6)",
                _("Требует много очков навыков на исполнение, но способен призывать сразу всех духов одновременно.\nСтоит больше очков, но действует намного быстрее.")))

        return s

screen menu_skit(var=None):
    tag menu

    default descr = ""
    default page = 1

    if var == "item":
        $ T = "Предметы"

        if page == 1:
            $ B = get_battle_item()
            $ item_text = "Боевые"
        elif page == 2:
            $ B = get_valuables_item()
            $ item_text = "Ключевые"
        elif page == 3:
            $ B = get_other_item()
            $ item_text = "Остальные"
    else:
        $ T = "Навыки"
        if gm_char == 0:
            if page == 1:
                $ item_text = "Проклятый Меч"
            if page == 2:
                $ item_text = "Ангельские техники"
            if page == 3:
                $ item_text = "Силы стихий"
        $ B = get_skills()

    if renpy.variant('touch'):
        $ zoom_factor = 0.9
        $ YALIGN = 1.0
        $ ADD = "_n"
    else:
        $ zoom_factor = 1.0
        $ YALIGN = 0.5
        $ ADD = ""

    $ ROWS = len(B)
    add renpy.get_screen("bg", layer='master')
    add "gm_bg"

    frame:
        if persistent.inv == "old" and var == "item":
            style_group 'inv_but'
        elif persistent.inv == "new" or var == "skill":
            style_group 'skit_but'

        if var == "item":
            vpgrid:
                xfill True
                spacing 15
                rows ROWS

                if ROWS > 11:
                    scrollbars "vertical"

                draggable True
                mousewheel True

                for nm, val in B:
                    textbutton "[nm!t]" action SetScreenVariable("descr", val) style "item_button"

        if var == "skill":
            grid 2 1:
                xfill True
                yfill True
                spacing 5

                if gm_char == 0:
                    if page == 1:
                        vbox:
                            spacing 10
                            textbutton "Безмятежный Демонический Клинок (SP: 2)":
                                action SetScreenVariable("descr", "Приём Иайдо, используемый в состоянии безмятежности.\nНаносит гигантский урон, пока призвана Ундина.\nК тому же он может принудительно оборвать призыв Ундины.")
                                style "item_button"
                            textbutton "Сотрясающее Землю Обезглавливание (SP: 3)":
                                action SetScreenVariable("descr", "Приём, использующий силу земли для нанесения гибельного удара сверху.\nБезумно мощный.\nК тому же приём становится ещё мощнее, если при его использовании вызвана Гнома.")
                                style "item_button"
                            if skill01 > 25:
                                textbutton "Разгон (SP: 0)":
                                    action SetScreenVariable("descr", "Демоническая техника, заключающаяся в концентрации своей темной энергии и извлечении из неё духовной энергии.{w}\nПри первом использовании даёт 2 SP. Прирост возрастает на единицу при каждом последовательном использовании.")
                                    style "item_button"
                            if skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0:
                                textbutton "Элементальная Спика (SP: 10)":
                                    action SetScreenVariable("descr", "Концентрация мощи всех Четырёх Духов в правой руке.\nПриём опустошительной мощи, изученный с помощью Генриха.")
                                    style "item_button"
                        vbox:
                            spacing 10
                            textbutton "Молниеносный Удар Клинка (SP: 2)":
                                action SetScreenVariable("descr", "Искусство, использующее движение ветра как усиление для нанесения удара молниеносной скорости.\nНаносит дополнительный урон, если воспользоваться этим приёмом на первом ходу.\nСтановится ещё сильнее, если при этом вызвана Сильфа.")
                                style "item_button"
                            textbutton "Неудержимый Испепеляющий Клинок (SP: 4)":
                                action SetScreenVariable("descr", "Бесчисленное количество ударов, ещё и вдобавок усиленных очищающим пламенем.\nНевероятно могущественный приём, созданный Гранберией.\nЕго сила увеличивается ещё больше, если во время его выполнения вызвана Саламандра.")
                                style "item_button"
                            textbutton "Крайность (SP: 0)":
                                action SetScreenVariable("descr", "Рискованный и отчаянный приём, резко снижающий количество очков здоровья Луки.\nЛегчайшего касания может оказаться достаточно, чтобы вывести Луку из себя, когда он на волоске от поражения.") style "item_button"
                            if skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0:
                                textbutton "Четырёхкратный Гига-удар (SP: 1)":
                                    action SetScreenVariable("descr", "Ультимативный приём абсолютной, опустошающей мощи, использующий всех четырёх духов для уничтожения противника.\nЕсли во время четырёх ходов зарядки противник наносит удар, то приём проваливается.\nБолее того, во время начала исполнения приёма все вызванные духи исчезают.")
                                    style "item_button"

                    elif page == 2:
                        vbox:
                            spacing 10
                            textbutton "Мгновенное Убийство (SP: 2)":
                                action SetScreenVariable("descr", "Тайный приём падшего ангела, прорубающий насквозь само время и пространство.\nОчень большой урон за сравнительно малое количество очков навыков на его использование.\nПри определённых условиях может бить дважды.\nПохоже, что он может сочетаться с силой ветра...")
                                style "item_button"
                            textbutton "Возрождение Небесного Демона\n(SP: 4)":
                                action SetScreenVariable("descr", "Таинственный приём, являющий собой концентрацию магической энергии для уничтожения противников.\nНикому из тех, кто его видел, не посчастливилось дожить до момента, чтобы его описать.\nПохоже, что этот приём может сочетаться с силой земли...") style "item_button"
                            textbutton "Медитация (SP: 3)":
                                action SetScreenVariable("descr", "Лука концентрирует силу своего духа, восстанавливая очки здоровья без использования магии.\nВосстанавливает половину максимального количества очков здоровья Луки.{w}\nЛегендарный навык, используемый падшими ангелами.") style "item_button"
                            if skill02 > 0:
                                textbutton "Танец Падшего Ангела: Буря (SP: 2)":
                                    action SetScreenVariable("descr", "Наделение себя могуществом Сильфы для максимального усиления Танца Падшего Ангела.\nСпособность уклоняться от большинства атак и атаковать дважды.\nНаибольший эффект очевиден при встрече с ангелами.") style "item_button"
                            else:
                                textbutton "Танец Падшего Ангела (SP: 2)":
                                    action SetScreenVariable("descr", "Тайный приём, заключающийся в единении со святым течением и уклонении от атак противника.\nПохоже, что он довольно эффективен в битвах с ангелами и против статус-атак.") style "item_button"
                        vbox:
                            spacing 10
                            textbutton "Девятиликий Ракшаса (SP: 6)":
                                action SetScreenVariable("descr", "Целый шквал атак мечом, бушующий в танце смерти.\nЛегендарный приём, которым могут пользоваться лишь сильнейшие из ангелов.\nИ похоже, что он может сочетаться с силой огня...") style "item_button"
                            textbutton "Денница (SP: 8)":
                                action SetScreenVariable("descr", "Приём абсолютной мощи, созданный первородным падшим ангелом - Люцифиной.\nСлужит контратакой в ответ на удар противника, аннигилируя его.\nПохоже, что он, к тому же, может сочетаться с силой воды...") style "item_button"

                    elif page == 3 :
                        vbox:
                            spacing 10
                            if skill02 > 0:
                                textbutton "Сильфа (ур. 3) (SP: 1)":
                                    action SetScreenVariable("descr", "Наделение себя могуществом опустошающего урагана для увеличения скорости движения.\nСпособность уклоняться от большинства атак и атаковать дважды.") style "item_button"
                            if skill04 > 0:
                                textbutton "Ундина (ур. 2) (SP: ВСЕ)":
                                    action SetScreenVariable("descr", "Доверие себя течению и достижение состояния абсолютной безмятежности.\nУсиление атаки и уклонение почти от всех атак противника.\nКаждый ход отнимает одно очко навыков до тех пор, пока их количество не достигнет нуля.") style "item_button"
                            if skill05 > 0:
                                textbutton "Саламандра (ур. 3) (SP: 4)":
                                    action SetScreenVariable("descr", "Наделение себя пламенем искупительного очищения.\nМгновенное восполнение очков навыков до максимума и значительное увеличение мощности атаки.\nСпособность при этом пользоваться Медитацией, и, когда спадает эффект, количество очков навыков не опускается до нуля.") style "item_button"
                            if skill_refresh > 0:
                                textbutton "Очищение (SP: 2)":
                                    action SetScreenVariable("descr", "Техника очищения организма, выученная от Ундины и работающая только при её призыве.\nВосстанавливает немного здоровья и выводит токсины из тела.") style "item_button"
                        vbox:
                            spacing 10
                            if skill03 > 0:
                                textbutton "Гнома (ур. 3) (SP: 1)":
                                    action SetScreenVariable("descr", "Способность наполнить всё своё собственное тело могуществом Земли.\nПочти полная неуязвимость к физическому урону и ослабление урона от секс-приёмов.\nВсе атаки наносят критический урон, а вырваться из сковываний становится ещё легче.") style "item_button"
                            if skill04 > 0:
                                textbutton "Ундина (ур. 3) (SP: 2)":
                                    action SetScreenVariable("descr", "Доверие себя течению и достижение состояния безмятежности.\nУсиливает атаку и шанс на уклонение от атак противника.\nНаибольший эффект проявляется при использовании против монстров.") style "item_button"
                            else:
                                textbutton "Безмятежный разум (SP: 2)":
                                    action SetScreenVariable("descr", "Состояние Безмятежности достижимо даже без Ундины.\nПовышается сила атаки и шанс на уклонение.\nПохоже, что оно довольно-таки эффективно против монстров.") style "item_button"
                            if skill02 > 0 and skill03 > 0 and skill04 > 0 and skill05 > 0:
                                textbutton "Призыв Четырёх Духов (SP: 6)":
                                    action SetScreenVariable("descr", "Требует много очков навыков на исполнение, но способен призывать сразу всех духов одновременно.\nСтоит меньше очков, однако от призыва Саламандры SP восстанавливается.") style "item_button"
                elif gm_char == 1:
                    vbox:
                        spacing 10
                        textbutton "Омега Блейз (SP: 4)":
                            action SetScreenVariable("descr", "Высвобождает пламененный шторм, сжигающий врагов до пепла.\nВ течении несколько ходов цель будет гореть.")
                            style "item_button"
                        textbutton "Террор Владыки Монстров (SP: 6)":
                            action SetScreenVariable("descr", "Ультимативный приём семьи Фейтбёрн, предеющийся из поколение в поколение.\nСила тьма разрывает противника на части.\nНаносит трижды тяжёлый урон.")
                            style "item_button"
                    vbox:
                        spacing 10
                        textbutton "Фрост Озма (SP: 4)":
                            action SetScreenVariable("descr", "Превращает врага в кусок ледышки.\nВ течении несколько ходов цель будет заморожена и обездвижена.")
                            style "item_button"
                        textbutton "Взгляд восстановления (SP: 8)":
                            action SetScreenVariable("descr", "Алиса использует свой магический взгляд для исцеления Луки.\nПолностью восстанавливает здоровье Луки и снимает все наложенные статус-эффекты.")
                            style "item_button"

                elif gm_char == 4:
                    vbox:
                        spacing 10
                        textbutton "Атака хвостом (SP: 4)":
                            action SetScreenVariable("descr", "Наделяя свой пушистый хвост силой Земли, Тамамо бьёт им противника.")
                            style "item_button"
                        textbutton "Девять Лун (SP: 5)":
                            action SetScreenVariable("descr", "Тайный приём Тамамо, ударяющий врага всеми девятью хвостами.")
                            style "item_button"
                        textbutton "Ангельский Поцелуй (SP: 6)":
                            action SetScreenVariable("descr", "Скопированный приём суккубов, восстанавливающий с помощью поцелуя половину здоровья Луки.")
                            style "item_button"
                        textbutton "Мгновенное Убийство: Землетрясение (SP: 5)":
                            action SetScreenVariable("descr", "Тамамо объединяется с Лукой для создания быстрого и мощного навыка меча.\nУсиленный магией Тамамо, Лука атакует Мгновенным Убийством\n Прорывает вражескую защиту.")
                            style "item_button"
                    vbox:
                        spacing 10
                        textbutton "Удар Тамамо (SP: 3)":
                            action SetScreenVariable("descr", "Тамамо бьёт своим фирменным ударом, весящим словно скала.")
                            style "item_button"
                        textbutton "Лунная Пушка (SP: 8)":
                            action SetScreenVariable("descr", "Тамамо, используя энергию луны, выстреливает потоком фотонов.")
                            style "item_button"
                        textbutton "Сдавливание Хвостами (SP: 8)":
                            action SetScreenVariable("descr", "Тамамо обволакивает противника своими хвостами.\nОбездвиживает цель на некоторое время, а также наносит урон.")
                            style "item_button"
                        textbutton "Слово Рассеивания (SP: 32)":
                            action SetScreenVariable("descr", "Заклинание, способное вернуть Тамамо её истинный облик и силу.\nНе совсем ясно, при каких условиях его можно использовать.")
                            style "item_button"
    
    if persistent.inv == "old" and var == "item":
        label '[descr!t]' style 'inv_descr'

    elif persistent.inv == "new" or var == "skill":
        label '[descr!t]' style 'skit_descr'

    if var == "item":
        frame:
            style_group 'pages'
            xsize 300

            side "l c r":
                xfill True

                imagebutton:
                    auto "left_%s" + ADD
                    at xyzoom(zoom_factor)
                    yalign YALIGN

                    action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', 3)), SetScreenVariable("descr", "")]

                label "[item_text!t]"

                imagebutton:
                    auto "right_%s" + ADD
                    at xyzoom(zoom_factor)
                    yalign YALIGN

                    action [If(page < 3, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), SetScreenVariable("descr", "")]

        key "K_PAGEDOWN" action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', 3)), SetScreenVariable("descr", "")]
        key "K_PAGEUP" action [If(page < 3, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), SetScreenVariable("descr", "")]

    if var == "skill" and gm_char == 0:
        frame:
            style_group 'pages'
            xsize 300

            side "l c r":
                xfill True

                imagebutton:
                    auto "left_%s" + ADD
                    at xyzoom(zoom_factor)
                    yalign YALIGN

                    action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', 3)), SetScreenVariable("descr", "")]

                label "[item_text!t]"

                imagebutton:
                    auto "right_%s" + ADD
                    at xyzoom(zoom_factor)
                    yalign YALIGN

                    action [If(page < 3, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), SetScreenVariable("descr", "")]

        key "K_PAGEDOWN" action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', 3)), SetScreenVariable("descr", "")]
        key "K_PAGEUP" action [If(page < 3, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), SetScreenVariable("descr", "")]

    use title(T)
    use return_but("gm")

style skit_but_frame:
    background Frame("images/System/win_bg_0.webp", 5, 5)
    xfill True
    yfill True
    margin (20, 60, 20, 195)
    padding (5, 10)

style skit_descr:
    is label
    background Frame("images/System/win_bg_0.webp", 5, 5)
    xfill True
    yfill True
    margin (20, 405, 20, 60)
    padding (5, 10)

style skit_descr_text:
    is label_text
    size 19
    outlines [(2, '#000c')]
    font "fonts/archiform.otf"
    color "#fffacd"
    line_spacing 4

style inv_but_frame:
    background Frame("images/System/win_bg_0.webp", 5, 5)
    xfill True
    yfill True
    margin (20, 60, 295, 60)
    padding (5, 10)

style inv_descr:
    is label
    background Frame("images/System/win_bg_0.webp", 5, 5)
    xfill True
    yfill True
    margin (515, 60, 20, 60)
    padding (5, 10)

style inv_descr_text:
    is label_text
    size 19
    outlines [(2, '#000c')]
    font "fonts/archiform.otf"
    color "#fffacd"
    line_spacing 4

style item_button:
    is default
    background None

style item_button_text:
    is gm_button_text
    size 20
    kerning 0

style skit_but_vscrollbar:
    is pref_vscrollbar
