label first_hansei:
    show ilias st02 with dissolve #700

    i "So you have come, Luka... Now then, on to the evaluation...{w}{nw}"

    show ilias st06 #700
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\n...Oh my, what's with that angry glare?"

    show ilias st05 with dissolve #700

    i "I may appear the same, but I am different from the Ilias you have seen until now.{w}\nYou could call me Ilias's conscience, if you so wish..."

    show ilias st03 with dissolve #700

    i "In the dirty, polluted river of filth that is her heart...{w}\nA tiny glimmer of conscience that still exists, still free of the filth."

    show ilias st02 with dissolve #700

    i "...That is me.{w}\nI will guide you from now on. Surely you don't object?"
    i "Now then, let us begin with the evaluation meeting..."

    show ilias st04 with dissolve #700

    i "...To begin with you...{w}\nYou are really pathetic."

    $ hansei_first = 1
    return


label cupid_hansei:
    $ persistent.count_hanseikai2 += 1
    call first_hansei 

    i "You gave the orgasm treatment to an angel drowning in lust...{w}\nHow would you like to be reincarnated with a vibration feature this time?"

    show ilias st01 with dissolve #700

    i "You, the master of the Four Spirits, should have no issues with Cupid.{w}\nBut make sure you keep up appropriate defenses, as she has some nasty tricks."
    i "If she mounts you, you cannot escape her unless you have Gnome summoned.{w}\nAs the danger of guaranteed loss is always there, make sure Gnome is always available."
    i "In addition, the abnormal status she inflicts is avoidable with either Sylph or Undine.{w}\nEither works, so just make sure one is up."
    i "After that, she should be easy with just your normal techniques.{w}\nAs always, watch your HP to ensure it stays above half."
    i "Lastly, if you should lose to her Temptation skill, she may perform a special insult to you.{w}\nThe reason for me telling you... Surely you realize it."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDye that pink-brained lustful angel's face into one of blood red."

    return


label valkyrie_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 
    show ilias st04 with dissolve #700

    i "20,000 years of punishment?{w}\nI'm sure that is more of a great reward, to you."
    i "Rather, I think the only one being punished here is Valkyrie...{w}\nSpending twenty years thinking she's punishing someone, when in fact he's enjoying it."

    show ilias st01 with dissolve #700

    i "Undine is very effective against Valkyrie.{w}\nIn particular, it has a high chance of evading her Breast Comfort move."
    i "In addition, Gnome is needed for escaping from her restraints.{w}\nIf you don't break free of her binds when she's nude, you could lose in one turn."
    i "If you fight safely, you shouldn't run into an issue.{w}\nJust don't neglect to recover as needed."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou should rape Valkyrie and punish her for the next 20,000 years now."
    i "...Forgive me. That's a rather long time to gloat just for getting revenge."

    return


label ariel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Even when your opponent is an Archangel, the result is the same.{w}\nFood to be consumed again... It seems you simply will never escape from that designation."

    show ilias st01 with dissolve #700

    i "Sylph will help with evading Ariel's attacks, especially the electric shock."
    i "On the other hand, Undine is not effective at all.{w}\nShe is not a transient being, so sensing her with Undine will not be easy."
    i "In addition, her attack power is very high. Gnome should be used to reduce the damage."
    i "Use up those spirits before you toss them away like garbage.{w}\nKill two birds with one stone, and overwork Gnome and Sylph while you defeat Ariel."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou yourself should bring the wrath of Heaven to that arrogant Archangel."

    return


label c_s2_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Prey of an experimental animal this time...{w}\nI know you love both tentacles and mucus, so you must be very happy with yourself."

    show ilias st01 with dissolve #700

    i "Without the spirits, you'll run into a little trouble.{w}\nMake sure you keep an eye on your health so you can recover."
    i "It's basic, but make sure to escape from her bind right away.{w}\nIf you wait, her Bubble Heaven will make short work of you."
    i "She's an irritating opponent, but shouldn't pose any trouble for you.{w}\nThose spirits were annoying anyway."
    i "If you lose to Bubble Heaven, she'll continue playing with you even after you lose."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nHurry up and dispose of that failure of an organism."

    return


label c_a3_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Squeezed to death by that failure of an angel...{w}\nYou yourself are like an experimental organism, aren't you?"

    show ilias st01 with dissolve #700

    i "Your tactics are still limited at this moment.{w}\nJust fight carefully, keeping an eye on your health."
    i "But watch out if she prepares a move...{w}\nIf you don't guard it, you will surely lose."
    i "In addition, if you lose to a certain move, she'll continue violating you even after your loss..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nAn ugly angel like that is not needed.{w} Dispose of her."

    return


label stein1_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Research like that is complete heresy.{w}\nThere's some use for it, but..."
    i "...Excuse me, you don't need to hear about that.{w}\nI, the pure Ilias, shall guide you."

    show ilias st01 with dissolve #700

    i "During this fight, you should regain your serene state of mind.{w}\nOnce you do, make sure to never lose it."
    i "With that, you'll be able to avoid most attacks...{w}\nBut if they hit, they are painful, so always keep an eye on your health."
    i "In addition, after she summons the wind spirit, she will use a powerful attack.{w}\nMake sure to guard if you wish to survive it."
    i "Since your opponent has a large amount of health, expect a long fight.{w}\nKeep an eye on your SP, and make sure you always have enough to recover and keep your serene state."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't hesitate to beat that great sinner to a pulp."

    return


label ranael_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Failures.{w} Failures everywhere, all over the place.{w}\nAnd the damn Hero is the biggest failure of all.{w} How many times will he come here before he's satisfied?"
    i "...Oh no, just ignore that."

    show ilias st01 with dissolve #700

    i "In this fight, a new power will awaken in you.{w}\nYou may particularly like the counter skill."
    i "But your serene state may not work too well, since Ranael is an angel.{w}\nInstead, you should use your new Fallen Angel Dance."
    i "It's avoidance is particularly effective against angels.{w}\nIf you face more in the future, you should keep this in mind."
    i "In addition, if you are bound you must struggle right away.{w}\nAny delay will lead to your doom."
    i "You must also be sure to guard if you sense her powering up her special skill.{w}\nAgain, if you don't, you will surely lose."
    i "There's an additional insulting scene if you lose by that move, but...{w}\nActually, at this point, I don't even care if you see it."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nUse that hideous being as a guinea pig for your new skills."

    return


label c_tangh_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Retiring so you can play with some tongues and saliva?{w}\nDidn't your parents ever tell you not to mouth off?"

    show ilias st01 with dissolve #700

    i "Your serene state of mind is very effective against her."
    i "The Fallen Angel Dance is more effective against angels...{w}\nKeep this distinction in mind for the future battles."
    i "Chimera Tongue's binding attacks are very strong.{w}\nYou have a high chance of evading it with your serene state, so make sure to keep it up at all times."

    show ilias st02 with dissolve #700

    i "Now go, oh melts-in-your-mouth Luka.{w}\nRip off every one of those tongues of hers."

    return


label angels_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Gang-raped by angels...{w}\nFor a breast-lover like you, that must have been quite an unexpected reward."

    show ilias st01 with dissolve #700

    i "The angels cooperative attacks can be very powerful.{w}\nLarge damage is likely, but you should be able to survive with fallen angel dance."
    i "But their HP isn't too high either...{w}\nYou may be able to simply power through them before they defeat you."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nBreasts are meant to be milked, not be the milkers..."

    return


label nagael_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Did you enjoy your torment by snakes?{w}\nI know how much you love things coiling around you, after all..."

    show ilias st01 with dissolve #700

    i "Since your opponent is an angel, use fallen angel dance.{w}\nYou'll be able to avoid her binding attacks with a high probability."
    i "Her HP is high, however, so plan for a drawn out fight.{w}\nKeep an eye on your SP to make sure you can always recover."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTurn that evil angel back into raw holy energy."

    return


label c_tentacle_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Prey to tentacles yet again...{w}\nDo you have some innate desire to be wound up whenever you see tentacles?"

    show ilias st01 with dissolve #700

    i "Since your opponent is a monster, you should use serene mind.{w}\nIf you fight carefully, you shouldn't have any issues."
    i "Her restraint is powerful, and there's no Gnome to break free quickly.{w}\nYou'll just have to keep struggling to get free..."
    i "In addition, you're now able to employ the Monster Lord as your ally in battle.{w}\nOnce per fight, she can use a recovery move or one of three attack moves."
    i "Use them on a case-by-case basis, depending on the enemy.{w}\nFor the Chimera Tentacle, any of her skills are effective."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCut off every tentacle from that hideous monster."

    return


label mariel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Defeated by a loli angel like that...{w}\n...Wait, you didn't lose BECAUSE she was a loli angel, did you!?"

    show ilias st01 with dissolve #700

    i "Mariel's high HP means this will be a drawn out fight.{w}\nMake sure to never lose your fallen angel dance."
    i "Since she's an angel, the Monster Lord's attacks won't work.{w}\nJust stick to her recovery move."
    i "In addition, if you're defeated while bound, a different defeat scene will occur.{w}\nSince you love lolis so much, I'm sure you'll find it."

    show ilias st02 with dissolve #700

    i "Now go, oh lolicon lover Luka.{w}\nYou should also get rid of those lolicon soldiers, too."

    return


label c_medulahan_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "A Medusa and a Dullahan combined?{w} How horrifying.{w}\nBut I know how you love those things that slither..."

    show ilias st01 with dissolve #700

    i "Her double attacks are quite strong.{w}\nSince she is a monster, you should use the serene state to avoid damage."
    i "In addition, if she petrifies you, you will lose for sure.{w}\nYou can avoid that for sure with a serene state, so make sure to always have it up."
    i "In addition, since she is part snake she is weak to cold.{w} Use the Monster Lord's ice attack.{w}\nAnd in the future, you should remember that snakes are weak to cold attacks."
    i "In addition, something extra may happen if you lose in a certain way...{w}\nThough I don't know what.{w} And I don't recommend you find out."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTry not to let ugly monster's blood like that rust your sword."

    return


label trinity_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Did the cross of pleasure feel good?{w}\nI'm sure the gospel of your stupidity will be spread to the world.{w} That's what usually happens with those who ascend on the cross, right?"

    show ilias st01 with dissolve #700

    i "Their combination plays are powerful, so fallen angel dance is necessary.{w}\nTheir restraint technique is very powerful, but there's no way around it but to struggle."
    i "In addition, since your opponent is an angel, the Monster Lord's attacks won't do any good.{w}\nJust use her ability for restoration."
    i "Lastly, there is an additional insult performed if you lose to their restraint combination...{w}\nIf you purposefully lose and pledge yourself to me that way, it would make me happy."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTo be perfectly honest, I don't mind if the harpy's village is destroyed."

    return


label c_bug_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Was it fun making children with an insect like that?{w}\nI hear having a large family makes a man happy..."

    show ilias st01 with dissolve #700

    i "Since she's a monster, the serene state is most effective.{w}\nIn addition, make sure to struggle right away if bound."
    i "Since she's an insect-type monster, flame is very effective."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEradicate every insect you see on the surface."

    return


label sisterlamia_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Did it feel good to die in a Lamia's embrace?{w}\nIt looked so good, my mouth was agape the whole time."

    show ilias st01 with dissolve #700

    i "Since she is a monster, a serene state is effective.{w}\nBut her attack patterns are very troublesome, unlike most monsters..."
    i "First, you must never attack when her breasts are exposed.{w}\nIf you do, you'll be guaranteed a loss."
    i "In addition, she has a chance of using a troublesome magical eye move.{w}\nIf you want to avoid that, you'll need to switch to fallen angel dance right away."
    i "Another problem arises if she summons an earth spirit.{w}\nIf she gets you in a bind after summoning her, then you won't be able to break free."
    i "The opposite to earth is wind...{w}\nBut you don't have Sylph at the moment."
    i "So you'll have to use the next best thing, fallen angel dance.{w}\nIt has a wind property to it, so it will let you dodge her binding move."
    i "I'll tell you more about opposite attributes later...{w}\nThe battles will only get more and more difficult from here on out."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIn the end, monsters will always simply be monsters.{w} The only thing they deserve is the wrath of the Heavens."

    return


label muzukiel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Ahh, what an ugly angel.{w}\nI really thought she was a monster the first time I laid eyes on her."

    show ilias st01 with dissolve #700

    i "Muzukiel's attack power is high, so be sure to use fallen angel dance.{w}\nHer binding skill is also very damaging, but there's nothing you can do but struggle out of it."
    i "Also, if she prepares something, make sure you guard.{w}\nIf you're in fallen angel dance and guarding, you'll be able to dodge it every time."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nI think that angel is ugly.{w} So destroy her without mercy."

    return


label mermaid_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Did you become the Mermaid's toy?{w}\nI guess you're already mostly everyone's toy at this point, though."

    show ilias st01 with dissolve #700

    i "Since the Mermaid is a monster, use serene mind.{w}\nThe problems come in when she summons that water spirit..."
    i "For a number of turns, her attacks become twice as powerful.{w}\nThere's nothing you can do at this point but to Guard through it."
    i "In addition, after she summons the spirit she can try to sing a song.{w}\nFallen angel dance will let you completely evade it."
    i "She can only summon the spirit for a short while, so you could also try to power through it.{w}\nIt's a risky tactic, but it could work out."
    i "Since she is a water-based monster, the Monster Lord's fire magic is very effective.{w}\nIf you get a chance, turn her into grilled seafood."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nMermaids are fish after all.{w} I recommend grilling or frying her."

    return


label g_mermaid_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Made helpless by a Mermaid?{w}\nWho am I kidding, even when you're not helpless you still love being used like a pet."

    show ilias st01 with dissolve #700

    i "Basically, the Mermaid General is just an enhanced version of the previous enemy.{w}\nA serene state is pretty much required to have any chance of avoiding her thunder attack, too."
    i "If you're bound, make sure to struggle for sure.{w}\nIf you don't, you will be raped and lose the following turn."
    i "Just as before, this Mermaid can summon a spirit of water.{w}\nShe can't maintain it for long, so perhaps powering through it may work?"
    i "But if all else fails, Guard through it.{w}\nAnd don't forget to grill her in the Monster Lord's fire."
    i "In addition, there's an additional scene if you lose to her rape attack.{w}\nThe wrath of the Heavens will come down on those who immediately go to view it."

    show ilias st02 with dissolve #700

    i "Now go, oh helpless Luka.{w}\nBy the way, you should also kill the other Mermaids in Natalia Port while you're at it."

    return


label ningyohime_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Prey to the Mermaid Princess now...{w}\nYou were fertilizer before, now you're fish food.{w} An all-purpose Hero, aren't you?"

    show ilias st01 with dissolve #700

    i "El is not too powerful of a monster...{w}\nBut the effect of your serene state isn't as powerful against her, either."
    i "Though that doesn't mean not to use it at all.{w}\nThe trigger rate is low, but it's better than nothing."
    i "Also, make sure to struggle if bound.{w}\nIf not you will be raped."

    show ilias st02 with dissolve #700

    i "Now go, oh multi-use Luka.{w}\nPlease, every once in a while show me an ending that doesn't result in you being preyed on..."

    return


label queenmermaid_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Hero Luka...{w} You decided to take a new role as a young girl's learning toy?{w}\nHow wonderful...{w} I didn't realize you cared so deeply about the education of this generation's youths."

    show ilias st01 with dissolve #700

    i "The Queen Mermaid's attack power is very high.{w}\nWithout your serene state, you won't be able to hold out."
    i "In addition, she gets even more powerful when she summons her water spirit.{w}\nAbandon your gung-ho style, and focus on defense in those cases."
    i "She is weak to fire, so baking her may be good...{w}\nBut saving the Monster Lord for an emergency heal may be wise, too."
    i "In addition, there's a separate scene for losing as she rapes you.{w}\nI hope that raises your tension for battle a little higher."

    show ilias st02 with dissolve #700

    i "Now go, oh learning toy Luka.{w}\nKill the Mermaid Queen, and send the rest of the Mermaids into despair."

    return


label shadow_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "So you were incorporated into that strange shadow's body...{w}\nCongratulations on your debut in the world of spirits."

    show ilias st01 with dissolve #700

    i "Due to her nature, the serene state isn't too effective against the Shadow Girl...{w}\nSince her attack power is also rather high, she's a fairly difficult opponent."
    i "But due to her low HP, an all-out attack is your best bet for beating her."
    i "Her defense against magic is high, too, so the Monster Lord's attacks won't do much."
    i "Flame is the most effective of the three, but using her for emergency recovery may be the best use."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTurn that shadow monster back into a normal shadow."

    return


label gaistvine_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "This time it's your art debut.{w}\nShall I display you in a museum, so everyone can point and laugh at your stupidity?"

    show ilias st01 with dissolve #700

    i "Since your opponent is a monster, use your serene state.{w}\nBut hold back when she goes into her picture."
    i "You'll lose instantly if you attack when she does this.{w}\nWhen she's in her picture, she will also still use eye magic..."
    i "But this status effect is completely avoidable with fallen angel dance.{w}\nWhen she goes into her picture, make sure to change to that."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nA painting like that needs to be completely destroyed."

    return


label chrom2_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "A victim to the Necromancer again...{w}\nYou really love zombies and ghosts, don't you?"
    i "I'm pretty sure that they are your best friends at this point.{w}\nAnd friendship is a beautiful thing..."

    show ilias st01 with dissolve #700

    i "Since they are monsters, your serene state is most effective.{w}\nYou can also avoid their instant kill attack with it."
    i "Her troublesome skill is her injection...{w}\nNot only is it damaging, but the name is pretty embarrassing."
    i "Fallen angel dance will let you avoid it completely...{w}\nBut since the instant kill attack is more troublesome, you should keep serene state up."
    i "So just accept that you'll have to take that attack.{w}\nIn preparation, keep your HP up and your SP ready to heal after."
    i "Lastly, if you lose while being raped by the ghosts, there's an additional insulting scene."
    i "I'll just make another Luka, so go ahead and do whatever. I don't care."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIf you lose again this time, I'll give you an injection."

    return


label berryel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "So you were preyed upon by a greedy angel I see.{w}\nJust who turns into prey for a fruit to eat?{w} I mean honestly."

    show ilias st01 with dissolve #700

    i "Since your opponent is an angel, fallen angel dance is a given.{w}\nThe troubles come in with her multiple binding moves."
    i "Her milking move is a normal bind, and should be dealt with as usual."
    i "But when she is about to swallow you whole, you must attack her to break free."
    i "Be sure to pay close attention to the moment you are bound to see the distinction between them."
    i "Lastly, she's an angel, so the Monster Lord's attacks won't work.{w}\nUse her like a cheap generic medicine herb."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nPluck that sweet fruit's life without mercy."

    return


label revel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "You who should have been reborn, instead came here without being reborn?{w}\nI am surprised myself."

    show ilias st01 with dissolve #700

    i "Revel only has powerful binding techniques, so you must escape as quick as you can."
    i "Her most powerful move that requires preparation can only be avoided with fallen angel dance and guarding.{w}\nAlso, your counter skill is not very good for this battle."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\n...By the way, what do you want to be in the next world?"

    return


label carmilla_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Sheesh, I let my eyes off of you for a moment, and you dry up like a fish.{w}\nYou're just like a worm on the side of the road."

    show ilias st01 with dissolve #700

    i "Since Carmilla is a monster, serene state is useful.{w}\nOnce bound, struggle right away."
    i "You can completely evade her ecstasy attack with fallen angel dance...{w}\nBut due to the large effects of your serene state on other attacks, that is preferable."
    i "The problem comes in when she summons a wind spirit...{w}\nWhen she summons it, she will attack multiple times for large damage."
    i "This is the start of a new basic trend for your future battles.{w}\nIf an enemy summons a spirit of a type, you generally want to summon the opposite spirit attribute."
    i "In this case, Gnome is the opposite attribute, and will significantly reduce the damage you take."
    i "But once her spirit fades, don't forget to go back to your serene state, as that will have a larger damage reduction effect."
    i "In general, vampires are weak to fire.{w} So don't forget to roast her in fire."
    i "Lastly, if you're defeated while being raped, some additional insult may occur...{w} Of course that isn't important."

    show ilias st02 with dissolve #700

    i "Now go, oh dried fish Luka.{w}\nDeliver divine justice to that hideous Vampire with your sword."

    return


label elisabeth_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Dried up like a fish?{w} Again!?{w}\nDo you wish to just skip to the end and be reincarnated as a beached fish from now on?"

    show ilias st01 with dissolve #700

    i "Though she's a monster and you may want to use a serene state...{w}\nHer binding move is very strong, and is hard to break free in a normal state."
    i "With Gnome, you can break free more easily from it.{w}\nFallen angel dance will let you dodge her ecstasy move, but Gnome should take precedence."
    i "But watch out when she herself summons the spirit of earth.{w}\nIn that case, not even Gnome will let you break free of her bind."
    i "You will want to use the opposite wind-based fallen angel dance to completely avoid it."
    i "In addition, if you are beaten by her special skill, you will be subjected to further insults...{w}\nIt would be rather unbecoming to be played with by such a small girl, though."

    show ilias st02 with dissolve #700

    i "Now go, oh dried fish Luka.{w}\nRemove the vampires from Sabasa, and bring harmony to the land."

    return


label queenvanpire_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "With your opponent as the Queen Vampire...{w} Of course you would end up as a dried fish again.{w}\nI see a beached whale near Port Natalia that wants to be your friend."

    show ilias st01 with dissolve #700

    i "The Queen Vampire is a powerful enemy with a large variance in attack patterns.{w}\nFor normal states, a serene state is best to avoid her attacks."
    i "When she opens her cloak, make sure never to attack.{w}\nIf you do, you will instantly lose."
    i "In addition, she can also summon the spirits of wind and earth.{w}\nWhen she does, counter with the opposite attributes of each."
    i "Make sure you avoid her powerful binding technique with the power of wind when she summons the earth spirit."
    i "Furthermore, there are additional insults to be suffered from two attacks of hers...{w}\nWhy not see them?{w} I mean, you're going to end up a dried fish no matter what, I've noticed."

    show ilias st02 with dissolve #700

    i "Now go, oh dried fish.{w}\nDefeat the Queen Vampire, and annihilate that disgusting race."

    return


label c_homunculus_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Today's youth has been avoiding the sciences too much...{w} But that seems to be no issue for you.{w}\nI'm glad you were able to get along so well with the product of a biology experiment."

    show ilias st01 with dissolve #700

    i "The opponent is a monster, but a serene state won't do much to avoid the damage."
    i "Since her offensive ability is high, Gnome will do well in reducing damage.{w}\nGnome will also let you break free of binds quickly, so she is the most useful."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIt's fine to be interested in the sciences, but please fight properly some of the time."

    return


label ironmaiden_k_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "So you are a torture device lover.{w} But don't normal people hate those kinds of things?"
    i "...Of course I know you are far from normal.{w} But please, try to act like a sensible person at least some of the time?"

    show ilias st01 with dissolve #700

    i "The serene state doesn't have much of an effect on this monster.{w}\nSince her attack power and binds are strong, Gnome should be used."
    i "In addition, if she uses her sadistic move when you are bound, you will surely lose.{w}\nMake sure you always have Gnome up so you can escape as quick as possible."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDo not let any more people be executed by a torture device."
    i "...Perhaps that saddens you, but normal people will be happy about that."

    return


label lusia_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Yes, good job.{w} How does it feel coming here after that happened, acting like nothing is wrong?{w}\nYou may be embarrassed, but I'm too ashamed for you to say anything else on the matter."

    show ilias st01 with dissolve #700

    i "Since she's a monster, a serene state is most effective in her normal state.{w}\nThe trouble comes in with the other attributes.{w} Don't forget to adjust as she summons spirits."
    i "In addition, the Monster Lord's attacks aren't too powerful here.{w}\nYou may want to save her for recovery."
    i "Lastly, if you are beaten by her predation move, there is a different result...{w}\nIf you're going to lose, I really don't care whether you're burned, roasted or whatever."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that disgusting girl and save the village."

    return


label silkiel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "An angel wrapped you up in a cocoon?{w} You sure do manage to find a variety of ways to die, don't you?{w}\nDo you realize just how hard it was to reclaim all the bits of your soul that cocoon dissipated?"

    show ilias st01 with dissolve #700

    i "Your opponent is an angel, so fallen angel dance is effective.{w}\nAn instant loss technique is also avoidable with it, so be sure it's up."
    i "You can more easily break from her binds with Gnome, but her instant kill technique is more dangerous."
    i "In addition, watch for a counterattack pose being taken. If you attack her during it, you can expect to lose."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nKnock that angel down into hell."

    return


label endiel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Prey to a horrible, indecent angel...{w}\nShall I send you and Endiel both to hell instead...?"

    show ilias st01 with dissolve #700

    i "Your opponent is an angel, so fallen angel dance is effective...{w}\nBut without Gnome, you risk losing to her binding move."
    i "Gnome would be the safer way to go, so you should summon her first thing."
    i "When she takes up a counter stance she can use an incredibly powerful move that can only be dodged with fallen angel dance.{w}\nMake sure to use it, and not to attack her until she is back to normal."
    i "In addition, if you lose to her requiem move, you will be further insulted.{w}\n...Do whatever you want, I don't care anymore."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDrop that fallen angel into hell."

    return


label lamianloid_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "You seem to be enthralled by her forty-eight squeezing units.{w}\nSince you went to such effort to feel them all, by all means don't be in any hurry to finish up."

    show ilias st01 with dissolve #700

    i "Her attack damage is high, so be sure to avoid it with a serene state.{w}\nAs long as you keep that up, there's no other skill to be particularly careful of."
    i "In addition, she is an ugly snake monster, so ice is effective."
    i "There is also an additional ending scene if you are defeated by a certain attack...{w}\nI don't particularly care how you meet your end..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nFight against her forty-eight units with your forty-eight genital..."

    show ilias st04 with dissolve #700

    i "...I feel sick just from imagining something like that.{w}\nGo on, get out of here."

    return


label knightloid_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Defeated by a man made monster, right in public view?"
    i "How did you like being a public spectacle?{w}\nPerhaps you need a ring name...{w} How does Luka Magnum sound?"

    show ilias st01 with dissolve #700

    i "The first half of the battle is primarily physical in nature.{w}\nIf you challenge her head on, you'll lose in no time."
    i "But if you use your serene state, you should be able to avoid most attacks.{w}\nSave your SP for later as you slowly chip away at her health for the first half."
    i "In addition, you won't have any issue if she uses a fire spirit, as serene state is water based."
    i "The problems arise in the second half, where she uses pleasure attacks.{w}\nIf she binds you without Gnome, your defeat is assured."
    i "But if she summons a fire spirit again, you will need to go back to a serene state to survive."

    show ilias st02 with dissolve #700

    i "Now go, Luka Magnum.{w}\nDefeat that foolish Knight in full view of the public."

    return


label akaname_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Oh, so you became the prey of a low class group of monsters...{w}\nNo matter where you go, you never change, do you?"

    show ilias st01 with dissolve #700

    i "Since there are three of them, you'll take a lot of damage by sheer number of attacks.{w}\nUse your serene state to limit it, even if just a little bit."
    i "Her ecstasy attack can be completely avoided with Sylph, and her bind broken with Gnome...{w}\nBut using a serene state to avoid damage is probably the best route to take."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nOnce you get tired of being licked, please fight properly."

    return


label mikolamia_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st02 with dissolve #700

    i "You love Miko!{w}\nI love Miko!{w}\nEveryone loves Miko!"

    show ilias st04 with dissolve #700

    i "The disguise that Lamia took for herself... Disgusts me."

    show ilias st01 with dissolve #700

    i "Her restraint attack is very powerful.{w}\nWithout Gnome, your defeat is assured if she catches you."
    i "But if she herself calls a spirit of earth, you won't be able to escape even with Gnome.{w}\nYou'll have to call Sylph, to avoid it altogether."
    i "In addition, you will be insulted further if you are defeated by a certain attack...{w}\nDo whatever you want."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIt's a time-honored tradition to cut off the head of a fallen medium."

    return


label kezyorou_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Sheesh, coiled in hair this time?{w}\nIt's nice to see you continuing your trend of being wound up in anything of sufficient length..."

    show ilias st01 with dissolve #700

    i "If you're bound in her hair, she can use an instant kill move.{w}\nIt takes time to escape from even with Gnome, so be aware."
    i "It's avoidable altogether with Sylph or a serene state, though.{w}\nSince she's a monster, serene state would be preferred."
    i "If she calls a spirit of wind herself, she'll launch continuous attacks.{w}\nMake sure to summon Gnome to protect yourself from her barrage."
    i "Lastly, if you're defeated as she rapes you, she'll insult you even further...{w}\nYou may be delighted, but it's a headache for me."

    show ilias st02 with dissolve #700

    i "Now go, Long-Thing-Lover Luka.{w}\nPull off all her hair, and turn her into a wig."

    return


label sirohebisama_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "My, you sure do love Lamias.{w} Any shape, size or color does it for you, I see.{w}\nDo you prefer albinos the most, now?"

    show ilias st01 with dissolve #700

    i "Since she's a monster, serene state should be good...{w} But since her binding attack is powerful, Gnome is preferred."
    i "In addition, she can summon spirits of earth and wind.{w} When she does, counter it with the opposite spirit."
    i "Since she's a disgusting snake, ice is effective.{w} Exploit that Monster Lord's ice magic to freeze her."
    i "In addition, if you lose to a specific attack, she will insult you further...{w}\nNot that you care, I'm sure."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nBy the way, don't forget to kill the sister too."

    return


label walraune_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Why is she called Walraune?{w} Because she's a bad (warui) Alraune.{w}\nWhat horrible naming sense..."

    show ilias st01 with dissolve #700

    i "She's a powerful enemy, who has numerous status effect attacks.{w}\nYou can avoid most of them with Sylph."
    i "Since her HP is low, protecting yourself from status effects and launching a full offensive is the way to go."
    i "Also, since she is a plant, she is weak to fire.{w}\nI'm sure it would be fun to burn her to ashes."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nNo matter what kind of Alraune it is, destroy them with your sword."

    return


label dryad_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Are you seeing if you can grow a plant simply with your semen?{w}\nI don't think they accept that as a record in Guinness..."

    show ilias st01 with dissolve #700

    i "The Dryad's attack patterns change from the first half to the second half.{w}\nThe method to counter them changes, too, so be careful."
    i "Since she's a plant, be sure to call Sylph quickly to avoid status effects."
    i "In the second half, she will use a powerful restraint technique.{w}\nMake sure to call Gnome in order to avoid it."
    i "Since she's a plant, she burns well...{w} But you may be better off saving your slave for recovery purposes."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy all the monsters haunting Plansect forest."

    return


label queenalraune_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Did you enjoy having sex with a pistil?{w}\nYou seem to have misplaced your human dignity in the previous chapter..."

    show ilias st01 with dissolve #700

    i "Since she's a plant, Sylph is the best spirit to use.{w}\nWith Gnome, you can easily escape from her bind...{w} But her status effects are quite potent, too."
    i "Since she has a lot of HP, you're in for a lengthy fight.{w}\nSaving the Monster Lord's skill for emergency healing may be the best route to take."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCrush that plant, pluck her flowers, and press them between a book to preserve her death for all time."

    return


label slimelord_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "I heard there's a new food on the market called Slimebait.{w}\n...Of course, I am referring to you."

    show ilias st01 with dissolve #700

    i "With both high attack power, and a strong bind, Gnome is your best choice here."
    i "If she summons a spirit of water, her attack power will further increase.{w}\nBut you don't have Salamander, the opposite attribute, yet..."
    i "Since you have no other choice, you may need to use serene state or Sylph to do your best to evade the damage."
    i "In addition, if you lose to her draining move, she will continue to insult you.{w}\nNot that your end result will be any different..."

    show ilias st02 with dissolve #700

    i "Now go, oh Slimebait.{w}\nYou aren't allowed to lose to a Slime at this point."

    return


label eggel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Sheesh... Losing to that egg thing?{w}\nDid you want to experience being an egg yolk that badly?"

    show ilias st01 with dissolve #700

    i "Since she's an angel, fallen angel dance is effective...{w}\nBut due to her powerful bind, Gnome may be the better choice."
    i "She can use an ecstasy inducing effect during her bind, so make sure to use Gnome to escape as quick as possible."
    i "In addition, don't lose to a specific move...{w}\nOr you'll be stuck in a hell of bubbles."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTurn her into a fried egg."

    return


label tutigumo_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Look at that pitiful insect, squirming on a spider's web...{w}\nOh my, that's you."

    show ilias st01 with dissolve #700

    i "Her binding techniques are very powerful.{w}\nWithout Gnome, your defeat is assured."
    i "So you should always keep Gnome up, to prevent an instant loss.{w}\nYou can avoid her string attacks with Sylph, but you run that risk."
    i "But if she herself summons a spirit of earth, switch to Sylph right away."
    i "Furthermore, the Monster Lord's attacks don't do much.{w}\nIt would be best to use her for recovery."
    i "Lastly, there are additional insults awaiting a loss to certain moves.{w}\nIt might matter to you, but I don't care."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy these evil Ants and Spiders bringing horror to the citizens of Grangold."

    return


label alakneload_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Have you heard the tale of The Spider's Thread?{w}\nIf you have, I bet you were too focused on the spider's thread to understand the rest of the story..."

    show ilias st01 with dissolve #700

    i "The Arachne has an instant kill move she can use when you're bound.{w}\nYou can't break loose right away, even with Gnome, so it's particularly dangerous."
    i "You can avoid it fully with a serene state, so be sure to keep it up.{w}\nIf she summons a spirit of wind, then you must counter it with Gnome."
    i "Like the last spider, the Monster Lord's skills don't do much damage.{w} Recovery is a better use for her."
    i "If you're defeated while caught in her web...{w} Then you know what will probably happen."
    i "I am already disgusted, since I can tell you're going to go see what happens."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nAll spiders should be eradicated from the earth.{w} Including every subspecies."

    return


label kumonomiko_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Not just being squeezed dry, but eaten too?{w}\nI'm happy to see you grow in the complexities of ways to die."

    show ilias st01 with dissolve #700

    i "As expected of the new Queen of Spiders, her restraint techniques are very strong.{w}\nSince you can't break free without Gnome, make sure to always have her on hand."
    i "You can avoid some status effects with Sylph, but the instant loss of her bind is more dangerous."
    i "In addition, she can summon a spirit of fire, which will overwhelm you.{w}\nSince you don't have Undine yet, make sure to use your serene state."
    i "Just like before, the Monster Lord's skills should be used for recovery."
    i "Lastly, if you lose to a certain move, additional insults await.{w}\nBut please, try to fight seriously."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that new Queen, and exterminate those ugly spiders."

    return


label narcubus_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st01 with dissolve #700

    i "Thanks for coooming!{w}\nPlease lay down over there, I'll be with you in a moment!"

    show ilias st04 with dissolve #700

    i "...Is that what you wanted me to say?{w}\nDo you need me to role play as a nurse now to recover your fighting spirit?"

    show ilias st01 with dissolve #700

    i "Since she's a monster, a serene state is useful."
    i "She has two types of restraint techniques as well.{w}\nIf you don't break out of them fast, it's an instant loss."
    i "If she uses her tail in her restraint, then you can break free simply by struggling, even without Gnome."
    i "However, you can't escape by struggling when she tries to examine you with her vagina...{w}\nIf you don't attack her right away, you will lose."
    i "Don't panic and struggle when you should attack.{w}\nHer attack power isn't high, so as long as you are calm and careful, you will win."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nMake that shameless Succubus herself be in need of some nursing."

    return


label inps_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "You gave in to such a low level monster?"
    i "You're doing this on purpose, aren't you?{w}\nYou're doing it on purpose...?{w}\nYou're doing it on purpose."

    show ilias st01 with dissolve #700

    i "Swing your sword.{w}\nThat's it."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nMake sure you deal with those weak-willed men laying on the ground, too."

    return


label eva_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Since your opponent was a Succubus, you don't seem too unhappy about being sucked dry.{w}\nFeeding her on a silver platter like that...{w} You've disgusted me yet again today."

    show ilias st01 with dissolve #700

    i "Since your opponent is a monster, use a serene state.{w}\nShe also has an instant kill move that can be dodged with a serene state."
    i "If she turns around, do not attack...{w}\nIn addition, she uses spirits of earth and wind.{w} Be sure to summon their counter."
    i "Lastly, she will continue to insult you if she defeats you with many different moves...{w}\nBlowjob, facesitting, footjob, armpit sex..."
    i "Oh dear, I'm a Goddess... What's with the indecent language?"

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that disgusting Succubus, and restore calm to the village."

    return


label trooperloid_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Do you have any pride left after having your semen gathered so easily?{w}\nYou seem to be as good as livestock...{w} Good enough for milking."

    show ilias st01 with dissolve #700

    i "Her restraint attack is very strong, be sure to summon Gnome to escape from it."
    i "You can avoid the paralysis with Sylph...{w} But Gnome is a better choice."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nRemodel that monster into a cleaner, and use her to vacuum up the dirt in that town."

    return


label assassinloid_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "If your opponent is an assassin, at least be assassinated like a man.{w}\nWhy come here after acting like such a pathetic creature...?"

    show ilias st01 with dissolve #700

    i "Her restraint is very powerful, make sure to always have Gnome summoned.{w}\nBut when she summons a spirit of fire, counter with a serene state."
    i "There are also times when she goes into a stealthy state, and becomes immune to attacks.{w}\nYou can completely avoid these attacks with a serene state...{w} But you may also be able to survive by just defending through it."
    i "When she returns to normal after her stealth state, she will be completely defenseless.{w} Use those turns for some free hits."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nAssassinate the assassin and restore peace to the town."

    return


label yomotu_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "You're the same as ever, aren't you?{w} Look at you, becoming one with that monster...{w}\nAs soon as I take my eyes off you, you keep trying to get yourself assimilated by strange creatures...{w} Such a strange Hero."

    show ilias st01 with dissolve #700

    i "Her attack patterns are simple, so use a serene state to avoid many of them."
    i "You can escape from her bind fast with Gnome, but it isn't that powerful to begin with.{w}\nThe Monster Lord's attacks aren't too powerful.{w} Using her for recovery is your best bet."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nAren't you interested in seeing what happens if you pull on that monster's hair...?"

    return


label wormiel_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Oh, what an ugly thing...{w}\nHaving that thing be in a Goddess's service is a sin unto itself."

    show ilias st01 with dissolve #700

    i "She's an angel, so fallen angel dance is useful...{w} But since she has an instant kill bind attack, Gnome is better."
    i "The damage you take will be higher, but it's better than taking a chance on a random loss."
    i "There's additional insults that await if you lose to a certain move.{w}\nBut she's just a disgusting mass of worms anyway, I'm sure you wouldn't want to see it."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that disgusting angel before others are able to see her."

    return


label drainplant_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Remodeling a divine angel into that thing...{w}\nI wish to tear both the plant and the one inside it to pieces right now..."

    show ilias st01 with dissolve #700

    i "Always have Gnome available due to her restraint.{w}\nOther than that, there's nothing of note."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIf you enter that monster again, I will smite you both."

    return


label drainloid_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Was it fun being squeezed along with those other boys?{w}\nSo many hole brothers...{w} Oh, sorry... That was a little vulgar."

    show ilias st01 with dissolve #700

    i "Without Undine, you risk losing to an instant death move.{w}\nGnome will let you escape from her bind quicker...{w} But the instant death risk takes priority."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSave your hole broth...{w} I mean, the other trapped humans."

    return


label replicant_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st01 with dissolve #700

    i "What is a man, Luka?{w}\nA miserable lit...{w} I mean.{w} Humans are animals that have come to know the meaning of love."
    i "I'll never recognize a creation that doesn't acknowledge that.{w}{nw}"

    show ilias st02
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\n...See, I can say good things sometimes, too."

    show ilias st01 with dissolve #700

    i "Your opponent can use an instant death move.{w}\nYou must always have Gnome summoned to allow yourself to escape."
    i "Outside of that, there's nothing to note."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nShow her the strength of humans with your blade."

    return


label laplace_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "A machine without a soul...{w} And you allow her to dominate you?{w}\nAshes to ashes, dust to dust, and scrap iron into the trash.{w} Those are my teachings."

    show ilias st01 with dissolve #700

    i "Since your opponent is a machine, she's very hard to read.{w}\nThus, Undine and Sylph's effects are both small."
    i "You should call Gnome to reduce the damage."
    i "In addition, when she uses her charged up move, not even the spirits and guarding will let you avoid it."
    i "You must instead use your counter move.{w} Always ensure you have enough SP to protect yourself."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCut her up into scrap metal to be recycled."

    return


label fermesara_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "The one who dried you up, is herself dried up...{w}\nDry, dry, dry...{w} Is everything in this world drying up?"

    show ilias st01 with dissolve #700

    i "Ferme Sara can use an instant kill technique when you're bound.{w}\nThe probability is low, but having Gnome to break free is a good idea."
    i "Abnormal statuses can be avoided with Sylph though, so the choice is yours."
    i "Also, outside of fire, the Monster Lord's skills aren't that effective."
    i "In addition, if you lose to a certain skill then there are additional insults awaiting...{w}\nWhy not go for some extra bandage service?"

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIt's your mission to prevent the drying out of anyone else..."

    return


label angelghoul_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Another abomination made of a heavenly messenger...{w}\nAnd you, pleased to be raped by this creation...{w} Sins everywhere."

    show ilias st01 with dissolve #700

    i "She's undead, but also an angel... So Sylph is an effective choice.{w}\nThe evasion rate isn't quite as high as normal, but it's still better than nothing."
    i "If you don't struggle right away, you will lose.{w}\nBut you can break away without Gnome, so she isn't necessary."
    i "Other than that, there isn't anything special."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSince she has turned into this, she must be put down."

    return


label dragonzonbe_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "To be eaten and digested by a zombie...{w}\nYou're just a snack for a corpse, I see."

    show ilias st01 with dissolve #700

    i "When she sends out that body of hers, her attack pattern changes.{w}\nAt the start, a serene state with Undine is the best course of action."
    i "But when that other body part comes out, she starts to use restraint techniques.{w}\nAt that time, switch to Gnome."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSend that dead dragon back to where she belongs."

    return


label cirque_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "An abhorrent circus of corpses...{w} How disgusting.{w}\nAnd you looked so happy as they gang-raped you...{w} You're disgusting, too."

    show ilias st01 with dissolve #700

    i "For the first round, your opponent is the Harpy and the Elf.{w}\nTheir instant kill move is dangerous, so you need to protect yourself with Sylph."
    i "Round two is Scylla and the Fairy.{w}\nA serene state is effective, but Gnome is also helpful to break from binds.{w} The choice is yours."
    i "The third round is the Lamia and the Mermaid...{w}\nThe Lamia's binding move is troublesome, so Gnome is a great help."
    i "It may seem dizzying at first, but adapt your spirits to your opponents."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTear down that circus with your own hands."

    return


label alice15th_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "The previous Monster Lord!{w} How horrifying!{w} What a disgusting creature!{w}\nAnd the first thing you did was let her feed on you!"

    show ilias st01 with dissolve #700

    i "Both Gnome and Sylph are useful...{w}\nBut Undine's serene state will probably do the most."
    i "Also, she has an instant kill move that can be completely avoided with a serene state."
    i "You'll avoid most of her attacks...{w} But her attack power is very high, so it will still hurt.{w}\nMake sure you always keep enough in reserve to heal yourself."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSend that monster back to hell where she belongs."

    return


label shirom_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Having a subordinate like that...{w}\nPromestein, this isn't over yet...{w} Ah, excuse me.{w} Don't worry about that..."

    show ilias st01 with dissolve #700

    i "Sylph and Undine don't have much effect on evasion.{w}\nSince her restraint moves are also a threat, you should use Gnome here."
    i "Since you won't be able to avoid any damage, you should save some SP for healing at all times."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that horrible necromancer, and bring calm back."

    return


label alice8th1_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "So you were defeated by Black Alice, and came here...{w}\nFor some reason, I'm starting to get nostalgic about Heinrich's time..."

    show ilias st01 with dissolve #700

    i "Her attacks are very strong, but you can avoid many of them with a serene state.{w}\nStill, her painful attacks will hit every so often.{w} Keep some SP available for healing."
    i "In addition, if you're defeated while she rapes you, there will be an additional insult."
    i "I won't say you should watch it..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Heinr...{w} I mean, Luka.{w}\nLike the ancient legendary Hero, you should be able to win as well."

    return


label traptemis_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Who would lay such a horrible trap!?{w}{nw}"

    show ilias st02
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\n...Ah. I would."

    show ilias st04 with dissolve #700

    i "Well, you see, I would be greatly troubled to have humans wandering around carelessly near the Gate...{w}\n...But those that were captured are quite pathetic, wouldn't you agree?"

    show ilias st01 with dissolve #700

    i "If you don't summon Sylph right away, you will be tempted by the gas."
    i "Just defeat her after that.{w} As long as you have Sylph, you can't lose."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nPlease don't break all of my statues..."

    return


label gargoyle_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "She has been watching that gate for centuries.{w} She must be quite bored.{w}\nBut now she has you as a toy to pass the time with.{w} You're such a nice guy."

    show ilias st01 with dissolve #700

    i "The Gargoyle is a powerful enemy who changes up her attack patterns.{w}\nIn her normal state, Gnome is required to break free of her restraint."
    i "When she goes back into her statue form, summon Sylph to avoid her status effect inducing skills."
    i "When she is in her statue state, you can continue attacking her.{w}\nDon't be worried about some sort of counter."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEnd the endless shift of that Gate Guard."

    return


label doppele_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "You raped yourself...?{w}\nCouldn't this just be said to be some elaborate form of masturbation?"

    show ilias st01 with dissolve #700

    i "Doppel Luka can offset all of your sword skills.{w}\nYou can't fight her directly without addressing that."
    i "You should check each of your skills against her, and perhaps try to find one she doesn't know..."
    i "After she strips, you can fight normally...{w} But you should give priority to rebuilding your SP."
    i "After she strips, Undine is effective."
    i "In addition, depending on when you lose, it will change whether she's dressed or not during the rape scene after."

    show ilias st04 with dissolve #700

    i "Oh I wonder what you're going to try now!?"

    show ilias st02 with dissolve #700

    i "No go, oh elaborate masturbator Luka.{w}\nBecause you disappointed me, I can tell you're the real one."

    return


label hainu_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Since I would never help a Monster, I came to you instead.{w}\nFor once, I'm not criticizing you.{w} But you should still pay attention."

    show ilias st01 with dissolve #700

    i "It seems like she can nullify any attack...{w}\nPhysical, pleasure, magic...{w} Why not try everything?"

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIt's regrettable, but you must pray that the disgusting Succubus wins."

    return


label amphis_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "That moving puddle lost, I see.{w}\nI know you didn't lose directly, but you shall undergo the evaluation anyway."

    show ilias st01 with dissolve #700

    i "It seems like that vibrating toy is slowly losing her energy.{w}\nTo get her to be fully drained, you need to have her keep using her armor."
    i "Of course, it will damage you as well...{w}\nBut I don't really care."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThis time, pray that the puddle wins."

    return


label tukuyomi_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "That damn fox...{w}\nDid she send you here just to harass us both?"

    show ilias st01 with dissolve #700

    i "The difference in power is clear...{w} But that damn fox is holding a trump card.{w}\nPerhaps if she just lowers the enemy's HP a bit..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nKill that wicked fox later on, too."

    return


label arcen_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "I wonder if her brain itself is composed of useless muscle?{w}\nSheesh, that lizard is useless during the only time she's needed."

    show ilias st01 with dissolve #700

    i "Since she's nothing but muscle, she might as well just swing her sword.{w}\nIn that case, she could at least reduce the enemy's HP a little bit."
    i "If it goes on long enough, she might even try something new.{w}\nEven monkeys figure out how to do things after enough time, after all."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nPray that the muscle lizard manages to actually win."

    return


label rapun_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Mating with an artificial monster this time...{w}\nI wouldn't be surprised to see you mate with stray dogs you find now..."

    show ilias st01 with dissolve #700

    i "Starting with this fight, you can unlock the true power of the spirits.{w}\nWith them usable, this shouldn't be a hard fight."
    i "Your evasion and defense will both be sky high...{w}\nSince you can stack them on top of each other, you aren't bound by picking a good one for each situation any longer."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nOnce you're tired of mating with everything you see, how about trying to save the world?"

    return


label heavensgate_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "To think you would even be raped by the gate...{w} You managed to surprise me... Surprisingly.{w}{nw}"

    show ilias st02 #700
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\n...Though I made that gate, so I knew it was possible."

    show ilias st01 with dissolve #700

    i "Your enemy is an angel, so fallen angel dance is useful.{w}\nGnome will also help with defense, of course."
    i "Undine isn't as effective, so you can skip her."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nI want to use that gate again later, so don't break her too badly."

    return


label eden_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Even my trusted confidant turned into something horrible...{w}\nHer appearance itself is a sin."

    show ilias st01 with dissolve #700

    i "Wielding great strength and magic, each of Eden's attacks can bring about instant death."
    i "You will need both evasion and defense to have any hope of surviving.{w}\nSo be ready to challenge her using all of the spirits."
    i "Also, if you lose to a certain attack, a different horrible fate awaits."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCorrect that foolish angel with your blade of divine justice."

    return


label stein2_hansei:
    $ persistent.count_hanseikai2 += 1

    if hansei_first == 0:
        call first_hansei 

    show ilias st04 with dissolve #700

    i "Promestein...{w} Just how far did your horrible research go?{w}\nYou've become a true demon, at last.{w} Divine punishment awaits."

    show ilias st01 with dissolve #700

    i "Close in form to a monster now, a serene state is effective.{w}\nFallen angel dance isn't too effective, so Sylph need not be called."
    i "Her restraints are also powerful, so Gnome is required.{w}\nThe damage reduction will help a lot, too."
    i "As before, make sure to summon the opposite attribute if she summons a spirit.{w}\nHer HP is very high, so prepare for a long fight."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nMake her taste the thousands of years of my anger..."

    return


label alice8th2_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st01m with dissolve #700

    i "...Don't mind the slightly ugly appearance."
    i "Since your opponent was once a Monster Lord, a serene state is effective.{w}\nWithout Gnome, you also can't break free of her restraint."
    i "Therefore, summoning Gnome and Undine is mandatory."
    i "When she uses her charge move to prey on you, you cannot evade or guard.{w}\nYou must use your Counter, or you will be eaten."
    i "If she defeats you with that, you will simply be preyed on.{w}\nThere won't be any other end for you..."

    show ilias st01m with dissolve #700

    i "Now go, oh brave Luka.{w}\nSlay the evil Monster Lord with your blade of justice."

    return


label alice8th3_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st01m with dissolve #700

    i "Welcome back, Luka.{w}\nI will give you my secret plan for victory..."
    i "Like before, Undine and Gnome are both required.{w}\nBut in this form, she's also using spirits.{w}\nBe sure to counter them."

    show ilias st01m with dissolve #700

    i "Now go, oh brave Luka.{w}\nSettle this long battle of Light and Darkness..."

    return


label alice8th4_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st01m with dissolve #700

    i "This will probably be the last evaluation meeting.{w}\nYour quest will be finished at last..."
    i "Her behavior patterns are complex, and tough to deal with."
    i "Even the names of her skills are filled with dread..."
    i "Like before, you need to counter her spirits with your own.{w}\nBut if you summon more than one, she will cancel them out."
    i "Undine will help your evasion, and Gnome will let you break free faster...{w}\nWhen she's fighting in a normal state, one should always be summoned."
    i "In addition, she has two types of binds.{w}\nPay attention to what part of your body is bound to see if you should struggle or attack..."
    i "In addition, if she looks like she's going to charge something up, you must use your counter."
    i "Also, she is able to use all four fake spirits herself.{w}\nAnd, in the same turn, she can cancel out your own!"
    i "Do not forget to counter those spirits.{w}\nA mistake here will mean your instant loss."
    i "She is indeed the culmination of every opponent you've ever faced.{w}\nObtain victory with your own hand!"
    i "In addition, every way she can eliminate you leads to further insults.{w}\nBut surely you wouldn't go through all of them...?"

    show ilias st01m with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy the ancient evil, and become a True Hero."

    return


label irias1_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st12 with dissolve #700

    i "Heh...{w} Hahahaha!{w}\nYou're pathetic, Luka.{w} You know that?"
    i "Did you really think I was some piece of Ilias that was still good?{w}\nI am the one and only Goddess!{w} I transcend Good and Evil!"
    i "...That's right, I've been deceiving  you this entire time.{w}\nYou have no idea how difficult it was...{w} To stifle my laughter as you danced in my palm, that is."
    i "Everything has been to lead you to defeat Black Alice after she absorbed me.{w}\nTo that end, I provided you with this assistance."
    i "Haha... I bet you never thought that was the reason for these, did you?{w}\nYou've been doing exactly what I've wanted, this whole time."

    show ilias st13 with dissolve #700

    i "Ah, yes, there's no more evaluation meetings.{w}\nI'll defeat you no matter how many times you try!"

    return


label irias2_hansei:
    $ persistent.count_hanseikai2 += 1
    show rucyfina st01:
        xpos 50
    with dissolve #700

    q "Luka... I forced a difficult fate upon you.{w}\nMy imprudent actions have tormented you for so long..."
    q "No... I did not come to beg your forgiveness.{w}\nI wish to lend my power to help defeat the true evil."
    q "First... Focus on healing yourself during the first phase.{w}\nGnome is required to avoid taking fatal damage."
    q "You must also summon both Sylph and Undine to help avoid damage, if even a little.{w}\nAside from recovery and spirits, don't waste your SP on anything else."
    q "And for the final phase...{w}\nIt will be a fight just between you, Ilias, and Alice."
    q "Make sure to always have Undine summoned to avoid a fatal instant kill attack.{w}\nAlso, Sylph can help you evade her other attacks, so summon her as well."
    q "Gnome can help you break free of her binds quicker, but isn't required."
    q "And in the end...{w} You must strike her down with everything you've got.{w}\nJoin together with Alice, and you cannot fail."

    show rucyfina st02:
        xpos 50
    with dissolve #700

    q "Luka...{w} Even though my body may be gone, I am still watching over you."
    q "Please, triumph...{w}\nAnd after, be happy in the peace you've obtained..."

    return