label part3demo:
    $ exp = 31999000
    call lvup_check
    $ storyon = 1
    $ demo_on = 1
    $ ng = 1
    $ demoend = 0
    $ item01 = 10
    $ item12 = 1
    $ skill01 = 27
    $ skill02 = 3
    $ skill03 = 3
    $ skill04 = 3
    $ skill05 = 3
    $ ivent01 = 1
    $ ivent02 = 1
    $ ivent03 = 1
    $ ivent04 = 1
    $ ivent05 = 1
    $ ivent06 = 1
    $ ivent07 = 1
    $ ivent08 = 1
    $ ivent09 = 1
    $ ivent10 = 1
    $ ivent11 = 1
    $ ivent12 = 1
    $ ivent13 = 1
    $ ivent14 = 1
    $ ivent15 = 1
    $ ivent16 = 1
    $ check01 = 1
    $ check02 = 1
    $ ribbon = 0
    $ red_orb = 1
    $ orb_count = 1
    $ sylph_rep = 0
    $ gnome_rep = 4
    $ undine_rep = 5
    $ salamander_rep = 0
    $ party_member[0] = "Alice"
    $ spirits = ["Undine", "Gnome", "Sylph", "Salamander"]

    show expression Text(_("Вы хотите Небесного Рыцаря в пати?"), size=24, style="ui_text") as menu_title:
        xalign 0.5
        yalign 0.1

    menu:
        "Тамамо":
            $ party_member[1] = 3
            $ knight_name = "Тамамо"
            $ knight_name1 = knight_name
            $ knight_name2 = knight_name

        "Гранберия":
            $ party_member[1] = 2
            $ knight_name = "Гранберия"
            $ knight_name1 = "Гранберией"
            $ knight_name2 = "Гранберию"

        "Альма Эльма":
            $ party_member[1] = 4
            $ knight_name = "Альма Эльма"
            $ knight_name1 = "Альма Эльмой"
            $ knight_name2 = "Альма Эльму"

    window auto
    $ ruka_tati = 1
    $ party_member[2] = 7
    $ party_member[3] = 6
    $ pm = party_member[2:4]
    play music "audio/bgm/hutuu1.ogg"
    scene attention with dissolve
    play sound "NGDATA/se/chime.ogg"

    "Это небольшое демо 3 части NG+{w}\nНа данный момент доступны только Деревня Суккубов, Голдпорт, Пирамида, Источник Ундины, Особняк Курому, Порт Наталия, Гранд Ной и Деревня Охоты на Ведьм."
    "3 часть полностью выйдет уже совсем скоро™."
    "На этом все.{w}\nНаслаждайтесь!"

    jump all_spirits2

label all_spirits2:
    $ part3 = 1
    scene bg 010 with blinds2
    play music "NGDATA/bgm/field9.ogg"
    $ countdown = 9999
    if sylph_order == 4:
        $ region = "Natalia"
    if gnome_order == 4:
        $ region = "Safina"
    if undine_order == 4:
        $ region = "Noah"
    if salamander_order == 4:
        $ region = "Gold"

    $ spirit_count = 4

    show alice st01b with dissolve
    a "Итак, ты собираешься попасть на Хеллгондо только после того, как соберёшь все шесть сфер?"
    l "Да...{w}\nМне они нужны для проведения ритуала пробуждения Гаруды.{w}\nОна позволит нам намного проще путешествовать по миру."
    show alice st04b with dissolve
    a "Хм, это в десяти днях пути."
    a "Хотя я могла бы сэкономить время и просто перелететь вместе с тобой.{w}\nНо это довольно непрактично."
    l "...{w}\nТогда почему же ты просто не перенесла меня к каждому из духов?"
    show alice st08b with dissolve
    a "Ну... это...{w}\nТы же не сможешь распространять сосуществование и влажные мечты с воздуха, да?"
    l "Я мог бы вернуть всех духов всего за один день!{w}\nЗнаешь ли, у нас не так много осталось времени!"
    show alice st07b with dissolve
    a "Ладно, я извиняюсь!{w} Я просто не подумала об этом!"
    a "В смысле...{w} Я не так уж и часто летаю.{w} Поэтому я просто забыла о том, что так могу."
    l ".............."
    "Невероятно.{w} У меня просто нет слов."
    "И я всё ещё не могу понять, как ламия может летать."
    scene bg black with blinds2
    l "Шесть сфер..."
    show vgirl st01 with dissolve
    l "Первую сферу, красную, я получил от бандиток в Илиасбурге.{w}\nОни сказали, что украли её у какого-то богатого человека."
    if banditcave == 0:
        a "О, бандитки?{w}\nТе самые, которых ты проигнорировал, так как тебе нужно было поскорее найти Гаруду?"
        l "Ух, давай не будем об этом..."
        "Я вернусь к ним когда-нибудь..."
    else:
        a "Правильно. Мы уже получили эту сферу."
        if acheck(pm,7) == True and (party1_banditcave == 7 or party2_banditcave):
            ipea "Эти маленькие девочки были такими милыми!"

    show sphinx st01 as c700 with dissolve
    l "Дальше, жёлтую сферу я получил в пирамиде региона Сафару.{w}\nИменно ту, где сидит Сфинкс."
    if party_member[1] == 2:
        g "Подожди, ты посещал пирамиду?"
        "Гранберия внезапно поднимает голос."
        l "Э, а что не так?"
        g "... Ясно.{w}\nПродолжай."
        "... Эм.{w} Это было немного странно."
        l "Так или иначе..."

    show grandnoa st02 as c700 with dissolve
    l "После этого, я отправился в Колизей Гранд Ноа. Призом за победу в турнире была зелёная сфера.{w}\nПоэтому мне пришлось победить и забрать её."
    if party_member[1] == 4:
        alma "Колизей!"
        l "Да, я знаю, Альма..."
        "Всякий раз, когда речь заходит о колизее, Альма Эльма превращается в гиперактивного ребёнка."
        l "Ладно, дальше..."

    l "Четвёртой сферой была пурпурная, которую принадлежала капитану Селене.{w}\nА именно на её утерянном пиратском корабле, где находились Вельзебубы."
    if party_member[1] == 3:
        t "Э!?{w}\nВельзебубы!?"
        l "Да, именно они."
        t "Получается они были распечатаны.{w}\nПонятно..."
        t "С ними нужно разобраться.{w}\nОчень опасно позволять им свободно разгуливать..."
        l "Ну, так и так мы пойдём туда."
        l "Затем..."
    show kraken st01 at xy(-200) as c700
    show poseidones st01 at xy(200) as c701
    with dissolve
    l "Последние две сферы, голубая и серебрянная, находятся под охраной двух правительниц морей: Кракена и Посейдонии."
    scene bg 010
    show alice st01b
    with blinds2
    l "Это регионы, которые мне точно следует посетить."
    l "... Но также мне следует сходить и в другие места! По всему континенту!"
    show alice st04b with dissolve
    a "Как бессмысленно...{w}\nЗаниматься глупыми, бессмысленными вещами, когда над нами нависает угроза войны.{w} Идиот."
    l "Это не бессмысленно!{w}\nЛюди и монстры всё ещё разделены в этих местах, так что у них не будет и шанса выстоять против химер и ангелов!"
    show alice st08b with dissolve
    a "..........{w}\nЛадно, твои слова звучит разумно."
    show alice st01b with dissolve
    a "Но уж точно бессмысленно торчать здесь, полагаю."
    l "Да, пора идти!"
    translator "На этом перевод стопорится. Ждите обновлений на {a=https://gitlab.com/Darkness9724/mgq-ng-ru}Гитлабе{/a} и нашем {a=https://discordapp.com/invite/xEEAa2w}Дискорд-сервере{/a}! Там весело :)"
    $ renpy.full_restart()