label city06_main:
    call city_null
    $ city_bg = "bg 037"
    $ city_button01 = "Юноша" #601
    $ city_button02 = "Мужчина" #602
    $ city_button03 = "Женщина" #603
    $ city_button04 = "Солдат" #604
    $ city_button05 = "Русалка A" #605
    $ city_button06 = "Русалка B" #606
    $ city_button07 = "Девочка-русалка" #607
    $ city_button08 = "Русалка-торговец" #608
    $ city_button10 = "Оружейный магазин" #610
    $ city_button11 = "Лавка" #611
    $ city_button12 = "Церковь" #612
    $ city_button13 = "Русалочий бар" #613
    $ city_button19 = "Покинуть город" #619

    while True:
        call cmd_city

        if result == 1:
            call city06_01
        elif result == 2:
            call city06_02
        elif result == 3:
            call city06_03
        elif result == 4:
            call city06_04
        elif result == 5:
            call city06_05
        elif result == 6:
            call city06_06
        elif result == 7:
            call city06_07
        elif result == 8:
            call city06_08
        elif result == 10:
            call city06_10
        elif result == 11:
            call city06_11
        elif result == 12:
            call city06_12
        elif result == 13:
            call city06_13
        elif result == 19:
            return


label city06_01:
    show mob seinen1 with dissolve #700

    youth "Почему Илиас Кройц так сильно ненавидят монстров...{w}\nМожет, это потому, что женщины так ревнуют к девушкам-монстрам?"
    youth "Ну, так говорит моя милая девушка-русалка..."

    return


label city06_02:
    show mob ozisan1 with dissolve #700

    man "Ненавижу беспорядочный терроризм Илиас Кройц."
    man "Если они взорвут Бар Русалок...{w}\nВозможно, тогда я сам стану террористом для террористов."

    return


label city06_03:
    show mob musume2 with dissolve #700

    woman "Я не очень люблю русалок...{w}\nНо напав на их школу, они зашли слишком далеко."

    return


label city06_04:
    show mob sensi2 with dissolve #700

    soldier "Я слышал, Гранберия напала на Илиасбург, но одинокий герой смог победить её."
    soldier "Думая, что человек смог противостоять Гранберии...{w}\nМир действительно удивителен, да?"

    return


label city06_05:
    show mob mermaid2 with dissolve #700

    mermaid_a "Мы, русалки, намного сильнее людей...{w}\nДаже если они взорвут наши здания, людей погибнет больше, чем монстров."
    mermaid_a "... Мы обеспокоены тем, что наше присутствие приносит вред местным жителям."

    return


label city06_06:
    show mob mermaid1 with dissolve #700

    mermaid_b "Эти негодяи из Илиас Кройц...{w}\nЕсли они что-то имеют против нас, почему бы просто об этом не сказать?"
    mermaid_b "Нападать на школу, полную детей...{w}\nНепростительно!"

    return


label city06_07:
    show mob mermaid3 with dissolve #700

    mermaid_girl "Спасибо больше, что спас нашу школу.{w}\nМои друзья могли пострадать!"
    show mob mermaid3 at xy(X=200)
    show alma_elma st12 at xy(X=-200)
    with dissolve #700
    alma "А разве ты сама не должна была быть в школе, юная леди?"
    show alma_elma st11 with dissolve
    mermaid_girl "Эмм, я прогуляла..."
    "Кстати, зачем русалкам своя собственная школа?{w}\nРазве они не могут заниматься учёбой совместо с людьми?"
    hide alma_elma with dissolve

    return


label city06_08:
    show meia st21 at xy(X=50, Y=50) with dissolve #700

    mermaid_merchant "Почему же моя жареная морская звезда не продаётся...{w}\n...Может, это плохой товар?"
    mermaid_merchant "Эй, а ты что думаешь?"
    l "Она... не очень аппетитно выглядит."
    mermaid_merchant "Понятно...{w}\nТогда поищу что-нибудь новенькое..."

    return


label city06_10:
    scene bg 150 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "Добро пожаловать!{w}\nУ нас собрано лучшее оружие со всего света."
    l "Хмм... не думаю, что мне нужен другой меч."

    show alice st11b at xy(X=-120) #700
    show mob ozisan3 at xy(X=160)
    with dissolve #701

    shopkeeper "Возможно, этой прекрасной девушке будет интересен нож для самозащиты?"
    a "Мои кулаки — сильнейшее оружие в мире."
    shopkeeper "П-понятно..."

    scene bg 037 with blinds
    return


label city06_11:
    scene bg 154 with blinds
    show mob seinen1 with dissolve #700

    shopkeeper "Добро пожаловать!"
    l "...Эта женская одежда очень откровенная..."
    shopkeeper "Это новый тренд.{w}\nОткровенную одежду сейчас хотят все"
    shopkeeper "Взять это, к примеру..."
    l "Да тут совсем нет ткани!"
    shopkeeper "...Разве не потрясающе?{w}\nА как насчёт этого!?"
    l "Аааа!{w}\nОденься ты так в моей деревне, солдаты из храма арестовали бы тебя!"
    shopkeeper "Хо-хо-хо... А как насчёт ЭТОГО!"

    show alma_elma st11 at xy(X=-160) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701
    alma "Оооо!{w}\nМне оно нравится!"
    l "... Оно даже не прикроет все твои места!{w}\nПо сути, это тоже самое, что ходить голой..."
    show alma_elma st13 with dissolve
    alma ".............."
    l "Ух, з-забудь!"
    hide mob
    show alice st11b at xy(X=-120)
    show alma_elma st14 at xy(X=150)
    with dissolve

    a "........."
    l "Хья! Алиса!?"
    a "..................... Идиоты."

    scene bg 037 with blinds
    return


label city06_12:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "Я немного понимаю позицию Илиас Кройц.{w}\nНельзя иметь сексуальных отношений с монстрами…"
    priest "Однако в городе много людей, нарушающих этот закон.{w}\nЭто очень серьёзная проблема…"

    scene bg 037 with blinds
    return


label city06_13:
    scene bg 155 with blinds
    show mob mermaid2 with dissolve #700

    mermaid "Добро пожаловать в знаменитый Русалочий Бар порта Наталия!"
    mermaid "Оооуу... какой милый мальчик.{w}\nХочешь поиграть со старшей сестрёнкой?"
    "Русалка кладёт руку Луки на свою грудь!{w}\nРусалка соблазнительно дует Луке в ухо!"
    l "... Нет"
    mermaid "Ээээ!?"

    play sound "audio/se/lvdown.ogg"

    "Лука отвергает распутную русалку!"

    scene bg 037 with blinds
    return


label city07_main:
    call city_null
    $ city_bg = "bg 044"
    $ city_button01 = "Guard A" #601
    $ city_button02 = "Guard B" #602
    $ city_button03 = "Priest A" #603
    $ city_button04 = "Priest B" #604
    $ city_button05 = "Scholar A" #605
    $ city_button06 = "Scholar B" #606
    $ city_button07 = "Soldier" #607
    $ city_button10 = "Chapel" #610
    $ city_button11 = "Basement Library" #611
    $ city_button12 = "Audience Hall" #612
    $ city_button13 = "Guard's Office" #613
    $ city_button19 = "Get Some Rest" #619

    while True:
        call cmd_city

        if result == 1:
            call city07_01
        elif result == 2:
            call city07_02
        elif result == 3:
            call city07_03
        elif result == 4:
            call city07_04
        elif result == 5:
            call city07_05
        elif result == 6:
            call city07_06
        elif result == 7:
            call city07_07
        elif result == 10:
            call city07_10
        elif result == 11:
            call city07_11
        elif result == 12:
            call city07_12
        elif result == 13:
            call city07_13
        elif result == 19:
            return


label city07_01:
    show mob heisi1 with dissolve #700

    guard_a "The guards inside San Ilia are carefully selected.{w}\nOnly the truly pious and brave are allowed to be guards."
    guard_a "It's the dream of knights all over the world."

    return


label city07_02:
    show mob heisi1 with dissolve #700

    guard_b "...I'm patrolling the castle."
    guard_b "I'll ensure no monster is able to step foot on this sacred ground.{w}\nDon't worry you two, for I am here."

    show alice st11b at xy(X=-120) #700
    show mob heisi1 at xy(X=160)
    with dissolve #701

    a "........."
    "The Monster Lord is already in here..."

    return


label city07_03:
    show mob sinkan with dissolve #700

    priest_a "Thousands of years ago, there was a holy war.{w}\nThe great Ilias leading angels on one side, and the first Monster Lord leading her forces on the other."
    priest_a "The conflict shook the land itself, and only stopped when Ilias defeated the Monster Lord."
    priest_a "You can still see traces of the war around the land."

    return


label city07_04:
    show mob sinkan with dissolve #700

    priest_b "The people in this world are truly outrageous.{w}\nThe heathens that turn away from Ilias... The heathens that conspire with monsters."
    priest_b "Truly, truly... Truly Outrageous."

    return


label city07_05:
    show mob gakusya with dissolve #700

    scholar_a "The large basement library has the largest collection of books in the world."
    scholar_a "So it's not an exaggeration to say it contains the world's knowledge."
    scholar_a "Scholars that seek that knowledge travel from all over the world to study here."
    scholar_a "As for me, I'm researching sacred energy."
    l "The largest library in the world... Wow...{w}\nAlice, do you want to take a look?"

    show alice st14b at xy(X=-120) #700
    show mob gakusya at xy(X=160)
    with dissolve #701

    a "I can't eat books..."
    scholar_a "Unfortunately, you need permission before you can enter the library.{w}\nIt contains so many valuable books, that you need to be properly vetted."
    scholar_a "It took me a week to get permission."
    l "It takes that long...?"
    "I don't really need anything from there, so I won't bother to ask for permission..."

    return


label city07_06:
    show mob gakusya with dissolve #700

    scholar_b "I'm studying the old scriptures."
    scholar_b "I specialize in deciphering ancient texts.{w}\nBut right now I'm stuck on a mysterious phrase in chapter seven paragraph three of this book..."
    scholar_b "{i}Whomsoever troubles thy goddess with repeated Evaluation Meetings shall receive judgement.{/i}"
    scholar_b "What does this mean?"
    scholar_b "If you do this {i}Evaluation Meeting{/i} too many times, you will be judged?"
    scholar_b "This is the first time this {i}Evaluation Meeting{/i} has been mentioned in any text..."
    scholar_b "How mysterious..."

    return


label city07_07:
    show mob sensi1 with dissolve #700

    soldier "In San Ilia, there's a sword called the {i}Goddess Sword{/i}.{w}\nIt seems as though the King himself manages it in Ilias's name."
    soldier "Once he finds a true hero worthy of it, he shall bestow it upon them."
    soldier "...I wonder if he'll choose me."

    return


label city07_10:
    scene bg 157 with blinds

    l "Uwa... This is a huge chapel.{w}\nIt's sooo wiiide!"

    show alice st11b with dissolve #700

    a "Hrm. Mine is a lot bigger than this."
    l "Now, let's pray!"

    show alice st13b with dissolve #700

    a "Inviting me to do something so stupid!"
    l "Please let my travels go well...{w}\nSo that I can fulfill my dream of humans and monsters coexisting."

    show alice st14b with dissolve #700

    a "Please destroy this castle...{w}\nAlso let Ilias perish."

    scene bg 044 with blinds
    return


label city07_11:
    show mob heisi1 with dissolve #700

    guard "...Hold up, boy.{w}\nYou need permission to enter the library.{w}\nDid you fill out the paperwork properly?"
    l "No... Not yet..."
    guard "I'm sorry then, I cannot let you pass.{w}\nPlease come back after getting permission."
    l "Alright..."
    "I don't really need anything from the library.{w}\nI won't bother to fill out any permission forms."

    return


label city07_12:
    show mob heisi1 with dissolve #700

    guard "...Hold up, boy.{w}\nHave you received permission to enter yet?"
    l "No... Not yet..."
    guard "I cannot let you through, then.{w}\nPlease go back and wait for your turn."
    l "Alright..."

    hide mob
    show alice st12b
    with dissolve #700

    a "Trying to break right in to meet with the King?{w}\nYou do some unexpected things sometimes."
    l "...It was worth a shot."

    return


label city07_13:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2 #701
    with dissolve

    guard_d "Ahhhh. That's some good coffee."
    guard_e "It's important to stay awake for this work.{w}\nWhen on duty, work hard.{w} When on break, rest hard."

    scene bg 044 with blinds
    return


label city08_main:
    call city_null
    $ city_bg = "bg 048"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Three Women" #602
    $ city_button03 = "Old Man" #603
    $ city_button04 = "Children" #604
    $ city_button05 = "Soldier A" #605
    $ city_button06 = "Soldier B" #606
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "San Ilia Castle" #612
    $ city_button19 = "Leave Town" #619

    while True:
        if hanyo[0] + hanyo[1] + hanyo[2] + hanyo[3] == 4:
            $ city_act19 = 1
        elif hanyo[0] + hanyo[1] + hanyo[2] + hanyo[3] < 4:
            $ city_act19 = 0

        call cmd_city

        if result == 1:
            call city08_01
        elif result == 2:
            call city08_02
        elif result == 3:
            call city08_03
        elif result == 4:
            call city08_04
        elif result == 5:
            call city08_05
        elif result == 6:
            call city08_06
        elif result == 10:
            call city08_10
        elif result == 11:
            call city08_11
        elif result == 12:
            call city08_12
        elif result == 19:
            return


label city08_01:
    show mob seinen1 with dissolve #700

    youth "My friends say they saw something strange in the forest to the northwest."
    youth "Some small person with butterfly wings dancing in a circle.{w}\nI wonder if he hallucinated... Or is something really there?{w}\nEither way, I don't want to go near that forest anymore..."

    return


label city08_02:
    show mob zyosei at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman_a "Did you hear?{w}\nAnother ghost appeared at that house..."
    woman_b "Yes... And I heard it was the ghost of a child!{w}\nThe tool shop owner saw it too!"
    woman_c "Oh Ilias... Please save that pitiful ghost..."
    l "Excuse me... Could you tell me more about that ghost?"
    woman_a "Oh, an adventurer?{w}\nIt doesn't involve treasure or anything, but sure."
    woman_a "If you go north from here, there's a large mansion.{w}\nTen years ago, a rich millionaire lived there."
    woman_a "But their only daughter died of a disease...{w}\nTorn by grief, they left their house."
    woman_a "Now it's uninhabited...{w}\nExcept for the ghost of the girl!"
    l "I see..."
    "It sounds pretty strange for the work of a monster..."

    $ hanyo[0] = 1
    return


label city08_03:
    show mob rouzin1 with dissolve #700

    old_man "The land to the north used to be an ancient execution ground...{w}\nIt's said that the hatred of those killed there lingers in the house..."
    old_man "...How terrifying."
    l "It was an execution ground?"

    $ hanyo[1] = 1
    return


label city08_04:
    show mob syounen at xy(X=-160) #700
    show mob syouzyo at xy(X=160) as mob2
    with dissolve #701

    boy "I heard there was a graveyard to the north..."
    girl "That's why ghosts appeared."
    l "It was a graveyard?"

    $ hanyo[2] = 1
    return


label city08_05:
    show mob sensi1 with dissolve #700

    soldier_a "I heard that the Monster Lord attacked San Ilia castle...{w}\nFirst she destroyed the {i}Goddess Sword{/i}, then set the library on fire..."
    soldier_a "It sounds like she caused some huge damage...{w}\nBut why is everything so calm?"

    return


label city08_06:
    show mob sensi2 with dissolve #700

    soldier_b "They say that some strange sorcerer settled in the mansion to the north, doing some kind of forbidden research..."
    soldier_b "I was offered a handsome reward to get rid of whatever was there...{w}\nBut I had to turn it down.{w} I'm scared of being in a house in the darkness."
    l "A strange sorceror...?{w}\nWhat's going on there...?"

    $ hanyo[3] = 1
    return


label city08_10:
    scene bg 150 with blinds
    show mob ozisan1 with dissolve #700

    owner "Welcome!{w}\nI'm sorry, but we only have a limited selection of merchandise due to various circumstances this month..."
    l "What happened?"
    owner "The blacksmith in charge of procuring materials went to the haunted mansion to the north on a dare."
    owner "He came back several days later broken down, and refusing to talk about what happened."
    owner "He's so shaken up he can't even work.{w}\nI wonder what happened there...?"

    scene bg 048 with blinds
    return


label city08_11:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    owner "Welcome! Can I interest you in any of my wares?{w}\nHow about this Sacred Anti-Ghost Water?"
    l "Sacred water...? It isn't an amulet or something?"
    owner "You'll be thankful to have it..."
    owner "I was passing by the haunted mansion to the north earlier, when I saw a girl looking at me through the window."
    owner "Ever since then, I've carried this sacred water with me so that she won't attack me."
    owner "If you ever meet a ghost, you'll be thankful you have this..."
    l "No... I don't need it."

    show alice st11b at xy(X=-120) #700
    show mob ozisan3 at xy(X=160)
    with dissolve #701

    a "Hrm. I'll take it."
    owner "Thank you!"

    play sound "audio/se/kaimono.ogg"
    pause 2.0
    $ item10 = 1

    l "Alice...?"
    a "It's just a whim... Things like ghosts are ridiculous."

    scene bg 048 with blinds
    return


label city08_12:

    l "I don't want to go back into the castle..."
    "I'm sure the castle is still in a state of confusion...{w}\nIt would be best not to go back until things settle down."

    return


label city09_main0:
    $ BG = "bg 041"
    play sound "audio/se/asioto2.ogg"
    show bg 048 with blinds2
    play music "audio/bgm/city1.ogg"


label city09_main:
    call city_null
    $ city_bg = "bg 048"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Three Women" #602
    $ city_button03 = "Elder" #603
    $ city_button04 = "Child" #604
    $ city_button05 = "Soldier A" #605
    $ city_button06 = "Soldier B" #606
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "San Ilia Castle" #612
    $ city_button19 = "Leave City" #619

    while True:
        call cmd_city

        if result == 1 and ivent05 == 0:
            call city08_01
        elif result == 1 and ivent05 == 1:
            call city09_01
        elif result == 2 and ivent05 == 0:
            call city08_02
        elif result == 2 and ivent05 == 1:
            call city09_02
        elif result == 3 and ivent05 == 0:
            call city08_03
        elif result == 3 and ivent05 == 1:
            call city09_03
        elif result == 4 and ivent05 == 0:
            call city08_04
        elif result == 4 and ivent05 == 1:
            call city09_04
        elif result == 5 and ivent05 == 0:
            call city08_05
        elif result == 5 and ivent05 == 1:
            call city09_05
        elif result == 6 and ivent05 == 0:
            call city08_06
        elif result == 6 and ivent05 == 1:
            call city09_06
        elif result == 10 and ivent05 == 0:
            call city08_10
        elif result == 10 and ivent05 == 1:
            call city09_10
        elif result == 11 and ivent05 == 0:
            call city08_11
        elif result == 11 and ivent05 == 1:
            call city09_11
        elif result == 12 and ivent05 == 0:
            call city08_12
        elif result == 12 and ivent05 == 1:
            jump city09b_main0
        elif result == 19:
            return


label city09_01:
    show mob seinen1 with dissolve #700

    youth "My friend saw something strange in the forest to the northwest.{w}\nA child with wings like a butterfly was dancing..."
    youth "Do you think he saw it because of his weak faith?"

    syoukan mob zyoseiy

    youth "...Did something just pass by me...?"

    return


label city09_02:
    show mob zyosei with dissolve #700

    woman_a "Did you hear?{w}\nPeople have been seeing ghosts!"

    show mob zyosei at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman_b "I know... I've heard!{w}\nSomeone saw a girl walking alone on a street... Then she vanished!"

    hide mob2
    show mob madam at center
    with dissolve #700

    woman_c "I heard a voice from my kitchen... But when I checked, nobody was there!"
    woman_c "And the other day my friend was talking to her two neighbors, and before she noticed it a fourth person joined in out of nowhere!"

    show mob madam at xy(X=-160) #700
    show mob zyoseiy at xy(X=160) as mob2
    with dissolve #701

    woman_d "Ahhh... How scary!"
    "There's four people here now too..."

    return


label city09_03:
    show mob rouzin1 with dissolve #700

    elder "Have you come for me...?{w}\nAre you a messenger from heaven?"

    show mob rouzin1 at xy(X=-160) #700
    show mob seineny at xy(X=160) as mob2
    with dissolve #701

    ghost "What? No... You'll probably live for a little while longer."
    elder "Oh, I see."

    return


label city09_04:
    show mob syounen at xy(X=-220) with dissolve #700

    boy "Yaay!"

    show mob syouzyo as mob2 with dissolve #701

    girl "You won't catch me!{image=note}"

    show mob syouzyoy at xy(X=220) as mob3 with dissolve #702

    ghost_girl "Hahahaha!"
    "The ghost and the children are playing tag..."

    return


label city09_05:
    show mob sensi1 with dissolve #700

    soldier_a "I don't know why, but reports of ghosts have been exploding.{w}\nIt must be the work of the Monster Lord who attacked the other day!"
    soldier_a "Curse you, Monster Lord!!{w}\nYou must be laughing in joy with all the ghosts rampaging around!"
    "The Monster Lord actually fainted because of them..."

    return


label city09_06:
    show mob sensi2 with dissolve #700

    soldier_b "The castle is in a serious uproar...{w}\nWhy do strange things keep happening recently?"
    soldier_b "How can bad things keep happening to this sacred ground?"

    return


label city09_10:
    scene bg 150 with blinds
    show mob ozisan1 with dissolve #700

    owner "This isn't funny.{w}\nI can't sell physical weapons to people afraid of ghosts!"
    owner "Damn! I'm so envious of that tool shop owner!"

    scene bg 048 with blinds
    return


label city09_11:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    owner "I sold out my anti-ghost water in three minutes this morning.{w}\nI also sold all my crosses!{w}\nHahaha! Thank you ghosts!"

    scene bg 048 with blinds
    return


label city09_12:

    l "I better not go into the castle right now..."
    "The castle is probably still in a state of confusion...{w}\nUntil it calms down a bit more, I'll stay away."

    return


label city09b_main0:
    $ BG = "bg 041"
    play sound "audio/se/asioto3.ogg"
    scene bg 044 with blinds2
    play music "audio/bgm/castle1.ogg"


label city09b_main:
    call city_null
    $ city_bg = "bg 044"
    $ city_button01 = "Guard A" #601
    $ city_button02 = "Guard B" #602
    $ city_button03 = "Arguing Priests" #603
    $ city_button04 = "Scholar A" #604
    $ city_button05 = "Scholar B" #605
    $ city_button06 = "Soldier" #606
    $ city_button10 = "Chapel" #610
    $ city_button11 = "Basement Library" #611
    $ city_button12 = "Audience Hall" #612
    $ city_button13 = "Guard's Office" #613
    $ city_button19 = "Back to Town" #619

    while True:
        call cmd_city

        if result == 1:
            call city09b_01
        elif result == 2:
            call city09b_02
        elif result == 3:
            call city09b_03
        elif result == 4:
            call city09b_04
        elif result == 5:
            call city09b_05
        elif result == 6:
            call city09b_06
        elif result == 10:
            call city09b_10
        elif result == 11:
            call city09b_11
        elif result == 12:
            call city09b_12
        elif result == 13:
            call city09b_13
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            scene bg 048 with blinds2
            play music "audio/bgm/city1.ogg"
            jump city09_main



label city09b_01:
    show mob heisi1 with dissolve #700

    guard_a "What's with the weird atmosphere...{w}\nThe other guards keep telling me about all the strange stuff that's happening, too..."

    return


label city09b_02:
    show mob heisi1 with dissolve #700

    guard_b "...I'm patrolling the castle.{w}\nI won't let even a bug by me!"

    syoukan mob seineny

    "...It doesn't seem like you can stop the ghosts, though."

    return


label city09b_03:
    show mob sinkan at xy(X=-220) with dissolve #700

    priest_a "You just don't understand!{w}\nNeither monsters nor ghosts should taint this sacred castle!"
    priest_a "If we leave them alone, it's an insult to Ilias!"

    show mob sinkan as mob2 with dissolve #701

    priest_b "The human {i}Soul{/i} is a gift from Ilias!{w}\nTreating them the same as monsters would be an insult to Ilias!"

    show mob sinkany at xy(X=220) as mob3 with dissolve #702

    ghost_priest "I shall read a passage from the Gospel of Mosai.{w}\n{i}The soul is created free from sin{/i}. That's the truth!"
    "That just makes the debate get even more heated..."

    return


label city09b_04:
    show mob gakusya with dissolve #700

    scholar_a "Luckily there wasn't too much damage from the fire.{w}\nAll of us scholars risked our lives by rushing into the flames to save the books."
    scholar_a "But we're having a lot of trouble collecting them...{w}\nAfter they saved the books, it seems like a lot of scholars just ran off with them..."

    return


label city09b_05:
    show mob gakusya with dissolve #700

    scholar_b "I saved one of the original editions of a book on Ilias from the fire...{w}\nI accidentally took it home, and now I can't return it..."
    l "That's not good!{w}\nDoesn't that make you a thief!?"
    "Wait, why can't he return it...?"
    l "Why don't you just secretly return it?"
    scholar_b "Taking a book out of the library is a serious crime...{w}\nIn the past, scholars would take out the books and forge fake additions to them before returning them."
    scholar_b "That's why I can't return it...{w}\nAh, Ilias! I'm sorry for my sin!"
    "Ah, Ilias! I'm sorry about that fire!"

    return


label city09b_06:
    show mob sensi1 with dissolve #700

    soldier "The {i}Goddess Sword{/i} was destroyed by the Monster Lord...{w}\n...But why was it broken so easily?"
    soldier "What would have happened to the Hero who challenged her wielding that?"
    "That's a good question...{w}\nNot ending up with that sword was a stroke of luck..."

    return


label city09b_10:
    scene bg 157 with blinds

    "Possibly because of the ghosts, the chapel was flooded with people."

    show mob obasan at xy(X=-160) #700
    show mob ozisan3 at xy(X=160) as mob2
    with dissolve #701

    woman "Ahh, oh great Ilias!{w}\nPlease send those horrible ghosts back to where they belong!"
    man "We're safe in this sacred place...{w}\nIlias, please help us!"

    show mob seineny at xy(X=-160)  with dissolve #700

    ghost "I'm finally able to pray to Ilias again in this chapel...{w}\nIlias, please watch over me!"
    l ".............."

    scene bg 044 with blinds
    return


label city09b_11:
    show mob heisi1 with dissolve #700

    guard "The library is being repaired. To prevent further damage, I cannot allow anyone not seriously vetted inside."
    guard "But you're safe for sure, Luka. You may enter."
    l "Ah... No, that's ok..."
    "I don't think it would be a good idea to go back inside..."

    return


label city09b_12:
    show mob sensi2 with dissolve #700

    guard_captain "Ah! Mr. Luka!{w}\nDo you have business with the king?"
    l "Ah... Yes...{w}\nHow is the King...?"
    guard_captain "He was disoriented for a while, but he has settled down.{w}\nBut it seems like the memory from the Monster Lord's attack still hasn't come back to him..."
    l "I see..."
    guard_captain "At any rate, you are allowed to go inside.{w}\nBut please don't remind him of the attack."
    l "I won't..."

    play sound "audio/se/asioto3.ogg"
    scene bg 045 with blinds2
    show sanilia g st03 with dissolve #700

    san_ilia_king "Why do my shoulders feel so heavy...?"
    l "Uwa! He's possessed!"
    san_ilia_king "Possessed...?{w} What do you mean?"
    "The King looks around him frantically.{w} It seems like he can't see it..."
    san_ilia_king "My head and shoulders feel so heavy...{w}\nI guess I haven't recovered..."
    l "That... must be it.{w} Please take care of yourself..."
    "I quickly end this uncomfortable meeting."

    scene bg 044 with blinds
    return


label city09b_13:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_d "Someone already drank all the coffee..."
    guard_e "The coffee's gone too?{w} Someone stole the snacks, too!"

    show mob madamy at xy(X=-160) #700
    show mob madamy at xy(X=160) as mob2
    with dissolve #701

    "While the guards argue, two elegant ghost women are talking with tea and crumpets in their hands."
    ghost_madame_a "The air feels stiff in here.{w}\nWould you be a dear and open that window?"
    ghost_madame_b "Of course."

    play sound "audio/se/door3.ogg"
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_d "Kyaa! The window opened by itself!"
    guard_e "Po...Poltergeist!"

    scene bg 044 with blinds
    return


label city10_main0:
    $ BG = "bg 039"
    play sound "audio/se/asioto2.ogg"
    show bg 037 with blinds2
    play music "audio/bgm/city1.ogg"


label city10_main:
    call city_null
    $ city_bg = "bg 037"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Man" #602
    $ city_button03 = "Woman" #603
    $ city_button04 = "Soldier" #604
    $ city_button05 = "Mermaid A" #605
    $ city_button06 = "Mermaid B" #606
    $ city_button07 = "Mermaid Girl" #607
    $ city_button08 = "Mermaid Merchant" #608
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Mermaid Pub" #613
    $ city_button14 = "Meia's House" #614
    $ city_button19 = "Leave Town" #619

    while True:
        if ivent04 == 0:
            $ city_act19 = 0
        elif ivent04 == 1:
            $ city_act19 = 1

        call cmd_city

        if result == 1:
            call city10_01
        elif result == 2:
            call city10_02
        elif result == 3:
            call city10_03
        elif result == 4:
            call city10_04
        elif result == 5:
            call city10_05
        elif result == 6:
            call city10_06
        elif result == 7:
            call city10_07
        elif result == 8:
            call city10_08
        elif result == 10:
            call city10_10
        elif result == 11:
            call city10_11
        elif result == 12:
            call city10_12
        elif result == 13:
            call city10_13
        elif result == 14 and ivent04 == 0:
            return
        elif result == 14 and ivent04 == 1:
            call city10_14
        elif result == 19:
            return


label city10_01:
    show mob mermaid2 at xy(X=160) as mob2
    show mob seinen1 at xy(X=-160) #700
    with dissolve #701

    youth "Since then, Ilias Kreuz hasn't committed any more acts of terrorism.{w}\nIsn't that great, honey?"
    mermaid "But it was scary...{w}\nI don't want my darling to get hurt because of me..."
    youth "If it's for you, I don't mind being hurt, honey.{w}\nEven if I have to give my life, I'll protect you!"
    mermaid "Ahh... Darling!{w}\nYou make me the happiest Mermaid in the world!"
    youth "Ahh... Honey!{w}\nYou're the best treasure in the world!"

    return


label city10_02:
    show mob ozisan1 with dissolve #700

    man "The person who bombed the Mermaid school the other day...{w}\nI heard he's the head of Ilias Kreuz."
    man "Apparently he ran away to the north."

    return


label city10_03:
    show mob musume2 with dissolve #700

    woman "Even though nothing's happened since then, I wonder when something will..."
    woman "As long as this town has Mermaids, I don't think it will stop."
    woman "Of course, the people who are attacking the Mermaids are the bad guys."

    return


label city10_04:
    show mob sensi2 with dissolve #700

    soldier "I heard that the Monster Lord herself attacked San Ilia the other day.{w}\nIt seems like the only damage she did was a fire in the library."
    soldier "It truly must have been Ilias's protection that prevented any further damages..."

    return


label city10_05:
    show mob mermaid2 with dissolve #700

    mermaid_a "Even after such a horrible attack like that, the people of this town still warmly accept us..."
    mermaid_a "That's why we really don't want to involve them..."

    return


label city10_06:
    show mob mermaid1 with dissolve #700

    mermaid_b "Us Mermaids are just bringing trouble for everyone else in this town...{w}\nRecently, we've taken to setting up patrols to help guard the town."
    mermaid_b "We're fine patrolling, since our lower bodies are very strong."
    mermaid_b "Even 200 meters under water, we can still swim at 15 knots.{w}\nWe can shatter a normal human's bones with one swipe of our tail."
    mermaid_b "...I'm kidding!{w}\nWe're actually incredibly weak..."

    return


label city10_07:
    show mob mermaid3 with dissolve #700

    mermaid_girl "My Mermaid song!{image=note}{w}\nCan you hear it?{image=note}"

    call show_skillname(_("Song of Seduction"))
    show mob mermaid3 at xy(X=-200) #700
    show mob ozisan1 at xy(X=160) as mob2
    with dissolve #701

    "A man passing by has been seduced by the Mermaid's song!"

    call hide_skillname

    man "M...m...m...master? {w}\nM...m...m...master!"
    mermaid_girl "Kyaaaa!"

    show mob seinen1 at xy(X=-160) #700
    show mob ozisan1 at xy(X=160) as mob2
    with dissolve #701

    youth "Help! My crazy old man is attacking a little Mermaid girl!"

    show mob seinen1 at xy(X=-240) as mob2 #700
    show mob ozisan1 at center #701
    show mob sensi1 at xy(X=240) as mob3
    with dissolve #702

    soldier "Another Ilias Kreuz attack!?{w}\nGet him!"

    play sound "audio/se/dageki.ogg"
    show mob at slash
    pause 1.0
    play sound "audio/se/dageki.ogg"
    show mob at slash
    pause 1.0
    play sound "audio/se/dageki.ogg"
    show mob at slash

    man "Guha!!"

    play sound "audio/se/down.ogg"
    hide mob with dissolve
    pause
    hide mob2
    hide mob3
    show mob mermaid3
    with dissolve #700

    mermaid_girl "Wawawa...{w} I wasn't supposed to sing that song..."
    mermaid_girl "I'm sorry, mister..."

    return


label city10_08:
    show meia st21 at xy(X=50, Y=50) with dissolve #700

    mermaid_merchant "I have a new product!{w} Grilled Sea Anemone!{w}\nBut it isn't selling well either..."
    l "...Why do you keep grilling such strange things?"
    mermaid_merchant "It's fine, just buy it!{w}\nI want to get out of these ragged clothes..."

    play sound "audio/se/kaimono.ogg"
    pause 2.0
    $ item11 = 1

    "Luka was forced to buy {i}Baked Sea Anemone{/i}!"

    return


label city10_10:
    scene bg 150 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "Welcome!{w}\nIn these dark times of disasters and terrorism, you want to get yourself some good equipment!"
    l "Is that kind of stuff really selling?"
    shopkeeper "These fire-proof clothes are getting really popular.{w}\nEveryone is uneasy about the terrorist attacks..."

    scene bg 037 with blinds
    return


label city10_11:
    scene bg 154 with blinds
    show mob seinen1 with dissolve #700

    shopkeeper "Welcome!"
    shopkeeper "We carry clothes for Mermaids at this shop.{w}\nClothes that expose almost all of the chest area are the most popular these days."
    l "...Really?"
    shopkeeper "I'm kidding... For now.{w}\nBut if our shop advertises it as the new fashion trend, that's what the trend will become."
    shopkeeper "Making lies become truth... Isn't the fashion world wonderful?"
    l "So you want to make such low-cut clothes popular...?"
    shopkeeper "I'm just trying to serve the needs of the men in the world better. Hahaha!"

    show alice st11b at xy(X=-120) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701

    a "........."
    l "Hya! Alice!?"
    shopkeeper "This isn't what you think...{w}\nI'm just... Advertising the newest fashion craze..."
    a "....................Pervert."

    scene bg 037 with blinds
    return


label city10_12:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "It's possible that Ilias Kreuz is actually carrying out Ilias's will.{w}\nIt's heresy for Mermaids and humans to live together in this town as they are..."

    scene bg 037 with blinds
    return


label city10_13:
    scene bg 155 with blinds
    show mob mermaid2 with dissolve #700

    mermaid "Oh... Welcome, you cute Hero.{w}\nWhat are you going to order?"
    l "Ah... I don't drink alcohol."
    mermaid "Then, do you want to hear the story of how Mermaids came to this town?{w}\n...It's a sad story from a hundred years ago, between a young human and a Mermaid."
    mermaid "A long time ago, there was a young sailor named Alan in this town.{w}\nOne day, his ship wrecked while on a voyage."
    mermaid "He was attacked by a shark, and was mortally wounded.{w}\nSuddenly, a beautiful Mermaid named Laura appeared, and saved him."
    mermaid "It was love at first sight for Laura.{w}\nAlan's injury was so severe, that normally it would kill a human."
    mermaid "There was a puncture wound through his left chest that went half an inch into his lung, leaving him in a state of cardiopulmonary arrest."
    mermaid "His spine was also injured in the shark attack, and he had lost a third of his blood."
    l "...That's unusually detailed."
    mermaid "Anyway, normally someone wouldn't survive that.{w}\n...Did I already say he was half dead?"
    mermaid "So, Laura made Alan drink her blood.{w}\nYou may not know, but the blood of a Mermaid is a powerful medicine."
    mermaid "Drinking her blood, Alan was somehow able to recover.{w}\nBut even with her blood, he wasn't able to be completely fine."
    mermaid "For a full year he had to drink her blood every day, or else he would die."
    mermaid "So Laura pretended to be a human, and followed Alan to the town.{w}\nShe secretly mixed her blood in with his food to keep him alive."
    l "...Why did Laura have to disguise herself?"
    mermaid "The town hated monsters.{w}\nSo Laura had no choice but to pretend to be a human."
    mermaid "Months passed like that, as she continued to take care of Alan.{w}\nLaura and Alan fell completely in love..."
    mermaid "One day, it was discovered that Laura was not human.{w}\nAlan's love didn't change, but the other townsfolk were different."
    mermaid "People were terrified of her, and accused her of being the source of all the bad things happening in town."
    mermaid "Finally, soldiers caught her and decided to execute her."
    l "But couldn't she have escaped?{w}\nAs a Mermaid, she would have been stronger than any guard..."
    mermaid "If Laura ran, then Alan wouldn't be able to drink her blood.{w}\nThat's why Laura didn't run."
    l "..........."
    mermaid "Laura decided she would turn herself in and choose to be executed by skewering."
    mermaid "Choosing the bloodiest possible method, she wanted Alan to be able to collect all of her blood."
    l "No... How awful..."
    mermaid "Alan figured out what Laura was planning on doing by the letter she sent him...{w}\nHe decided to kill himself by drinking poison."
    mermaid "If he died, Laura wouldn't be forced to stay by his side to give him her blood.{w}\nHe couldn't bear the thought of Laura being executed for his sake."
    l "..........."
    mermaid "Laura fled from the execution ground after hearing of Alan's death.{w}\nFor the next few days, a storm blew through the town, as if summoned by Laura's grief."
    mermaid "The people of the town regretted their actions.{w}\nThey decided to repent, and came to accept Mermaids."
    mermaid "...The end."
    l "...That's a sad story."
    mermaid "It's a legend, so I don't know how true it really is, but it's been handed down through the years in this town..."

    scene bg 037 with blinds
    return


label city10_14:

    "There's a piece of paper stuck on Meia's door.{w}\nWe're on our honeymoon! We won't be back for a while!"

    return


label city11_main:
    call city_null
    $ city_bg = "bg 048"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Three Women" #602
    $ city_button03 = "Elder" #603
    $ city_button04 = "Playing Children" #604
    $ city_button05 = "Soldier A" #605
    $ city_button06 = "Soldier B" #606
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "San Ilia Castle" #612
    $ city_button19 = "Leave Town" #619

    while True:
        call cmd_city

        if result == 1 and ivent05 == 0:
            call city11_01a
        elif result == 1 and ivent05 == 1:
            call city11_01b
        elif result == 2 and ivent05 == 0:
            call city11_02a
        elif result == 2 and ivent05 == 1:
            call city11_02b
        elif result == 3:
            call city11_03
        elif result == 4 and ivent05 == 0:
            call city11_04a
        elif result == 4 and ivent05 == 1:
            call city11_04b
        elif result == 5 and ivent05 == 0:
            call city11_05a
        elif result == 5 and ivent05 == 1:
            call city11_05b
        elif result == 6 and ivent05 == 0:
            call city11_06a
        elif result == 6 and ivent05 == 1:
            call city11_06b
        elif result == 10 and ivent05 == 0:
            call city11_10a
        elif result == 10 and ivent05 == 1:
            call city11_10b
        elif result == 11 and ivent05 == 0:
            call city11_11a
        elif result == 11 and ivent05 == 1:
            call city11_11b
        elif result == 12:
            jump city11b_main0
        elif result == 19:
            return


label city11_01a:
    show mob seinen1 with dissolve #700

    youth "My friend saw something strange in the forest to the northwest.{w}\nA strange child with wings like a butterfly was dancing around."
    youth "Is it because of his lack of faith that he saw something like that?"

    syoukan fairy st01

    youth "...It looks like my faith is too weak.{w} I'm going to the church..."

    return


label city11_01b:
    show mob seinen1 with dissolve #700

    youth "My friend saw something strange in the forest to the northwest.{w}\nA strange child with wings like a butterfly was dancing around."
    youth "Is it because of his lack of faith that he saw something like that?"

    syoukan mob zyoseiy

    youth "...Did something just pass by me?"

    syoukan fairy st01

    youth "...It looks like my faith is too weak.{w} I'm going to the church..."

    return


label city11_02a:
    show mob zyosei with dissolve #700

    woman_a "Hey, did you hear?{w}\nCreepy demon children have been seen roaming the streets."

    show mob zyosei at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman_b "How scary...{w}\nThe tool shop owner said he saw a pixie, too!"

    hide mob2
    show mob madam at center
    with dissolve #700

    woman_c "Jimmy's crops were ruined, too.{w}\nWe have to be careful..."

    show mob madam at xy(X=-150) #700
    show fairys_a st01 at xy(X=370, Y=100)
    with dissolve #701

    fairy "I want to talk toooo!"

    show mob zyosei at xy(X=-260) #700
    show mob madam as mob2 #701
    show fairys_a st01 at xy(X=460, Y=100)
    with dissolve #702

    woman_a "That's fine, little miss.{w} Feel free to join us."
    woman_b "Oh my!{w} What nice wing accessories.{w}\nI want them too..."
    l "............."

    return


label city11_02b:
    show mob zyosei with dissolve #700

    woman_a "Hey, did you hear?{w}\nCreepy demon children have been seen roaming the streets."

    show mob zyosei at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman_b "I've heard about that...{w}\nThey fly around the streets with hate filled laughter!"

    hide mob2
    show mob madam at center
    with dissolve #700

    woman_c "How scary...{w}\nThe tool shop owner said he saw a pixie, too!"

    show mob madam at xy(X=-160) #700
    show mob zyoseiy at xy(X=160) as mob2
    with dissolve #701

    ghost_woman "Jimmy's crops were ruined, too.{w}\nWe have to be careful..."

    show mob madam at xy(X=-260) #700
    show mob zyoseiy at center as mob2 #701
    show fairys_a st01 at xy(X=460, Y=100)
    with dissolve #702

    fairy "I want to talk toooo!"
    woman_a "That's fine, little miss.{w} Feel free to join us."
    woman_b "Oh my!{w} What nice wing accessories.{w}\nI want them too..."
    l ".............."

    return


label city11_03:
    show fairys_f st51 at xy(X=400, Y=100)
    show mob rouzin1 at xy(X=-160) #700
    with dissolve #701

    elder "Oh how unusual!{w} Are you a fairy?{w}\nWhen I was young, there were a lot of fairies here, too..."
    elder "...Do you want my rice ball?"
    fairy "Yay!{w} Snacks!{image=note}"

    if ivent05 != 0:
        show fairys_f st51 at xy(X=500, Y=100)
        show mob seineny as mob2 #701
        show mob rouzin1 at xy(X=-280) #700
        with dissolve #702

        ghost "After that small stream in the south was filled in, fairies weren't seen anymore."
        ghost "I was around twenty at the time... So around sixty years ago?"
        elder "Ah yes, yes.{w}\nThis is my first time seeing a fairy since then..."

    return


label city11_04a:
    show mob syounen at xy(X=-220) with dissolve #700

    boy "Yaay!"

    show mob syouzyo as mob2 with dissolve #701

    girl "Kyaa! Yay!{image=note}"

    show fairys_c st21 at xy(X=450, Y=100) with dissolve #702

    fairy "Yaay!{image=note}"
    "The children and the fairy are playing tag..."

    return


label city11_04b:
    show mob syounen at xy(X=-320) zorder 4 with dissolve #700

    boy "Yaay!"

    show mob syouzyo at xy(X=-100) zorder 3 as mob2 with dissolve #701

    girl "Kyaa! Yay!{image=note}"

    show mob syouzyoy at xy(X=110) zorder 2 as mob3 with dissolve #702

    ghost_girl "Hahahaha!"

    show fairys_c st21 at xy(X=490, Y=100) with dissolve #703

    fairy "Yaay!{image=note}"
    "The children, ghost and fairy are playing tag..."

    return


label city11_05a:
    show mob sensi1 with dissolve #700

    soldier_a "I don't know why, but reports of little pixies have increased.{w}\nIt must be more fallout from that Monster Lord attack..."
    soldier_a "...But they do say they're actually kind of cute.{w}\n...And that people are feeding them snacks..."

    return


label city11_05b:
    show mob sensi1 with dissolve #700

    soldier_a "Right after the ghost infestation is a pixie infestation...{w}\nAt this rate, San Ilia will be taken over by strange things!"
    soldier_a "...But they do say the pixies are actually kind of cute.{w}\n...And that people are feeding them snacks..."

    return


label city11_06a:
    show mob sensi2 with dissolve #700

    soldier_b "The castle is still in a serious uproar...{w}\nPeople keep reporting unnatural phenomena."
    soldier_b "How can something like this happen in this sacred ground...?"

    return


label city11_06b:
    show mob sensi2 with dissolve #700

    soldier_b "The castle is still in a serious uproar...{w}\nPeople keep reporting unnatural phenomenon."
    soldier_b "Has this land already been forsaken by Ilias?"

    return


label city11_10a:
    scene bg 150 with blinds
    show mob ozisan1 with dissolve #700

    owner "Even if those fearful demon children are attacking...{w}\nSince they're so tiny, my weapons don't sell at all!"
    owner "Damn that tool shop owner!"

    scene bg 048 with blinds
    return


label city11_10b:
    scene bg 150 with blinds
    show mob ozisan1 with dissolve #700

    owner "One after another, this crap keeps happening!{w}\nThe only thing that doesn't change is my lack of sales!"
    owner "Damn that tool shop owner!"

    scene bg 048 with blinds
    return


label city11_11a:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    owner "Sunflower seeds are selling very good right now.{w}\nSince it's those pixies favorite food, people keep buying it hoping to make them into pets."
    owner "Even more surprising, the pixies themselves are coming to buy them!{w}\nNaturally, I don't discriminate with my customers."

    scene bg 048 with blinds
    return


label city11_11b:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    owner "I'm no longer carrying anti-ghost water.{w}\nThe ghosts started to protest outside my shop, so I stopped selling it."
    owner "But now, sunflower seeds are selling well.{w}\nSince it's those pixies favorite food, people keep buying it hoping to make them into pets."
    owner "Even more surprising, the pixies themselves are coming to buy them!{w}\nNaturally, I don't discriminate with my customers."

    scene bg 048 with blinds
    return


label city11b_main0:
    $ BG = "bg 041"
    play sound "audio/se/asioto3.ogg"
    scene bg 044 with blinds2
    play music "audio/bgm/castle1.ogg"


label city11b_main:
    call city_null
    $ city_bg = "bg 044"
    $ city_button01 = "Guard A" #601
    $ city_button02 = "Guard B" #602
    $ city_button03 = "Arguing Priests" #603
    $ city_button04 = "Scholar" #604
    $ city_button05 = "Soldier" #605
    $ city_button10 = "Chapel" #610
    $ city_button11 = "Basement Library" #611
    $ city_button12 = "Audience Hall" #612
    $ city_button13 = "Guard's Office" #613
    $ city_button19 = "Back to Town" #619

    while True:
        call cmd_city

        if result == 1 and ivent05 == 0:
            call city11b_01a
        elif result == 1 and ivent05 == 1:
            call city11b_01b
        elif result == 2:
            call city11b_02
        elif result == 3:
            call city11b_03
        elif result == 4:
            call city11b_04
        elif result == 5:
            call city11b_05
        elif result == 10 and ivent05 == 0:
            call city11b_10a
        elif result == 10 and ivent05 == 1:
            call city11b_10b
        elif result == 11:
            call city11b_11
        elif result == 12 and ivent05 == 0:
            call city11b_12a
        elif result == 12 and ivent05 == 1:
            call city11b_12b
        elif result == 13 and ivent05 == 0:
            call city11b_13a
        elif result == 13 and ivent05 == 1:
            call city11b_13b
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            scene bg 048 with blinds2
            play music "audio/bgm/city1.ogg"
            jump city11_main


label city11b_01a:
    show mob heisi1 with dissolve #700

    guard_a "I keep hearing the laughter of a small girl...{w}\nOther soldiers hear things too... But we can't find the source..."

    return


label city11b_01b:
    show mob heisi1 with dissolve #700

    guard_a "What's with this weird atmosphere...{w}\nWhat's more, I keep hearing a small girl's laughter..."
    guard_a "The soldiers are getting scared with all this weird stuff happening..."

    return


label city11b_02:
    show mob heisi1 with dissolve #700

    guard_b "...I'm patrolling the castle.{w}\nI won't let even a single insect by me!"
    "A fairy slowly creeps toward the Guard's back while holding a frog.{w}\nSuddenly, she throws the frog at him!"
    guard_b "Kyaa!{w} What was that!?{w}\nA... frog?{w} Who threw this!?"
    "The guard doesn't seem to notice the fairy giggling right in front of him."

    return


label city11b_03:
    show mob sinkan at xy(X=-220) with dissolve #700

    priest_a "Recently, pixies have been running wild.{w}\nThese sightings must be because they hold weak hearts."

    if ivent05 == 0:
        show mob sinkan as mob2 with dissolve #701

        priest_b "Hmph. From the book of Muran.{w} {i}Bad people see bad things{/i}.{w}\nFirst, we must stop the evil of man."
    elif ivent05 == 1:
        show mob sinkany as mob2 with dissolve #701

        ghost_priest "Hmph. From the book of Muran.{w} {i}Bad people see bad things{/i}.{w}\nFirst, we must stop the evil of man."

    show fairys_e st41 at xy(X=560, Y=100) with dissolve #702

    fairy "Hey hey, what are you two talking about?{w}\nTell meeee!"
    "It looks like the two priests don't notice the fairy..."

    return


label city11b_04:
    show mob gakusya with dissolve #700

    scholar "In this ancient thousand year old book, there is a lot of information about fairies."
    scholar "In other words, one thousand years ago, fairies were common."
    scholar "Why did they vanish from the scriptures since then...?{w}\nAn interesting question..."

    if ivent05 != 0:
        show mob gakusya at xy(X=-160) #700
        show mob rouziny at xy(X=160) as mob2
        with dissolve #701

        elder_ghost "I'm only 500 years old...{w}\nBut we had picture books that had fairies in it."
        scholar "Is that true!?{w}\nBut now, fairies have vanished from folklore..."
        scholar "Were they unseen for so long that their existence was forgotten?{w}\nOr were they intentionally erased from the scriptures...?"
        scholar "An interesting question..."

    return


label city11b_05:
    show mob sensi1 at xy(X=-160) #700
    show fairys_b st13 at xy(X=430, Y=100)
    with dissolve #701

    soldier "Here... It's a sunflower seed...{w}\nDon't you want it?"
    soldier "...I love cute things."

    return


label city11b_10a:
    scene bg 157 with blinds

    "The chapel is in an uproar because of the fairies."

    show mob obasan at xy(X=-160) #700
    show mob ozisan3 at xy(X=160) as mob2
    with dissolve #701

    woman "Oh great Ilias...{w}\nPlease cleanse us of these pixies..."
    man "We can rest easy in this sacred chapel.{w}\nIlias, please save us from those hateful pixies!"

    show tfairy st01 at xy(X=60, Y=130) with dissolve #700

    fairy_a "God thingy sitting on a cloud, please make the flowers bloom!"
    fairy_b "Also please make mommy and sister happy!"
    l "................"
    "The fairies mimic the humans as they pray..."

    scene bg 044 with blinds
    return


label city11b_10b:
    scene bg 157 with blinds

    "Because of the fairies and ghosts, the chapel is in an uproar."

    show mob obasan at xy(X=-160) #700
    show mob ozisan3 at xy(X=160) as mob2
    with dissolve #701

    woman "Oh great Ilias...{w}\nPlease cleanse us of these pixies and ghosts..."
    man "We can rest easy in this sacred chapel.{w}\nIlias, please save us from those hateful beings!"

    show mob seineny at xy(X=-160) with dissolve #700

    ghost "Oh great Ilias, please watch over me..."

    hide mob2
    show tfairy st01 at xy(X=380, Y=130)
    with dissolve #701

    fairy_a "God thingy sitting on a cloud, please make the flowers bloom!"
    fairy_b "Also please make mommy and sister happy!"
    l "..............."
    "The fairies mimic the humans as they pray..."

    scene bg 044 with blinds
    return


label city11b_11:
    show mob heisi1 with dissolve #700

    guard "The library is still being repaired.{w}\nFor some reason, there are scribbles and drawings appearing all over the castle."
    guard "Because of these strange occurrences, we aren't allowing anyone into the library."
    l "...That sounds serious."

    return


label city11b_12a:
    show mob sensi2 with dissolve #700

    guard_captain "Oh, Mr. Luka!{w}\nThe body and mind of the King has completely recovered!{w}\nBut... It looks like the memory of the attack has been lost forever."
    guard_captain "The psychic shock must have been too much, and he forgot all about it.{w}\nAt any rate, please enter."
    l "O...Ok..."

    play sound "audio/se/asioto3.ogg"
    scene bg 045 with blinds2
    show sanilia t st03 with dissolve #700

    san_ilia_king "Oh, the Hero Luka.{w} I'm glad you came.{w}\nRecently, strange things keep happening in my town!"
    l "............."
    "I wonder if he's really healed..."
    l "Excuse me, King...{w}\nHow does your head feel...?"

    show sanilia t st04 with dissolve #700

    san_ilia_king "Oh?{w} Ahh! What's this!?"
    "The King grips the jar on his head in surprise.{w}\nApparently he hadn't noticed it..."
    san_ilia_king "Wh...Where is my crown!?"
    "Over in the corner of the room, several fairies are playing with his crown."
    "With the fairies invisible to the King, the crown seems like it's moving on its own."
    san_ilia_king "Ah!{w} What's happening around here!?{w} Ilias, please save us!"

    show sanilia tf st04 with dissolve #700

    "The fairies suddenly jump on top of his shoulders.{w}\nI don't know why, but it seems like they really like him."
    "Laughing and giggling, the fairies start to dance and play on his shoulders."
    san_ilia_king "Nooooo... What is this!?{w}\nWhy are my shoulders being tickled by the disembodied voices of little girls!?"
    l "Uhm... You seem busy today..."
    "I quickly walk out of the audience hall."

    scene bg 044 with blinds
    return


label city11b_12b:
    show mob sensi2 with dissolve #700

    guard_captain "Oh, Mr. Luka!{w}\nThe body and mind of the King has completely recovered!{w}\nBut... It looks like the memory of the attack has been lost forever."
    guard_captain "The psychic shock must have been too much, and he forgot all about it.{w}\nAt any rate, please enter."
    l "O...Ok..."

    play sound "audio/se/asioto3.ogg"
    scene bg 045 with blinds2
    show sanilia tg st03 with dissolve #700

    san_ilia_king "Oh, the Hero Luka.{w} I'm glad you came.{w}\nRecently, strange things keep happening in my town!"
    l "............."
    "I wonder if he's really healed..."
    l "Excuse me, King...{w}\nHow does your head feel...?"

    show sanilia t st04 with dissolve #700

    san_ilia_king "Oh?{w} Ahh! What's this!?"
    "The King grips the jar on his head in surprise.{w}\nApparently he didn't notice it..."
    san_ilia_king "Wh...Where is my crown!?"
    "Over in the corner of the room, several fairies are playing with his crown."
    "With the fairies seemingly invisible to the King, the crown seems like it's moving on its own."
    san_ilia_king "Ah!{w} What's happening around here!?{w} Ilias, please save us!"

    show sanilia tf st04 with dissolve #700

    "The fairies suddenly jump on top of his shoulders.{w}\nI don't know why, but it seems like they really like him."
    "Laughing and giggling, the fairies start to dance and play on his shoulders."
    san_ilia_king "Nooooo... What is this!?{w}\nWhy are my shoulders being tickled by the disembodied voices of little girls!?"
    l "Uhm... You seem busy today..."
    "I quickly walk out of the audience hall."

    scene bg 044 with blinds
    return


label city11b_13a:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_d "Oh? Are we already out of sugar and coffee?"
    guard_e "Sugar now too? Where are all of the snacks going!?"

    hide mob
    hide mob2
    show fairys_a st01 at xy(X=60, Y=100) #700
    show fairys_c st21 at xy(X=370, Y=100)
    with dissolve #701

    fairy_a "I{w} Love{w} Snacks!{image=note}"
    fairy_b "Sugar{w} Is{w} Delicious!{image=note}"
    "A fairy peeps her head out from the sugar jar as she sings a song."
    fairy_c "Wawawawawa!"
    "Another fairy at the other side of the room has her face stuck in a jar."

    play sound "audio/se/break5.ogg"

    "As she struggles to get out, the vase drops on the floor and shatters!"

    hide fairys_a
    hide fairys_c
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_c "Aiiee!{w} Poltergeist!"
    guard_d "This castle! I'm sick of it!"

    scene bg 044 with blinds
    return


label city11b_13b:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_d "We're out of coffee awfully fast today..."
    guard_e "The sugar and cake is gone too."

    show mob madamy at xy(X=-160) #700
    show mob madamy at xy(X=160) as mob2
    with dissolve #701

    "On the other end of the room, two elegant ghost women are chatting."
    ghost_madame_a "This castle is getting awfully noisy."
    ghost_madame_b "It's those little children!"

    hide mob
    hide mob2
    show fairys_a st01 at xy(X=60, Y=100) #700
    show fairys_c st21 at xy(X=370, Y=100)
    with dissolve #701

    fairy_a "I{w} Love{w} Snacks!{image=note}"
    fairy_b "Sugar{w} Is{w} Delicious!{image=note}"
    "A fairy peeps her head out from the sugar jar as she sings a song."
    fairy_c "Wawawawawa!"
    "Another fairy at the other side of the room has her face stuck in a jar."

    play sound "audio/se/break5.ogg"

    "As she struggles to get out, the vase drops on the floor and shatters!"

    hide fairys_a
    hide fairys_c
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_c "Aiiee!{w} Poltergeist!"
    guard_d "I'm sick of this castle!"

    scene bg 044 with blinds
    return



label city12_main:
    call city_null
    $ city_bg = "bg 064"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Woman" #602
    $ city_button03 = "Elder" #603
    $ city_button04 = "Man" #604
    $ city_button05 = "Soldier A" #605
    $ city_button06 = "Soldier B" #606
    $ city_button07 = "Boy" #607
    $ city_button08 = "Prostitute" #608
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Mermaid Pub" #614
    $ city_button14 = "Sabasa Castle" #613
    $ city_button19 = "Hotel" #619


    while True:
        if hanyo[0] + hanyo[1] < 2:
            $ city_act19 = 0
        elif hanyo[0]+hanyo[1] == 2:
            $ city_act19 = 1

        call cmd_city

        if result == 1:
            call city12_01
        elif result == 2:
            call city12_02
        elif result == 3:
            call city12_03
        elif result == 4:
            call city12_04
        elif result == 5:
            call city12_05
        elif result == 6:
            call city12_06
        elif result == 7:
            call city12_07
        elif result == 8:
            call city12_08
        elif result == 10:
            call city12_10
        elif result == 11:
            call city12_11
        elif result == 12:
            call city12_12
        elif result == 13:
            call city12_14
        elif result == 14:
            play sound "audio/se/asioto2.ogg"
            scene bg 065 with blinds2
            play music "audio/bgm/castle2.ogg"
            jump city12b_main
        elif result == 19:
            return


label city12_01:
    show mob seinen1 with dissolve #700

    youth "The desert around here is really harsh.{w}\nBut due to our King's ability, we've been able to thrive in spite of that."
    youth "The current King is the ninth generation, but he's just as good as his predecessors."
    youth "But... Is there going to be an issue with succession?{w}\nEver since the Queen died, the King has remained unmarried.{w}\nHe hasn't taken a concubine or anything either... There's just the Princess."
    youth "What's going to happen...?"
    l ".............."
    "Is the kidnapping related to the succession of the throne...?"

    return


label city12_02:
    show mob obasan at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman "The King is a wonderful man... But he isn't immortal.{w}\nWhat's going to happen when he dies?"
    woman "Will the Princess take the throne as the first Queen of Sabasa?"
    lady "The man she marries should become the next King.{w}\nI've heard that the King has been secretly looking for suitable marriage partners in the last few days..."
    l ".............."
    "Is the kidnapping related to the succession of the throne...?"

    return


label city12_03:
    show mob rouzin1 with dissolve #700

    elder "I'm interested in the legends around the world, especially of the Pyramid."
    elder "It's said that deep inside of the Pyramid is the legendary Sphinx.{w}\nExisting for over a thousand years, it's extremely powerful."
    elder "It's said that crazy monster is guarding an incredible treasure that will be given to the one who can overcome the trial of the Pyramid."
    l "A trial and treasure..."
    "I wonder if the trial and that treasure are related to the kidnapping?{w}\n...That can't be it, can it?"

    return


label city12_04:
    show mob ozisan2 with dissolve #700

    man "This area was hit by the Ilias Kreuz terrorists as well.{w}\nThey attacked the bazaar, claiming we were doing business with monsters."
    man "But the King cracked down on them swiftly and without mercy.{w}\nSince then, they haven't tried anything else here."

    return


label city12_05:
    show mob sensi1 with dissolve #700

    soldier_a "It seems as though you can undergo the {i}Dragon Seal Trial{/i} in the pyramid.{w}\nNobody really knows what it is, but everyone who has gone to challenge it hasn't returned."
    soldier_a "If someone managed to pass it, they would become a household name.{w}\n...But nobody has come back after going out to attempt it."

    return


label city12_06:
    show mob sensi2 with dissolve #700

    soldier_b "Yesterday, some soldier was walking up to people and staring at them in a really creepy manner."
    soldier_b "After a few seconds he would sigh and say \"This man is no good\"."
    soldier_b "It really depressed everyone for some reason..."

    return


label city12_07:
    show mob syounen with dissolve #700

    boy "I'm going to grow up and marry the Princess!"

    show mob seinen1 at xy(X=-160) as mob2 with dissolve #701

    youth "Me too!"

    show mob ozisan3 at xy(X=160) as mob3 with dissolve #702

    man "Me too!"

    show mob sensi1 at xy(X=-300) as mob4 with dissolve #703

    soldier "Same here!"

    show mob rouzin1 at xy(X=300) as mob5 with dissolve #704

    elder "I will too!"
    boy "Yaay!"
    l "..............."
    "The Princess seems pretty popular among the men.{w}\nIs her popularity related to the kidnapping...?"

    return


label city12_08:
    show mob musume2 with dissolve #700

    prostitute "Does the cute little Hero want to play with me?{w}\nDo you want to try some Princess role playing?"
    l "Princess roleplaying...?"
    prostitute "Hehe... I dress up as the cute Princess.{w}\nThere are a lot of men who are into that these days..."
    l "................."
    "People seem to be pretty obsessed with the Princess...{w}\nIs this related to the kidnapping?"

    return


label city12_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    owner "Welcome to my five-star establishment!{w}\nPlease look around to your hearts content!"
    l "Thanks..."
    "Everything here looks better than all the other towns...{w}\nBut nothing is better than my current sword."

    scene bg 064 with blinds
    return


label city12_11:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "Welcome!{w} Are you looking for something in particular?{w}\nHow about some dried scorpions as emergency provisions?"
    l "Noooooooooooooooooooo!"

    play sound "audio/se/ikazuti.ogg"
    with Quake((0, 0, 0, 0), 0.75, dist=25)

    "The Shopkeeper attacks Luka with psychological trauma!"

    scene bg 064 with blinds
    return


label city12_12:
    show mob sinkan #700
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "South of here is the {i}Witch Hunt Village{/i}.{w}\nThe female Lord arrests villagers and travelers accused of being witches.{w}\nShe tortures them until they confess, then executes them."
    priest "Anyone accused is arrested right away.{w}\nRegardless of age or sex, she prosecutes any suspects."
    l "What!?{w} That's horrible..."
    priest "I am a man that serves Ilias.{w}\nI cannot allow monsters or witches to continue living."
    priest "But that woman's acts are in excess.{w}\nThe guilty and innocent alike are being killed by her."
    "Is she related to Ilias Kreuz?{w}\nIt's pretty apparent that she must have a big hatred of monsters..."
    l "That's inexcusable!"

    hide mob
    show alice st11b
    with dissolve #700

    a "Calm down.{w}\nDon't you already have to deal with the Princess and Gnome?"
    l "...So what?"
    a "...Are you serious?"
    "Alice lifts her eyebrows at me."
    l "I can't let such a horrible thing happen.{w}\nJust like monsters attacking humans, I can't have humans attacking monsters."

    show alice st14b with dissolve #700

    a "................."

    $ hanyo[1] = 1
    scene bg 064 with blinds
    return


label city12_14:
    scene bg 155 with blinds
    show mob mermaid1 with dissolve #700

    mermaid "Welcome to the Mermaid Pub! Can I help you?"
    l "Oh? There's a Mermaid Pub in this town too?"
    mermaid "Yes! The King allowed us to open up here."
    mermaid "Now we have two locations!{w}\nHopefully one day we can have Mermaid Pubs all over the world!"
    l "Oh?{w} Good luck!"
    mermaid "Once our Mermaid Pub conquers the world, the Queen may come out!"
    l "The Queen?{w} The Mermaid Queen?"
    mermaid "Yes... The Queen hates humans for some reason.{w}\nAfter we became friendly with humans, she vanished."
    mermaid "Why does she hate them so much?{w} Where did she go?{w} Nobody knows..."
    l "I see..."
    "The Mermaid Queen hates humans...{w}\nIt doesn't look like even the Mermaids themselves know why."

    scene bg 064 with blinds
    return


label city12b_main:
    call city_null
    $ city_bg = "bg 065"

    $ city_button01 = "Minister A" #601
    $ city_button02 = "Minister B" #602
    $ city_button03 = "Chief Retainer" #603
    $ city_button04 = "Sabasa Knight Leader" #604
    $ city_button05 = "Scholar" #605
    $ city_button06 = "Priest" #606
    $ city_button10 = "Audience Hall" #610
    $ city_button11 = "Princesses Room" #611
    $ city_button12 = "Soldier's Office" #612
    $ city_button19 = "Return to Town" #619

    while True:
        call cmd_city

        if result == 1:
            call city12b_01
        elif result == 2:
            call city12b_02
        elif result == 3:
            call city12b_03
        elif result == 4:
            call city12b_04
        elif result == 5:
            call city12b_05
        elif result == 6:
            call city12b_06
        elif result == 10:
            call city12b_10
        elif result == 11:
            call city12b_11
        elif result == 12:
            call city12b_12
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            scene bg 064 with blinds2
            play music "audio/bgm/sabasa.ogg"
            jump city12_main


label city12b_01:
    show mob daizin with dissolve #700

    minister_a "When the Princess was kidnapped, the King tried to rush off to the Pyramid alone."
    minister_a "But with all of the vassals help, we were able to get him to stop.{w}\nBut if we can't save her soon, I'm sure he'll run off alone..."
    minister_a "Please, Hero! You must save the Princess!"

    return


label city12b_02:
    show mob daizin with dissolve #700

    minister_b "They went to all the trouble to kidnap the Princess...{w}\nThey won't harm her that easily...{w} I want to think that..."

    return


label city12b_03:
    show mob rouzin1 with dissolve #700

    chief_retainer "The wife of the first King of Sabasa Castle was a monster.{w}\nThat is, the royal family's bloodline contains monster blood..."
    l "Are you allowed to talk about such a thing so openly...?"
    chief_retainer "It's no problem. It's a known legend.{w}\nThe royal family doesn't hide it.{w} Rather, they take pride in it."
    l "Oh... Is that so...?"

    hide mob
    show alice st12b
    with dissolve #700

    a "Hmm... A King with such a nice viewpoint.{w}\nIt seems as though he's the closest human to your ideal society, Luka."
    a "Better than a discriminating fake Hero who still believes in that angel's silly prohibition."
    l "I... I don't discriminate!{w}\nI just... Believe in the prohibition..."

    show alice st13b with dissolve #700

    a "...Idiot.{w}\nIf Ilias told you to jump off a bridge and die, would you?"
    l "...She wouldn't ask that..."

    $ hanyo[0] = 1
    return


label city12b_04:
    show mob sensi2 with dissolve #700

    sabasa_knight_leader "The fact that the Princess has been kidnapped is confidential.{w}\nBut we won't be able to hide it forever..."
    sabasa_knight_leader "If we can't save her in secret, dire measures will need to be taken.{w}\nWe might even have to deploy the entire army to the Pyramid."
    sabasa_knight_leader "I'm already secretly preparing in case it comes to that..."

    return


label city12b_05:
    show mob gakusya with dissolve #700

    scholar "What is the Pyramid?{w} Why, it's a giant labyrinthine dungeon made by monsters thousands of years ago."
    scholar "According to the legends, it appears as though it was made for some sort of trial."
    l "A trial...?"
    scholar "The {i}Dragon Seal Trial{/i} is what it seemed to be called."
    scholar "There used to be an old document in the library about it...{w}\nBut it was lost a few years ago."

    return


label city12b_06:
    show mob sinkan with dissolve #700

    priest "I've been praying to Ilias every day after the Princess was kidnapped.{w}\nPlease save her, Ilias..."

    return


label city12b_10:
    scene bg 045 with blinds
    show sabasa st01 with dissolve #700

    sabasa_king "Do you require something, Luka?{w}\nDo you need weapons or armor? I'll do anything in my power to procure it for you."
    l "No... I'm fine as is."

    show sabasa st03 with dissolve #700

    sabasa_king "I wanted to go the Pyramid myself when she was taken...{w}\nBut my vassals convinced me that the body holding up the nation can't do something that risky..."
    sabasa_king "Please save her, Luka!"
    "With his words of encouragement, I leave the audience hall."

    scene bg 065 with blinds
    return


label city12b_11:
    scene bg 067 with blinds
    show mob madam with dissolve #700

    chamberlain "This is the Princesses room.{w}\nI cannot let men enter it recklessly..."
    chamberlain "...By any chance, are you the Hero Luka?"
    l "Yes... I am..."
    chamberlain "Ahh... Please save the Princess!{w}\nShe's pious, modest, and so gentle..."
    chamberlain "Such an innocent Princess falling into a monster's hands...{w}\nAhh! How horrible!"

    scene bg 065 with blinds
    return


label city12b_12:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    soldier_a "Something feels off about the castle.{w}\nWe've been given orders to standby and check our equipment."
    soldier_a "Is something about to happen...?"
    soldier_b "It's as if we're preparing for a war...{w}\nBut we don't have any strained relations with anyone as far as I'm aware of."
    soldier_b "The King wouldn't ever do a surprise one-sided attack, either.{w}\nJust what's going on...?"

    scene bg 065 with blinds
    return


label city13_main0:
    $ BG = "bg 062"
    play sound "audio/se/asioto2.ogg"
    scene bg 064 with blinds2
    play music "audio/bgm/sabasa.ogg"


label city13_main:
    call city_null
    $ city_bg = "bg 064"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Woman" #602
    $ city_button03 = "Elder" #603
    $ city_button04 = "Man" #604
    $ city_button05 = "Soldier A" #605
    $ city_button06 = "Soldier B" #606
    $ city_button07 = "Boy" #607
    $ city_button08 = "Prostitute" #608
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Church" #612
    $ city_button13 = "Mermaid Pub" #614
    $ city_button14 = "Sabasa Castle" #613
    $ city_button19 = "Leave Town" #619

    while True:
        call cmd_city

        if result == 1 and ivent06 == 0:
            call city12_01
        elif result == 1 and ivent06 == 1:
            call city13_01
        elif result == 2 and ivent06 == 0:
            call city12_02
        elif result == 2 and ivent06 == 1:
            call city13_02
        elif result == 3 and ivent06 == 0:
            call city12_03
        elif result == 3 and ivent06 == 1:
            call city13_03
        elif result == 4 and ivent06 == 0:
            call city12_04
        elif result == 4 and ivent06 == 1:
            call city13_04
        elif result == 5 and ivent06 == 0:
            call city12_05
        elif result == 5 and ivent06 == 1:
            call city13_05
        elif result == 6 and ivent06 == 0:
            call city12_06
        elif result == 6 and ivent06 == 1:
            call city13_06
        elif result == 7 and ivent06 == 0:
            call city12_07
        elif result == 7 and ivent06 == 1:
            call city13_07
        elif result == 8 and ivent06 == 0:
            call city12_08
        elif result == 8 and ivent06 == 1:
            call city13_08
        elif result == 10:
            call city12_10
        elif result == 11:
            call city12_11
        elif result == 12 and ivent06 == 0:
            call city12_12
        elif result == 12 and ivent06 == 1 and ivent07 == 0:
            call city13_12a
        elif result == 12 and ivent06 == 1 and ivent07 == 1:
            call city13_12b
        elif result == 14:
            play sound "audio/se/asioto2.ogg"
            scene bg 065 with blinds2
            play music "audio/bgm/castle2.ogg"
            jump city13b_main
        elif result == 13 and ivent06 == 0:
            call city12_14
        elif result == 13 and ivent06 == 1 and check02 == 0:
            call city13_14a
        elif result == 13 and ivent06 == 1 and check02 == 1:
            call city13_14b
        elif result == 19:
            return


label city13_01:
    show mob seinen1 with dissolve #700

    youth "I've heard that the Princess is training with a sword these days.{w}\nI guess strength is expected from her if she's going to be the Queen."
    youth "Our royal family came from a famous Hero, after all.{w}\nOther countries weak royals don't hold a candle to ours."

    return


label city13_02:
    show mob obasan at xy(X=-160) #700
    show mob madam at xy(X=160) as mob2
    with dissolve #701

    woman "The Princess is almost of age...{w}\nWhy isn't there more excitement...?"
    lady "The previous Queen was deeply in love with the King before her death.{w}\nNobody wants her to rush into a marriage of political convenience."

    return


label city13_03:
    show mob rouzin1 with dissolve #700

    elder "I love myths, but...{w}\nEvery area seems to have their own special legendary creature."
    elder "Kraken in the southern seas, Sphinx in the western deserts.{w}\nYamata no Orochi to the west, and Poseidon in the north sea."
    elder "I'd like to see them all before I die...{w}\n...But as soon as I meet one, that would probably be the end of my life..."

    return


label city13_04:
    show mob ozisan2 with dissolve #700

    man "I approved of Ilias Kreuz a few years ago, but now I'm a moderate.{w}\nThey're making enemies all over the world with their terrorism."
    man "Losing their original purpose, they're just horrible terrorists now.{w}\nTheir members are deserting too... They're only one tenth of what they used to be."

    return


label city13_05:
    show mob sensi1 with dissolve #700

    soldier_a "I tried the {i}Dragon Seal Trial{/i} as well.{w}\nBut the Mummy Girl right at the entrance was too much for me."
    soldier_a "I was really lucky, and managed to run away.{w}\nI shouldn't have thought becoming famous by finishing the trial would have been easy..."

    return


label city13_06:
    show mob sensi2 with dissolve #700

    soldier_b "The monsters around here are really strong...{w}\nBut if you go further north, they're even stronger."
    soldier_b "I think I'll just retire here."

    return


label city13_07:
    show mob syounen with dissolve #700

    boy "Who here loves the Princess!?"

    show mob seinen1 at xy(X=-160) as mob2 with dissolve #701

    youth "If I do, what are you going to do about it!?"

    show mob ozisan3 at xy(X=160) as mob3 with dissolve #702

    man "I'd desert my wife for her!"

    show mob sensi1 at xy(X=-300) as mob4 with dissolve #703

    soldier "I'd throw away my life for her!"

    show mob rouzin1 at xy(X=300) as mob5 with dissolve #704

    elder "I'll trade my wife for her!"
    boy "Yaay!"
    l "................."
    "She's just as popular as ever."

    return


label city13_08:
    show mob musume2 with dissolve #700

    prostitute "I snapped at a customer when I was playing the Princess.{w}\n{i}The Princess would never do a blowjob!{/i} I yelled at him."
    prostitute "The Princess would never do something like that!{w}\nDon't you think so too?"
    l "Ehh... I wonder..."

    return


label city13_12a:
    show mob sinkan #700
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "Some of the villagers who fled the {i}Witch Hunt Village{/i} are being protected by the church."
    priest "Something truly terrible is happening in that village."
    priest "When I looked deeper, an astounding number of travelers are vanishing upon entering that village."
    priest "Please, make sure you do not approach there."

    scene bg 064 with blinds
    return


label city13_12b:
    show mob sinkan #700
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "There was a strange entry in this book on animism."
    priest "{i}When Sylph is summoned too often, she will be tired out.{w}\nWhen exhausted, something may change{/i}."
    priest "Just what does it mean?"

    scene bg 064 with blinds
    return


label city13_14a:
    scene bg 155 with blinds
    show mob mermaid1 with dissolve #700

    mermaid "Welcome to the second Mermaid Pub!"

    hide mob with dissolve

    "It's still daytime, so there aren't too many customers.{w}\nAs I take a look around, I see someone I wish I hadn't..."
    l "You!"

    play music "audio/bgm/kiki2.ogg"
    show lazarus st05 with dissolve #700

    "Lazarus.{w} My father's best friend, and the new leader of Ilias Kreuz.{w}\nThe fanatic terrorist who bombed that school..."
    "...What is he doing here?{w}\nIn a Mermaid Pub of all places, quietly drinking alcohol..."

    show lazarus st01 with dissolve #700

    lazarus "Hm... What's wrong, boy?{w}\nDo you have business with me?"
    "Lazarus turns his gaze toward me as I glare at him."
    "Complex emotions whirl in my mind.{w}\nBut Alice's words slowly calm me down."
    "My father's crimes are not my own...{w}\nI can't let them bind me down..."
    l "...No.{w} I don't."
    "I calm down and bluntly reply.{w}\nBut Lazarus gives a surprising response."

    show lazarus st02 with dissolve #700

    lazarus "That's good... Sit here, boy.{w}\nIt's depressing to drink alone, you know?"
    l "Eh...?"
    "What would this kind of guy want with someone like me...{w}\nI expected him to blow me off after my response."
    l "..................."
    "Trying to control myself, I sit down on the other side of the table.{w}\nIf I lose my cool here, it would be his victory..."
    lazarus "Hey, waitress.{w} Can we get an orange juice?"
    mermaid "Right away!"
    l "Don't screw around, I'm not a ch..."
    lazarus "If you start drinking alcohol at your age, you'll be a crappy adult."
    l "................"
    "I keep silent and sip at the juice.{w}\nWe sit in silence for a while..."
    "The liquor that Lazarus is drinking is the same kind my father liked..."
    l "Why are you here?{w}\nAre you going to bomb something here, too?"
    "I bring up the subject I wanted to."
    "Even if I forget about my father, this man is a criminal.{w}\nIf he's up to something, I have to stop him."

    show lazarus st01 with dissolve #700

    lazarus "Oh... Do you know who I am, boy?{w}\nI didn't think I had become so popular with kids."
    "Lazarus barely looks fazed at my question.{w}\nHe lets out a big sigh after taking another sip of alcohol."

    show lazarus st05 with dissolve #700

    lazarus "...Anyway, I'm not doing anything here.{w}\nI'm just taking the desert route back to Gold Port to rest at our headquarters."
    l "................."
    "Ilias Kreuz's headquarters is at Gold Port at the northernmost part of the continent."
    "From Port Natalia there are two routes there.{w}\nWest through Sabasa, or East through Noah."
    "The desert route is harsh, but you would attract less attention...{w}\nThat must be why Lazarus is here."
    l "...Why did you want me to sit with you?{w}\nJust what are you up to?"

    show lazarus st04 with dissolve #700

    lazarus "Why do you think I'm up to something?{w}\nDo I smell funny or something?"
    l "Then why?"

    show lazarus st03 with dissolve #700

    lazarus "Your eyes look just like his...{w}\nMy friend..."
    l "....!"
    "I choke on my juice at his words."

    show lazarus st04 with dissolve #700

    lazarus "Oi, oi... What's wrong?{w}\nAre you drunk on orange juice?"
    l "No... It's nothing...{w}\nSo...{w} I look like your friend?"

    show lazarus st05 with dissolve #700

    lazarus "...He died a long time ago.{w}\nIn a really pathetic way, too."
    l "...I hate to say it, but I agree."

    show lazarus st01 with dissolve #700

    lazarus "........?{w}\nI don't know why, but you're an interesting boy."
    lazarus "I wonder if this meeting was fate..."
    "...I don't want to think I'm tied by fate to this guy in any way."
    lazarus "So, are you an adventurer or something?{w}\nYou carry yourself pretty well."
    l "...Is that how you try to recruit people?"
    lazarus "Ya know, I was a soldier myself a long time ago.{w}\nI can tell your ability just from looking."
    lazarus "It seems like you use mainly quick attacks, and thrusting moves.{w}\nJudging from how worn out your shoes are, that is."
    lazarus "The way you walk, it's obvious you're used to quickly moving around as you fight.{w}\nBut on the other hand, your chest and arms are still weak and undeveloped."
    lazarus "It seems as though you don't have much combat experience, but what you have experienced has been done in a pretty short amount of time."
    lazarus "Judging from the way your best points have been focused on, you must have a pretty good teacher."
    l "................"
    lazarus "If you're an adventurer, why are you training so desperately?{w}\nMoney?{w} Honor?{w} Revenge?"
    l "All wrong!{w}\nI fight for the weak!"

    show lazarus st02 with dissolve #700

    lazarus "...Oh, that's funny.{w} I fight for the weak, too."
    l "No!{w} I fight for man and monster coexisting together!"
    l "Don't think you're the same as me!"

    show lazarus st05 with dissolve #700

    lazarus "Coexistence?{w} That's impossible."
    "Lazarus came back with a surprising answer."
    "I thought he'd give an angry response.{w}\nDenying my dream with indignation and anger..."
    "But he just shrugged and flatly responded.{w}\nUnable to accept that bland response as his answer, I just get even angrier."
    l "Why isn't it possible!?{w}\nWhat the hell do you know!?"

    show lazarus st03 with dissolve #700

    lazarus "I know it's impossible...{w}\nOne day, you will too."
    l "I'm different from you!"

    show lazarus st01 with dissolve #700

    lazarus "...We're the same.{w}\nWielding a sword in the name of coexistence."
    lazarus "I'm guessing you've met quite a few monsters or humans who disagree with you, and you've had to use that to crush their beliefs with your own."
    lazarus "How is that any different from me?"
    l "It's different!{w} I'm fighting for an ideal world!"
    lazarus "That ideal is your ideal.{w}\nWhy do you think yours is correct?"
    lazarus "Thinking you must be right... That's just arrogance.{w}\nI know what happens when you take that arrogance to the end."
    l "...So what about you?{w}\nDo you believe what you're doing is right?"
    lazarus "I know very well the self-righteousness of my actions.{w}\nYou realize your own too, right?"
    lazarus "Coexistence of man and monster...{w}\nIf you come across someone who disagrees with you, are you going to crush them with your \"justice\"?"
    "Lazarus stands up as he says that.{w}\nBefore I had noticed, his glass was empty."

    show lazarus st02 with dissolve #700

    lazarus "The most important thing for justice is persuasion.{w}\nIf you can't persuade anyone, your justice will fail."
    l "...If you know that, then why are you doing what you're doing!?"

    show lazarus st03 with dissolve #700

    lazarus "...My path can't be changed anymore.{w}\nMy friend was killed by monsters..."
    "Lazarus puts money on the table."
    lazarus "...Put that kid's juice on my bill too, waitress."
    l "I don't want anything from you!"

    show lazarus st02 with dissolve #700

    lazarus "Hey, don't say that.{w}\nI just want to treat the boy who reminded me of my friend."
    "Saying that, Lazarus starts to head out of the bar.{w}\nRight before leaving, he glances back at me."

    show lazarus st01 with dissolve #700

    lazarus "...Make sure you get out of this bar quickly after finishing that juice.{w}\nA kid like you shouldn't be in this place..."
    l "Don't screw with me!  What ar..."

    play sound "audio/se/asioto2.ogg"
    hide lazarus with dissolve

    "Ignoring my yell, Lazarus heads out of the bar."
    l "Damn it..."
    "I'm in no mood to finish off this juice.{w}\nDisgusted, I leave the bar."

    play sound "audio/se/asioto2.ogg"
    scene bg 064 with blinds2
    play music "audio/bgm/sabasa.ogg"
    show alice st19b with dissolve #700

    "Alice joins me outside, and gladly finishes off my juice."
    a "Don't you want some juice too?{w}\nThe Sabasa Oranges have quite a rich taste to them..."
    l "...Sorry, but I'm not in the mood for it.{w}\nStarting today, I hate orange juice."

    show alice st11b with dissolve #700

    a "......?"

    hide alice with dissolve

    l "That guy...{w}\nWhy would he go to the Mermaid Pub of all places...?"

    # РїРµСЂРµРєСЂР°С€РёРІР°РµРј РёР·РѕР±СЂР°Р¶РµРЅРёСЏ РІ РѕРґРёРЅ С†РІРµС‚ #eeeeee
    show bg 064 grayscale
    show bg 155 grayscale with dissolve
    show lazarus st01 grayscale #700

    lazarus "...Make sure you get out of this bar quickly after finishing that juice.{w}\nA kid like you shouldn't be in this place..."

    # РїРµСЂРµРєСЂР°С€РёРІР°РµРј РёР·РѕР±СЂР°Р¶РµРЅРёСЏ РІ РѕРґРёРЅ С†РІРµС‚ off
    hide lazarus
    show bg 155 with dissolve
    scene bg 064 with dissolve

    l "Oh shit!{w} He couldn't....!"

    pause 0.5
    show bg 155
    show mob mermaid1 #700
    with blinds

    mermaid "Oh, he left his bag here...{w}\nI need to return it back to the cust..."

    stop music fadeout 1
    pause 1.0
    scene bg white with dissolve
    play sound "audio/se/bom1.ogg"
    with Quake((0, 0, 0, 0), 2.0, dist=55)
    pause 1.0
    scene bg 064 with Dissolve(3.0)
    play music "audio/bgm/kiki2.ogg"

    l "No!!"
    "An explosion shakes the ground.{w}\nI stand in horror at the sight of the destroyed Mermaid Pub in front of me."
    "Why didn't I realize it sooner...{w}\nI knew he was a terrorist.{w} Why wasn't I able to stop it!?"
    "The anger and regret swirls in my body as I'm consumed by rage."
    l "...LAZARUS!!!!"

    show alice st13b with dissolve #700

    a "Quiet down, it doesn't seem like anyone is dead.{w}\nIt looks like both the Mermaids and humans are fine."
    "Soldiers quickly fill the area as they start to pull out the wounded.{w}\nAs Alice said, it doesn't seem like anyone is dead..."

    play music "audio/bgm/hutuu2.ogg"

    l "Hey, Alice...{w}\nIs what I'm doing just self-righteous?"

    show alice st11b with dissolve #700

    a "...Who knows.{w} Good and evil is relative.{w}\nNo matter what you say, it just comes down to stupid wordplay."
    l "But... I don't even even listen to humans and monsters who disagree with me.{w}\nI just ignore them..."

    show alice st14b with dissolve #700

    a "I see..."

    hide alice with dissolve

    "Some monsters attack humans, and some humans attack monsters...{w}\nAnd whatever the reason, the Four Heavenly Knights are attacking or disrupting human towns."
    "Even if I'm a hypocrite... Even if it's self-righteousness, I can't allow that.{w}\nI can't allow either side to commit violence or persecute the other."
    l "Let's go, Alice.{w}\nWe still have a lot to do!"
    "Renewing my oath, I continue on my path."

    hide alice with dissolve
    $ check02 = 1
    pause 1.0
    play music "audio/bgm/sabasa.ogg"
    return


label city13_14b:

    "There's a poster in front of the collapsed building.{w}\n{i}The re-opening of the second Mermaid Pub is still undecided{/i}."

    return


label city13b_main0:
    $ BG = "bg 062"
    play sound "audio/se/asioto3.ogg"
    scene bg 065 with blinds2
    play music "audio/bgm/castle2.ogg"


label city13b_main:
    call city_null
    $ city_bg = "bg 065"
    $ city_button01 = "Minister A" #601
    $ city_button02 = "Minister B" #602
    $ city_button03 = "Chief Retainer" #603
    $ city_button04 = "Sabasa Knight Leader" #604
    $ city_button05 = "Scholar" #605
    $ city_button06 = "Priest" #606
    $ city_button10 = "Audience Hall" #610
    $ city_button11 = "Princesses Room" #611
    $ city_button12 = "Soldier's Office" #612
    $ city_button19 = "Return to Town" #619

    while True:
        call cmd_city

        if result == 1 and ivent06 == 0:
            call city12b_01
        elif result == 1 and ivent06 == 1:
            call city13b_01
        elif result == 2 and ivent06 == 0:
            call city12b_02
        elif result == 2 and ivent06 == 1:
            call city13b_02
        elif result == 3 and ivent06 == 0:
            call city12b_03
        elif result == 3 and ivent06 == 1:
            call city13b_03
        elif result == 4 and ivent06 == 0:
            call city12b_04
        elif result == 4 and ivent06 == 1:
            call city13b_04
        elif result == 5 and ivent06 == 0:
            call city12b_05
        elif result == 5 and ivent06 == 1:
            call city13b_05
        elif result == 6 and ivent06 == 0:
            call city12b_06
        elif result == 6 and ivent06 == 1:
            call city13b_06
        elif result == 10 and ivent06 == 0:
            call city12b_10
        elif result == 10 and ivent06 == 1:
            call city13b_10
        elif result == 11 and ivent06 == 0:
            call city12b_11
        elif result == 11 and ivent06 == 1:
            call city13b_11
        elif result == 12 and ivent06 == 0:
            call city12b_12
        elif result == 12 and ivent06 == 1:
            call city13b_12
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            show bg 064 with blinds2
            play music "audio/bgm/sabasa.ogg"
            jump city13_main


label city13b_01:
    show mob daizin with dissolve #700

    minister_a "But why would a monster kidnap the Princess?{w}\nSince monsters are female, they wouldn't normally kidnap another female..."
    minister_a "Was it some sort of political purpose?{w}\nYou crossed swords with them, right?{w} What was their purpose?"
    l "Err... Well...{w}\nI was just desperate to rescue her and didn't really pay attention..."
    minister_a "I see... It must have been an intense fight.{w}\nAble to rescue the Princess while still a boy... You're amazing!"

    return


label city13b_02:
    show mob daizin with dissolve #700

    minister_b "Oh!{w} The Hero, Luka!{w}\nYou're now the man of the hour!"
    minister_b "There are going to be many who try to gain favor with you.{w}\nBe sure to be careful in recklessly accepting invitations."
    minister_b "Ah... By the way, would you like to have dinner with my family?{w}\nI would love to become close friends with you..."

    return


label city13b_03:
    show mob rouzin1 with dissolve #700

    chief_retainer "The King is above any when it comes to both literary and military arts.{w}\nBut most of all, he is an expert judge of character."
    chief_retainer "With just a glance, he can accurately gauge the caliber of someone."
    chief_retainer "Because of that, we don't expect the King to make mistakes.{w}\nWe have high hopes for the Princess, too!"
    l ".............."

    return


label city13b_04:
    show mob sensi2 with dissolve #700

    sabasa_knight_leader "It's good for the military to have free time.{w}\nIf they're always busy, it means the country is in disarray."
    sabasa_knight_leader "I'm proud that my forces have a lot of spare time.{w}\nOf course, I never neglect training and discipline."

    return


label city13b_05:
    show mob gakusya with dissolve #700

    scholar "Some old documents about the {i}Dragon Seal Trial{/i} were returned.{w}\nIt seems as though the Princess had them in her possession."
    scholar "I wonder why..."

    return


label city13b_06:
    show mob sinkan with dissolve #700

    priest "I'm glad the Princess has returned safely.{w}\nMy prayers must have reached Ilias."
    priest "Oh, I don't mean to downplay your actions though.{w}\nBut you must have arrived here due to my prayers."

    return


label city13b_10:
    scene bg 045 with blinds
    show sabasa st01 with dissolve #700

    sabasa_king "Oh! Luka!"
    sabasa_king "Please, take a rest in my castle.{w}\nIf you prefer, you can rest here permanently as Sara's husband."
    l "I already told you what I thought about that..."

    show alice st11b at xy(X=-120) #700
    show sabasa st01 at xy(X=160)
    with dissolve #701

    a "Hmph."

    show sabasa st02 at xy(X=160) with dissolve #701

    sabasa_king "Are you a companion of Luka's?{w}\nIt would be pretty troublesome if your relationship was too deep."
    sabasa_king "If you leave him to me, I can ens...{w}\n..............?"

    show sabasa st04 at xy(X=160) with dissolve #701

    sabasa_king "...Are you really human?"
    a "................."

    show sabasa st02 at xy(X=160) with dissolve #701

    sabasa_king "Sorry... That was rude of me.{w}\nWhether monster or human, I don't mind."
    sabasa_king "I was just trying to gauge your ability... But I wasn't able to.{w}\nIt seems I should ask you to be judging me."

    show alice st12b at xy(X=-120) with dissolve #700

    a "You're quite capable... For a human."
    sabasa_king "Haha... So you can see right through me, can you?{w}\nAt any rate, feel free to spend time in my country in peace."

    scene bg 065 with blinds
    return


label city13b_11:
    scene bg 067 with blinds
    show mob madam with dissolve #700

    chamberlain "You are Luka, are you not?{w}\nSara is training at the moment.{w}\nPlease wait while I call her..."
    l "Ah... No, that's fine.{w}\nI didn't really have anything to talk to her about."
    chamberlain "You didn't return to become her husband?"
    l "I'm not going to be her husband.{w}\nBesides, don't you think it's odd for her to be given away as a reward like that?"
    chamberlain "It's true that a father normally wouldn't give their daughter away as a reward."
    chamberlain "But a powerful Hero able to save the Princess from danger is a suitable husband.{w}\n...That's what the King thinks."
    l ".................."
    "That's a little embarrassing."

    scene bg 065 with blinds
    return


label city13b_12:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    soldier_a "The odd atmosphere seems to have gone away.{w}\nWhat do you think was the cause of that tension before?"
    soldier_b "Well, it's fine now that it's quiet again.{w}\nLet's just hope it continues forever."

    scene bg 065 with blinds
    return


label city14_main:
    call city_null
    $ city_bg = "bg 072"
    $ city_button01 = "Farmer" #601
    $ city_button02 = "Lady" #602
    $ city_button03 = "Woman" #603
    $ city_button04 = "Elder" #604
    $ city_button05 = "Snitch" #605
    $ city_button06 = "Boy" #606
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button19 = "Lord's Mansion" #619

    while True:
        if hanyo[0] == 0:
            $ city_act19 = 0
        elif hanyo[0] == 1:
            $ city_act19 = 1

        call cmd_city

        if result == 1:
            call city14_01
        elif result == 2:
            call city14_02
        elif result == 3:
            call city14_03
        elif result == 4:
            call city14_04
        elif result == 5:
            call city14_05
        elif result == 6:
            call city14_06
        elif result == 10:
            call city14_10
        elif result == 11:
            call city14_11
        elif result == 19:
            return


label city14_01:
    show mob ozisan3 with dissolve #700

    farmer "I saw it from a distance...{w}\nThe Lord, Lily, went into her mansion."
    farmer "Please save my daughter, Mary...{w}\nShe was caught at age seventeen, and hasn't returned in three years..."
    l "...I'll do my best."
    "Three years...{w} I don't know if she would still be alive..."

    show mob obasan with dissolve #700

    woman "Traveler... My daughter too...{w} Please save Erica..."

    show mob obasan at xy(X=-160) #700
    show mob seinen1 at xy(X=160) as mob2
    with dissolve #701

    youth "Celia, my wife, was caught as well!"
    "One after another, the villagers crowd around me shouting out names.{w}\nToo many have been taken..."
    l "...I understand!{w} I'll save everyone!"
    "...They may already be dead, though...{w}\nAnger swirls inside of me."

    $ hanyo[0] = 1
    return


label city14_02:
    show mob zyosei with dissolve #700

    lady "My younger sister studied magic in Sabasa Castle...{w}\nAs soon as she returned here, she was caught as a witch."
    lady "I want to run away from here.{w}\nBut I'm so poor, I can't migrate anywhere."

    return


label city14_03:
    show mob obasan with dissolve #700

    woman "Don't talk to me!{w}\nYou're just going to get caught by Lily!"
    woman "When that happens, everyone you talked to will be taken as well!{w}\nYou're nothing but a plague on this village!"
    l "............."

    return


label city14_04:
    show mob rouzin1 with dissolve #700

    elder "This village has always excluded mages and intellectuals.{w}\nLily became the Lord a long time ago..."
    elder "However, as soon as she become a Lord, the hunts became intense.{w}\nIt's now a horrible frenzy..."

    return


label city14_05:
    show mob seinen2 with dissolve #700

    snitch "Hehehe... I'm an {i}Information Monger{/i}.{w}\nSo far, I've supplied information on dozens of witches."
    snitch "If you oppose me, I'll supply them \"information\" on you too."
    snitch "We wouldn't want the soldiers to have to get involved, now would we?"
    l ".............."

    return


label city14_06:
    show mob syounen with dissolve #700

    boy ".............."
    boy "Mommy said I shouldn't talk to strangers."
    boy ".............."

    return


label city14_10:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "I have nothing to sell to you. Leave.{w}\nIf I get involved with travelers, I'll be labeled a witch."

    scene bg 072 with blinds
    return


label city14_11:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "Isn't this an amazing church for such a small village?{w}\nAll property of witches is handed over to the church."
    priest "Hehe... That's Ilias's wishes, after all.{w}\nWe must capture more witches in her name..."
    l "................"

    scene bg 072 with blinds
    return


label city15_main0:
    $ BG = "bg 020"
    play sound "audio/se/asioto2.ogg"
    show bg 072 with blinds2
    play music "audio/bgm/mura2.ogg"


label city15_main:
    call city_null
    $ city_bg = "bg 072"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Villager Girl" #602
    $ city_button03 = "Lady" #603
    $ city_button04 = "Woman" #604
    $ city_button05 = "Elder" #605
    $ city_button06 = "Snitch" #606
    $ city_button07 = "Boy" #607
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Lord's Mansion" #612
    $ city_button19 = "Leave Village" #619

    while True:
        call cmd_city

        if result == 1:
            call city15_01
        elif result == 2:
            call city15_02
        elif result == 3:
            call city15_03
        elif result == 4:
            call city15_04
        elif result == 5:
            call city15_05
        elif result == 6:
            call city15_06
        elif result == 7:
            call city15_07
        elif result == 10:
            call city15_10
        elif result == 11:
            call city15_11
        elif result == 12:
            call city15_12
        elif result == 19:
            return


label city15_01:
    show mob seinen1 with dissolve #700

    youth "Lily's soldiers all ran away.{w}\nThis village is finally free of their darkness..."
    youth "Even though I want to forget the past, we can't...{w}\nWe can't let it happen again."

    return


label city15_02:
    show wormv st11 with dissolve #700

    villager_girl "Thank you again, Hero.{w}\nThis village is peaceful now."
    villager_girl "My arm still has yet to recover...{w}\nI don't want to shock everyone, so I'm still hiding it."
    villager_girl "Once the prejudice vanishes from this village, I'll be able to live without hiding it."

    return


label city15_03:
    show mob zyosei with dissolve #700

    lady "We decided to imprison Lily for life.{w}\nIn a jail on the outskirts of the village, we will keep her locked up until she dies."
    lady "Many wanted to put her to death...{w}\nBut we decided not to kill her."
    lady "We don't want any more death in this village.{w}\nNo matter how worthless her life is."

    return


label city15_04:
    show mob obasan with dissolve #700

    woman "Thank you again!{w}\n...Eh?{w} What do you mean I called you a plague on the village?"
    woman "Hey... I was really supporting you before... I just couldn't say anything..."
    l "................"

    return


label city15_05:
    show mob rouzin1 with dissolve #700

    elder "Lily wasn't able to gain control and run wild alone.{w}\nThe prejudice and hate in this village enabled her to commit her atrocities."
    elder "Lily didn't have to force the villagers to participate in the witch hunts in the beginning, either."
    elder "We were all walking along the same road..."

    hide mob
    show alice st14b
    with dissolve #700

    a "The subjects under a tyrant's rule become tyrants themselves."

    return


label city15_06:
    show mob ozisan2 with dissolve #700

    farmer "...The snitch was killed.{w}\nThe day after Lily was stopped, his corpse was found in the middle of the street in pieces."
    farmer "It's said that he had over a hundred wounds on his body.{w}\nCrushing wounds, sword wounds, spear wounds... Everything."
    farmer "Many people were killed because of him.{w}\nIt must have been the retaliation of the families."
    l "................"
    "The witch hunt isn't fully over yet...{w}\nThe grudge and hatred is still festering..."

    return


label city15_07:
    show mob syounen with dissolve #700

    boy "My mommy said I can talk to strangers again.{w}\nWelcome to the Witch Hunt Village, traveler!"
    boy "...Witch Hunt Village is what we've been called for a while, anyway...{w}\nI don't know what the real name is..."
    "I still don't think you should talk to random strangers..."

    return


label city15_10:
    scene bg 151 with blinds
    show mob ozisan3 with dissolve #700

    shopkeeper "I'm very sorry about the other day!"
    shopkeeper "Look... I'm just a weak shopkeeper.{w}\nI had no choice but to act like that if I wanted to protect my family."
    shopkeeper "So from now on, I'll give you an eighty percent discount!"

    scene bg 072 with blinds
    return


label city15_11:
    scene bg 153 with blinds
    show mob sinkan with dissolve #700

    priest "Don't stare at me like that!{w}\nThe last priest was arrested after the witch hunts ended, and I took over as the new priest."
    l "Oh, I see...{w} I'm sorry about glaring at you like that."
    "His silhouette looks exactly the same... I couldn't tell."
    priest "At first I was going to destroy this giant church built on the money stolen from the deceased..."
    priest "But I think a better use would be to house all the orphans from the witch hunts."

    scene bg 072 with blinds
    return


label city15_12:
    scene bg 073 with blinds
    show mob heisi1 with dissolve #700

    soldier "This mansion is off-limits.{w}\nThere are still suspicious things inside that we are investigating."
    soldier "We don't know what kinds of traps or dangers are inside, so we're waiting for a specialist to come in."

    if ivent05 != 0 and check01 != 1:
        l "I see... That sounds serious."

        hide mob with dissolve

        l "...Oh?{w} That woman..."
        "A strange woman is standing in front of the mansion..."

        play music "audio/bgm/kiki2.ogg"
        show stein st01 with dissolve #700

        mysterious_woman "................."
        l "You were at the haunted mansion in Natalia..."
        mysterious_woman "................."

        play sound "audio/se/asioto2.ogg"
        hide stein with dissolve

        "The woman glances at me, then quickly walks away."
        l "Why is she here..."

        show alice st11b with dissolve #700

        a "What? Is she a friend of yours?"
        l "Oh yeah... You were still unconscious after fainting before.{w}\n...Is that woman a monster?"
        a "No... I barely felt any magic from her.{w}\nI doubt she's a monster."
        l "...A human, then?"
        "But why was she here?{w}\nThis can't be a coincidence... Just who is she?"

        play music "audio/bgm/mura2.ogg"
        $ check01 = 1

    scene bg 072 with blinds
    return


label city16_main:
    call city_null
    $ city_bg = "bg 081"
    $ city_button01 = "Old Man" #601
    $ city_button02 = "Youth" #602
    $ city_button03 = "City Girl" #603
    $ city_button04 = "Young Woman" #604
    $ city_button05 = "Noblewoman" #605
    $ city_button06 = "Soldier A" #606
    $ city_button07 = "Soldier B" #607
    $ city_button08 = "Harpy" #608
    $ city_button09 = "Slime Girl" #609
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Minotauros's Milk Shop" #612
    $ city_button13 = "Spider Girl's Sewing Shop" #613
    $ city_button14 = "Alraune's Florist Shop" #614
    $ city_button15 = "Church" #615
    $ city_button16 = "Information Monger" #616
    $ city_button17 = "Grand Noah Castle" #617
    $ city_button19 = "Inn" #619

    while True:
        if hanyo[3] == 0:
            $ city_act16 = 1
        elif hanyo[3] == 1:
            $ city_act16 = 0

        if hanyo[0] + hanyo[1] + hanyo[2] + hanyo[3] == 4:
            $ city_act19 = 1
        elif hanyo[0] + hanyo[1] + hanyo[2] + hanyo[3] < 4:
            $ city_act19 = 0

        call cmd_city

        if result == 1:
            call city16_01
        elif result == 2:
            call city16_02
        elif result == 3:
            call city16_03
        elif result == 4:
            call city16_04
        elif result == 5:
            call city16_05
        elif result == 6:
            call city16_06
        elif result == 7:
            call city16_07
        elif result == 8:
            call city16_08
        elif result == 9:
            call city16_09
        elif result == 10:
            call city16_10
        elif result == 11:
            call city16_11
        elif result == 12:
            call city16_12
        elif result == 13:
            call city16_13
        elif result == 14:
            call city16_14
        elif result == 15:
            call city16_15
        elif result == 16:
            call city16_16
        elif result == 17:
            call city16_17
        elif result == 19:
            return


label city16_01:
    show mob ozisan1 with dissolve #700

    old_man "The monsters downtown?{w}\nAs long as they aren't annoying, who cares."
    old_man "Well, there actually is one thing I wish would change.{w}\nA lot of monsters have weird cultural rituals that can get pretty annoying..."

    return


label city16_02:
    show mob seinen1 with dissolve #700

    youth "Do I mind the monsters in the city?{w}\nNo, not at all!"
    youth "This city is great!{w}\nI'm not leaving here until I die!"
    youth "There are always cute, hungry monster girls around.{w}\nI didn't like it at first, but now I..."
    youth "...Oops!{w} Forget that last part."

    return


label city16_03:
    show mob musume1 with dissolve #700

    city_girl "I'm looking forward to the bout tomorrow.{w}\nI need to get up early and save a seat at the Colosseum."
    l "You're interested in martial arts?"
    city_girl "No, I don't care about the fight.{w}\nI just... Hehehe..."
    city_girl "You're pretty cute, why don't you try your luck in the Colosseum?{w}\nI'd definitely go to see you..."

    return


label city16_04:
    show mob musume2 with dissolve #700

    young_woman "My boyfriend was raped by a group of monster girls.{w}\nNow he isn't satisfied with my body anymore..."
    young_woman "You damn monsters, I hate you!{w}\nI'm getting out of this cursed city!"

    return


label city16_05:
    show mob madam with dissolve #700

    noblewoman "I bought a special box seat for tomorrow's match.{w}\nHoo hoo... I'm looking forward to it."
    l "Oh, you're interested in the martial arts?"
    noblewoman "Haha!{w} Who goes to the Colosseum to see that pointless activity?{w}\nI'm after something much more entertaining."
    l ".........?"

    return


label city16_06:
    show mob sensi1 with dissolve #700

    soldier "To... To get into the Colosseum...{w}\nMoney... Need money..."
    soldier "Ahhh!{w} I want to fight in the Colosseum!"
    l "Ahh, how nice!{w} A powerful soldier, living his life by his sword.{w}\nThirsting for his next chance to test his mettle in a match of arms."
    l "Truly, a man that lives for honing his skills in the martial arts!"

    show alice st01b at xy(X=-120)
    show mob sensi1 at xy(X=160)
    with dissolve

    a "...Really?{w}\nI smell another kind of desire coming off him..."

    return


label city16_07:
    show mob sensi2 with dissolve #700

    soldier "Far off to the east of here is a village called Yamatai.{w}\nIt's a very quiet place, where you can live a life of leisure."
    soldier "I've heard that the village head was looking for a skilled adventurer to hire.{w}\nIt seems like a monster has been bothering them for a while."
    l "A troublesome monster... I can't stay silent when I hear about that!"

    hide mob
    show alice st02b
    with dissolve #700

    a "More importantly, they're known for their rich food culture.{w}\nWe must go there, at all costs."
    l ".............."
    "I don't care about satisfying her stomach, but I can't leave the matter of the monster alone."

    $ hanyo[2] = 1
    return


label city16_08:
    show mob hapy2 with dissolve #700

    fishing_loving_harpy "My fishing partner has gone missing.{w}\nHe said he was going to \"Taboo Spring\".{w} I told him not to..."
    l "Taboo Spring?"
    fishing_loving_harpy "You absolutely must not step foot in there.{w}\nAs soon as you do, divine punishment will be handed down to you."

    return


label city16_09:
    show mob mamono4 at xy(X=244, Y=80)
    with dissolve #700

    slime_girl "Purupurupuru...{w}\nWant to do something fun?"
    slime_girl "Come on, if you have free time, let's play! {image=note}{w}\nLet's purupurupuru together! {image=note}"

    return


label city16_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    weapon_shop_owner "Welcome!{w}\nOh, how rare.{w} A human customer!"
    l "Rare?"
    weapon_shop_owner "Yeah, I mainly deal in weapons for monsters.{w}\nBut I have some that humans can use, too."
    l "I see..."
    "As he said, there doesn't seem to be many things usable by a human here...{w}\nIt's not like I wanted to replace my weapon anyway."

    scene bg 081 with blinds
    return


label city16_11:
    scene bg 151 with blinds
    show mob mamono1 at xy(X=100, Y=-100) with dissolve #700

    elf "Welcome to the Elf Tool Shop!{w}\n...Oh? Is it really that strange for a monster to own a shop?"

    if ribbon == False:
        l "Y...Yeah..."
        elf "Ah, you must have come from the south.{w} That's why you're so surprised.{w}\nIt's true this might be pretty unusual down there."
        l "Yes... But I think this city is wonderful."
        elf "Yes, I love this city too.{w}\nIt's much better living together with humans than living in the hills and forests."
        elf "But I've heard that Grangold Castle has an even more integrated society.{w}\nThe stories go that they live in complete harmony there."
        l "Even more than here?"
        "Even more in harmony than here...?{w}\nI really want to see that!"
        "We'll be heading to the Gold Region when I go get Salamander...{w}\nI'm really looking forward to Grangold Castle!"

        hide mob
        show alice st01b
        with dissolve #700

        a "Hey, Luka...{w} I... Want that."
        "Alice gently pulls on my sleeve as she points to something on the wall."
        l "...Hmm?{w} I didn't think a tool shop sold food..."
        a "...It isn't food.{w} Here, look."
        "Alice is pointing to a display with accessories for monsters.{w}\nShe seems to be enthralled by a that goes on a tail."
        a "............."
        l "...You want it?"

        show alice st02b with dissolve #700

        a "............."
        "Like she was a small child, she gives a tiny silent nod.{w}\n...Sheesh, oh well."

        play sound "audio/se/kaimono.ogg"
        pause 2.0
        $ ribbon = 1
        show alice st09b with dissolve #700

        a ".........{image=note}"
        "With the now attached to her tail, she happily swings her tail around."
        "That was surprisingly expensive...{w}\nBut she looks so happy now, so whatever."

        $ hanyo[0] = 1

    scene bg 081 with blinds
    return


label city16_12:
    show mob mamono5 with dissolve #700

    minotauros_milkmonster "Hey, boy.{w} Want some certified Minotauros brand milk?{w} It will help turn that small frame of yours into one of a strong warrior!"
    l "N...No thank you... I'm fine..."
    "That milk... It... It couldn't be... No..."

    return


label city16_13:
    show mob mamono3 with dissolve #700

    spider_girl_shop_owner "Welcome!{w} I hope you don't want anything made..."
    l "No, not really..."
    spider_girl_shop_owner "Good, good."
    spider_girl_shop_owner "Because honestly... Though I call myself Spider Girl...{w} I'm actually a Tarantula Girl..."
    spider_girl_shop_owner "Honestly, I'm no good at sewing..."
    l ".............."
    "Then why do you own a sewing shop?"
    spider_girl_shop_owner "Maybe I should abandon everything and return back home..."
    l "So then you weren't born in this city?"
    spider_girl_shop_owner "No, I came from a woodland village in the north called Plansect Village.{w}\nIt's a village inhabited by various monsters."
    l "Ah, a monster village..."
    "I know similar monsters band together sometimes, but I haven't heard of a monster village filled with monsters of different races."

    hide mob
    show alice st01b
    with dissolve

    a "Hmm... Plansect Village, eh?"
    l "Have you heard of it, Alice?{w}\nI kind of want to see it myself..."
    "I want to visit a village that has monsters of varying races living together in harmony."
    "I might be able to learn something important to realize my dream."
    a "I wouldn't really recommend going there...{w}{nw}"

    show alice st04b
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nBut I wouldn't stop you if you really wanted to."
    l "..........?"
    "Somehow, it feels like she's trying to hide something...{w}\nIs there a problem she knows about...?"

    $ hanyo[1] = 1
    return


label city16_14:
    show mob mamono2 at xy(X=147) with dissolve #700

    florist_shop_owner "Welcome to Alraune's Florist Shop! {image=note}{w}\nMr. Traveler, would you like a flower?"

    if item14 != 1:
        l "Sure, I'll take one..."

        play sound "audio/se/fanfale.ogg"
        $ item14 = 1

        "{i}Alraune Flower{/i} obtained!"

        show alice st02b at xy(X=-110)
        show mob mamono2 at xy(X=303)
        with dissolve #701

        a "Alright, let's hurry up and see how it tastes."
        l "Oi, oi! Don't eat it!"
        florist_shop_owner "Here, I have a fresh one you can eat!{image=note}"
        l "Eh!? You really can eat it!?"

    return


label city16_15:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "Oh! What travesty!{w} Such corruption of public morals in this town!{w}\nI'm so sorry, Ilias!"
    l "Uhm... If there were monsters who were followers of Ilias, what would she do?"
    priest "Ilias hates monsters.{w} She would never accept them as followers."
    l "That's true....{w}\n.............."

    scene bg 081 with blinds
    return


label city16_16:
    show amira st21 with dissolve #700

    information_monger "Hehe, I have good information for you today..."
    l "Wait, wait. That outline...{w}\nYou're Amira, aren't you!?"

    stop music fadeout 1
    play music "audio/bgm/battlestart.ogg" noloop
    show amira st01 with Dissolve(1.5) #700

    "Unfortunate Lamia appears!"

    pause 0.3
    pause 4.0
    play music "audio/bgm/amira.ogg"

    amira "Your piercing gaze saw right through my clever ruse.{w} You truly are my Hero!"
    amira "I am Amira, an Unfortunate Lamia."
    l "You... Weren't you just in the jail at Sabasa?"
    amira "I was kicked out.{w}\nAlas, there is no rest for one such as I..."
    "Squatting in a jail just to get three meals a day... Of course you would be kicked out."
    l "So, do you have any good information?"
    amira "You asked about Undine and Salamander before, remember?{w}\nI gathered quite a bit on Undine."
    amira "In the forest west of here, there is a tiny spring called \"Undine's Spring\"."
    l "Undine's Spring...{w} So I guess it must mean exactly what it says."
    amira "The people of Noah also know it by another name, \"Taboo Spring\"."
    amira "The name \"Undine's Spring\" is seen only in ancient documents these days..."
    amira "The last eyewitness account is from an animist 500 years ago...{w}\nThere have been no recent sightings."
    l "I see, so it's another thing from the ancient past.{w}\nJust like with Gnome."
    amira "About that place, I have some other juicy tidbits...{w}\nAccording to rumors, it seems that there is a dungeon underneath it."
    l "A dungeon?{w} Under the spring?"
    amira "It's just gossip, so it can't be for sure.{w}\nBut that's what those who have gone to the spring and come back have said."
    amira "Also last month, a man fishing at the spring never came back."
    l "I see..."
    "I'm sure there's something there..."
    amira "That's about it for Undine.{w}\nNow all that remains is Salamander."
    amira "I'll have to head to the Gold Region to find anything out about her.{w}\nSo then, I shall meet you in the Gold Region!"
    l "Thank you, Amira.{w}\nYou're really helpful."
    l "So, what I owe you for the information so far..."
    amira "Money is not necessary.{w}\nIn exchange, build a world where a monster like me can live freely!"
    l "Amira..."
    amira "Even an ugly monster like me desires a world where I can loaf around, doing nothing all day!{w}\nThat's my only desire!"
    l "\"Loaf around\"?{w} That seems rather..."

    play sound "audio/se/escape.ogg"
    show amira st21 with dissolve #700

    amira "I shall resume my Unfortunate Lamia duties, and obtain information!"
    l "I don't think that has anything to do with gathering information..."
    amira "Till next time, in Grangold!"

    play sound "audio/se/escape.ogg"
    hide amira with dissolve

    "Shouting that final parting farewell, Amira runs away."

    play music "audio/bgm/city1.ogg"

    l "Amira has helped me a lot.{w}\nFor her sake too, I need to create a better world..."

    show alice st01b with dissolve #700

    a "That girl is already pretty much doing whatever she wants."

    $ hanyo[3] = 1
    return


label city16_17:
    scene bg 043 with blinds
    show mob heisi1 with dissolve #701

    guard "I'm sorry, but I cannot allow any unauthorized persons past here.{w}\nPlease turn back."
    l "I see."
    "Obviously there's no reason I could just walk into the castle.{w}\nI don't need anything there either, so I'll just turn back."

    scene bg 081 with blinds
    return


label city17_main0:
    $ BG = "bg 019"
    play sound "audio/se/asioto2.ogg"
    scene bg 081 with blinds2
    play music "audio/bgm/city1.ogg"


label city17_main:
    call city_null
    $ city_bg = "bg 081"
    $ city_button01 = "Old Man" #601
    $ city_button02 = "Youth" #602
    $ city_button03 = "City Girl" #603
    $ city_button04 = "Young Woman" #604
    $ city_button05 = "Noblewoman" #605
    $ city_button06 = "Soldier A" #606
    $ city_button07 = "Soldier B" #607
    $ city_button08 = "Harpy" #608
    $ city_button09 = "Slime Girl" #609
    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Minotauros's Milk Shop" #612
    $ city_button13 = "Spider Girl's Sewing Shop" #613
    $ city_button14 = "Alraune's Florist Shop" #614
    $ city_button15 = "Church" #615
    $ city_button16 = "Grand Noah Castle" #616
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city

        if result == 1:
            call city17_01
        elif result == 2:
            call city17_02
        elif result == 3:
            call city17_03
        elif result == 4:
            call city17_04
        elif result == 5:
            call city17_05
        elif result == 6:
            call city17_06
        elif result == 7:
            call city17_07
        elif result == 8:
            call city17_08
        elif result == 9:
            call city17_09
        elif result == 10:
            call city17_10
        elif result == 11:
            call city17_11
        elif result == 12:
            call city17_12
        elif result == 13:
            call city17_13
        elif result == 14:
            call city17_14
        elif result == 15:
            call city17_15
        elif result == 16:
            jump city17b_main0
        elif result == 19:
            return


label city17_01:
    show mob ozisan1 with dissolve #700

    old_man "It seems like that Queen's Cup was quite something.{w}\nI heard a human warrior actually beat that famous Kyuba."

    return


label city17_02:
    show mob seinen1 with dissolve #700

    youth "Ah... This is such a nice city...{w}\nI was walking down this street last night when a group of harpies raped me."
    youth "Where should I walk around tonight..."
    l "Uhm...{w} That's forbidden, you know..."
    youth "Should I do some bondage with that Lamia down that east alley...?{w}\nOh, oh... Or how about that Spider Girl's web in the west..."
    l "..........."

    return


label city17_03:
    show mob musume1 with dissolve #700

    city_girl "You were the boy that beat Kyuba...{w}\nYou two were moving so fast, it was incredible."
    city_girl "...That ending was so unfortunate for you.{w} I'm upset at it, too."
    city_girl "That's no way for a tournament to end."
    city_girl "I really wanted to watch you get played with..."

    return


label city17_04:
    show mob musume2 with dissolve #700

    young_woman "I got in a fight with that Elf who my boyfriend was cheating with...{w}\nOf course, I didn't stand a chance against her."
    young_woman "Ahh, this is so damn frustrating!{w}\nHow can I get rid of all this pent up anger at men...?"

    return


label city17_05:
    show mob madam with dissolve #700

    noblewoman "I've become a fan of yours.{w}\nI really wish to see that dashing figure of yours in the Colosseum once again."
    noblewoman "...I'd also love to see that dashing figure being raped by a monster."

    return


label city17_06:
    show mob sensi1 with dissolve #700

    soldier "I was raped by the Cerberus in the arena the other day...{w}\nI...It was incredible."
    soldier "She sucked me dry with all three of her heads...{w}\nI wonder who will rape me next time..."

    return


label city17_07:
    show mob sensi2 with dissolve #700

    soldier "I want to try going to the Colosseum again...{w}\nH...Hey!{w} I don't have any bad intentions!"
    soldier "I just thought I'd try getting raped by a monster one time!{w} I promise, that's all it was!"
    l "..............."

    return


label city17_08:
    show mob hapy2 with dissolve #700

    fishing_loving_harpy "I was attacked by a Mermaid last night...{w} She broke my fishing rod."
    fishing_loving_harpy "She said people who fish should all be killed without mercy."
    fishing_loving_harpy "Those Mermaids really are annoying..."
    l "............"
    "There really is a lot of variety inside the monster world..."

    return


label city17_09:
    show mob mamono4 at xy(X=244, Y=80) with dissolve #700

    slime_girl "Purupurupuru...{w}\nMy friend Kenta lost in the Colosseum."
    slime_girl "I wonder if I should try fighting there next time...?{w}\nThere are lots of healthy young men to play with there.{image=note}"

    return


label city17_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    weapon_shop_owner "I made a fortune on selling weapons for this tournament.{w}\nBy the way, could you sign this sword?"
    weapon_shop_owner "It's the first time I've seen a human win the tournament in decades.{w}\nI'd love to decorate the store with a signed sword from the victor to attract customers."

    scene bg 081 with blinds
    return


label city17_11:
    scene bg 151 with blinds
    show mob mamono1 at xy(X=100, Y=-100)
    with dissolve #700

    elf "Welcome!{w} Can I help you with something?"
    l "No, not really..."
    "I should gather some stuff to prepare for the next trip."
    l "By the way, do you not have any issues with Ilias Kreuz's terrorism?{w}\nI've seen what they've done a few times in the middle of my journey..."
    elf "Ah, there aren't any of them in this city.{w}\nA few years ago, there were a couple incidents...{w}\nBut they were all forced out of the city."
    l "Eh? Is that true?"
    elf "With this many monsters here, they couldn't do anything.{w}\nThe group as it is now is puny compared to how strong it used to be."
    l "Puny...?"
    elf "Before their leader died, it was really powerful.{w} Then it went to pieces.{w}\nNow all they can manage is some pathetic incidents of terrorism."
    elf "Most of their divisions had to all go back to their headquarters at Gold Port."
    l "Gold Port headquarters, hmm..."
    "Lazarus and my father formed Ilias Kreuz in that city after their dreams of defeating the Monster Lord were shattered..."
    "That port is where their failed voyage to Hellgondo set out from."
    "Once I get all the powers of the spirits, I'll need to visit that port if I want to make it to Hellgondo myself."

    scene bg 081 with blinds
    return


label city17_12:
    show mob mamono5 with dissolve #700

    minotauros_milkmonster "Boy, how about some Minotauros brand milk?{w}\nThe winner of this year's Queen's Cup got strong by drinking it, you know!"
    l "N...No thank you..."
    "I'd never drink that..."

    return


label city17_13:
    show mob mamono3 with dissolve #700

    spider_girl_shop_owner "The sign is a lie.{w} It's really Tarantula Girl's Sewing Shop."
    spider_girl_shop_owner "My silk is completely unfit for sewing."
    l "I think you should change careers..."

    return


label city17_14:
    show mob mamono2 at xy(X=147) with dissolve #700

    florist_shop_owner "Weeeelcomeee!{w} To Alraune's Flooorist shop!"
    florist_shop_owner "Sweet, delicious flowers!"

    return


label city17_15:
    scene bg 156 with blinds
    show mob sinkan with dissolve #700

    priest "The faith of the populace in this city is damaged.{w}\nNot only do they live with monsters, but they blatantly break the taboo in the streets!"
    priest "How can the Queen permit this to continue!?"

    scene bg 081 with blinds
    return


label city17b_main0:
    play sound "audio/se/asioto3.ogg"
    scene bg 065 with blinds2
    play music "audio/bgm/castle3.ogg"


label city17b_main:
    call city_null
    $ city_bg = "bg 065"
    $ city_button01 = "Minister A" #601
    $ city_button02 = "Minister B" #602
    $ city_button03 = "Minister C" #603
    $ city_button04 = "Guard A" #604
    $ city_button05 = "Guard B" #605
    $ city_button06 = "Guard Captain" #606
    $ city_button07 = "Dullahan" #607
    $ city_button10 = "Queen" #610
    $ city_button11 = "Guardroom" #611
    $ city_button19 = "Return to City" #619

    while True:
        call cmd_city

        if result == 1:
            call city17b_01
        elif result == 2:
            call city17b_02
        elif result == 3:
            call city17b_03
        elif result == 4:
            call city17b_04
        elif result == 5:
            call city17b_05
        elif result == 6:
            call city17b_06
        elif result == 7:
            call city17b_07
        elif result == 10:
            call city17b_10
        elif result == 11:
            call city17b_11
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            scene bg 081 with blinds2
            play music "audio/bgm/city1.ogg"
            jump city17_main


label city17b_01:
    show mob daizin with dissolve #700

    minister_a "As you can see, there is very little discrimination against monsters in this city.{w}\nDue to the Colosseum, the people became very used to the sight of monsters."
    minister_a "Though... The monsters do cause a lot of issues, every day.{w}\nBut as our customs are very different, it's unavoidable."

    return


label city17b_02:
    show mob daizin with dissolve #700

    minister_b "The Mermaids are lobbying for increased waterway size and flow.{w}\nThe Alraunes are planning a protest for more city parks.{w}\nCity planning is quite difficult here..."

    return


label city17b_03:
    show mob daizin with dissolve #700

    minister_c "A farmer was just sent to the hospital after accidentally mistaking a Mandragora for a radish."
    minister_c "Harvesting season is almost here, so we must be very diligent."

    return


label city17b_04:
    show mob heisi1 with dissolve #700

    guard_a "I wonder when the custom of raping the loser in the Colosseum started...{w}\nMan, I'm so jealous...{w}\nNo, no.{w} Don't feel bad for me.{w} It's ok."

    return


label city17b_05:
    show mob heisi1 with dissolve #700

    guard_b "I watched you win in the Colosseum.{w}\nAs for me...{w} I lost against the Dullahan in the first round..."

    return


label city17b_06:
    show mob sensi2 with dissolve #700

    guard_captain "If the monsters ever decided to go wild in the town, us human guards would be helpless.{w}\nBut should such a situation arise, we would enlist the aid of all the Colosseum fighters."
    guard_captain "They love this city too, so they would join us in defending it."

    return


label city17b_07:
    show dullahan st01 with dissolve #700

    dullahan "What's wrong?{w}\nEven though I'm a monster, I'm part of the Queen's Royal Guard."
    dullahan "After battling in the Colosseum, I advanced far...{w}\nI really love this city."

    return


label city17b_10:
    scene bg 045 with blinds
    show grandnoa st03 with dissolve #700

    grand_noah_queen "Ah!{w} If it isn't the Hero, Luka.{w}\nHow have you fared since last we met?"
    grand_noah_queen "You are the one who enabled my dream to be a reality.{w}\nIf I can do anything for you, please just say the word."

    scene bg 065 with blinds
    return


label city17b_11:
    scene bg 060 with blinds
    show mob heisi1 at xy(X=-160) #700
    show mob heisi1 at xy(X=160) as mob2
    with dissolve #701

    guard_a "There was a huge incident the other day when a Bee Girl tried to suck the nectar from an Alraune.{w}\nToday, we spent an hour trying to get a Slime Girl out from being clogged in a drain."
    guard_b "Tomorrow we have to repair that fountain that a Minotauros smashed on accident.{w}\nWe're always dealing with monster troubles..."

    scene bg 065 with blinds
    return


label city18_main:
    call city_null
    $ city_bg = "bg 088"

    $ city_button01 = "Elder" #601
    $ city_button02 = "Youth" #602
    $ city_button03 = "Village Girl" #603
    $ city_button04 = "Old Woman" #604
    $ city_button05 = "Old Man" #605
    $ city_button10 = "Cat Shrine" #610
    $ city_button11 = "Snake Shrine" #611
    $ city_button12 = "Fox Shrine" #612
    $ city_button13 = "Meeting Hall" #613
    $ city_button19 = "Wait for Night" #619

    while True:
        if hanyo[0] == 0:
            $ city_act12 = 1
        elif hanyo[0] == 1:
            $ city_act12 = 0

        call cmd_city

        if result == 1:
            call city18_01
        elif result == 2:
            call city18_02
        elif result == 3:
            call city18_03
        elif result == 4:
            call city18_04
        elif result == 5:
            call city18_05
        elif result == 10:
            call city18_10
        elif result == 11:
            call city18_11
        elif result == 12:
            call city18_12
        elif result == 13:
            call city18_13
        elif result == 19:
            return


label city18_01:
    show mob rouzin1 with dissolve #700

    elder "Sheesh, young'uns these days are pathetic."
    l "Yes, they really are..."
    elder "If I was fifty years younger, that meeting hall would have been filled with blood.{w}\nTrying to get what you want through words alone...{w} Absolutely pathetic."
    l "............"

    return


label city18_02:
    show mob seinen1 with dissolve #700

    youth "All the guys are arguing about who's going to get to be the sacrifice...{w}\nIt's a really depressing sight."
    l "Y...Yes, that's right!"
    youth "To think their aspirations are so low...{w} I plan to go much higher in life."
    youth "I've heard of a village to the north called \"Succubus Village\""
    youth "My dreams are to make it there...{w}\nI'll surpass all of them!{w} Hahahaha!"
    l "...It looks like nobody here cares about the prohibition."

    return


label city18_03:
    show mob musume1 with dissolve #700

    village_girl "Are monsters really that good...?{w}\nThis is making me angry..."

    return


label city18_04:
    show mob rouba with dissolve #700

    old_woman "There are elves around this village, driving off all outsiders.{w}\nIt keeps out the riff-raff...{w} But it's also driving away merchants and good visitors, too..."
    l "Why are they doing that?"
    old_woman "Our village and the nearby elf village have had a marriage union since long ago.{w}\nMany men are adopted into their village, and marry into the elf village."
    old_woman "In return, the warrior elves defend the village from any invaders."

    return


label city18_05:
    show mob ozisan3 with dissolve #700

    old_man "There are a lot of shrines in this village.{w}\nI don't know if the gods will grace you with their presence, but you should try going and praying."
    l "Err...{w} I'm a follower of Ilias..."
    old_man "You people from the west only believe in one thing...{w}\nWe're grateful of everything, so we worship everything."

    return


label city18_10:
    scene bg 159 with blinds
    show nekomata st01 at xy(Y=50) with dissolve #700

    nekomata "...Nya?"
    l "H...Hello..."

    show nekomata st04 at xy(Y=50) with dissolve #700

    nekomata "...Nyaa!{image=note}"
    "She seems awfully friendly toward humans.{w}\nI think this Nekomata is even the resident of this shrine..."
    l "Is this also... Humans and monsters coexisting?"

    hide nekomata
    show alice st01b
    with dissolve #700

    a "Hmm, that's one method, isn't it...{w}\nEnshrining monsters and worshipping them...{w} Quite an interesting culture they have here."

    scene bg 088 with blinds
    return


label city18_11:
    scene bg 159 with blinds
    show sirohebi st11 with dissolve #700

    l "Wah, monsters here too..."
    white_snake_girl "Oh my, I am Shirohebi."
    white_snake_girl "Watch your mouth, impudent youngster."
    l "S...Sorry...{w}\nThe monsters that live in this village are quite friendly, aren't they...?"
    shirohebi "Only friendly people can approach this village, so only friendly people live here.{w}\nBut long ago, the humans turned violent and forced the monsters out."
    l "What did they do then?"
    shirohebi "They left the village, and lived in the mountains.{w}\nTravelers were attacked, and crimes were committed..."
    shirohebi "My elder sister was part of that group, too...{w}\nHow nostalgic..."
    l "............"
    "There's a lot I want to say about that...{w}\nBut for now, I'll deal with the current issue."

    scene bg 088 with blinds
    return


label city18_12:
    scene bg 159 with blinds
    play music "audio/bgm/tamamo.ogg"
    show tamamo st04 with dissolve #700

    t "All I have to do is stay here, and they give me thin fried tofu.{image=note}"
    l "............."
    "Tamamo sits on the steps of the shrine, happily munching away at fried tofu.{w}\nOne of the Heavenly Knights feared around the world...{w} I wonder what people would think if they could see this sight...?"

    show alice st01b at xy(X=-110) #700
    show tamamo st01 at xy(X=160)
    with dissolve #701

    a "Kitsune..."
    "Alice grabs hold of Tamamo's tail, and starts to pull on it.{w}\nShe really hates Kitsunes, doesn't she..."
    t "Hey!{w} Monster Lord!{w}\nDon't bully Kitsunes, you idiot!"

    show alice st04b at xy(X=-110) with dissolve #700

    a "............"

    play sound "audio/se/escape.ogg"
    "Alice grimaces under Tamamo's cold gaze, then runs away."

    hide alice
    show tamamo st01 at center
    with dissolve #700

    t "Sheesh...{w} No matter how much time has passed, children are always children."
    l "Why does Alice hate Kitsunes...?"
    t "You may not have known this, but I was in charge of the Monster Lord's training.{w}\nDue to that, she lived along with my clan."
    t "Since a young age, she was surrounded by Kitsunes, who were constantly nagging and picking on her.{w} I guess now she wants to bully them in revenge."
    l "I see...{w}\nThat seems like a really childish reason..."
    t "...My method of upbringing was bad.{w}\nI had to ensure she didn't end up detesting humanity."
    l "Eh...?"
    t "One wrong move, and she would turn into an existence that lives only for revenge, filled with hatred of man."
    t "Were that to happen, the disaster would be even worse than Alice the 8th, 500 years ago...{w}\nDue to that, she was not allowed out of the castle before she was raised..."
    t "But as a side effect, she is quite ignorant of the ways of the world..."
    l "Did something happen to Alice?{w}\nWhat would make her hate humans so much...?"

    show tamamo st03 with dissolve #700

    t "When she was a child, something happened that broke her young heart..."
    "After mumbling that, Tamamo's face grows dark."

    show tamamo st01 with dissolve #700

    t "...But that's all in the past.{w}\nThere's no need to return to old events."
    t "By the way, there's something I want to inquire about...{w}\nThat ring... Can I take a closer look at it?"
    l "It's a memento from my mother...{w} I don't want to take it off my finger, but...{w}\nHere, is this good?"
    "I hold out my hand to Tamamo.{w}\nShe gazes at the ring from all sides, then starts to sniff it."
    l "Alice said she couldn't feel anything special from it.{w}\nWhy are you both interested in this ring...?"
    t "Indeed, there's no magic coming from it...{w}\nBut the Monster Lord is still immature."
    t "The fact that there's no magic coming from this doesn't mean it isn't unusual.{w} It's quite the opposite..."
    l "\"The opposite\"...?{w} What do you mean?"
    t "The ore that this ring is made out of naturally holds a small amount of power in it...{w}\nNo matter how many thousands of years, that small power should remain."
    t "But not a thing can be felt from this ring...{w}\nThat's why it's unusual."
    l "So basically...{w} What? I have no idea."

    show tamamo st04 with dissolve #700

    t "Basically...{w} I have no idea, either.{image=note}"

    show tamamo st01 with dissolve #700

    t "If we study that ring with various techniques, we may learn something...{w}\nIt would take a few months if you wanted to try."
    l "I see...{w}\nIf it would take all that time, I'd rather not do it."
    t "In addition... She missed one other oddity.{w}\nWhen you fought Nanabi and fell unconscious, that unusual power in you was awakened."
    t "With it, you were able to dodge that powerful seven moons attack...{w}\nEven more, you managed to seal her."
    t "But the final attack you used was not a sword move with Angel Halo.{w} Yet still, she was sealed."
    l "I don't remember much...{w}\nThat was strange..."
    t "Are you just concluding that now, you idiot?{w}\nIn other words, the source of your power must be the same as the Angel Halo's."
    t "For some reason, a latent holy power resides in you."
    l "Wh...What do you mean...?"
    t "You're a rare species, boy.{w}\nNow, how about I give your body an extra careful examination...?"
    l "N...No thank you..."
    "A rare species?{w} I don't like being treated like some kind of spectacle..."
    t "Anyway...{w} Don't use that power ever again."
    l "Eh...?{w} Why...?"
    t "That much power is too much for a human body to bear.{w}\nSuch high powers of raw holy power will eat into your flesh."
    l "Eat into my flesh...?{w} Is it a disease?"
    t "As you use it, your human body will slowly be eaten away, and replaced by raw holy power.{w}\nIn the end, you'll cease being human and turn into something else."
    t "I've seen men consumed by holy power in the past.{w}\nThey continued to pay the price for pursuing that power, and eventually turned into a being made of pure holy energy..."
    t "Destroyed by that double-edged power, they became a new existence..."
    "Tamamo glances at my waist after that last statement.{w}\nI think she's looking at the Angel Halo..."
    t "You don't want to go down that road.{w}\nSo keep in mind that wielding that power is dangerous."
    l "...Thanks for the advice.{w}\nI don't really get it, but if it's that dangerous, I'll avoid using it."
    "I had already decided I wanted to fight with power I earned myself, anyway.{w}\nIf using that could destroy my body, all the more reason to avoid it."
    t "Please continue to take care of the Monster Lord.{w}\nShe's still young, and is quite quick to anger..."
    l "Ah... Yeah..."
    "Really... One of the Heavenly Knights asking a Hero to look after the Monster Lord...{w}\nBut I can't do anything but nod obediently after her earnest request."

    $ hanyo[0] = 1
    play music "audio/bgm/yamatai.ogg"
    scene bg 088 with blinds
    return


label city18_13:
    scene bg 089 with blinds
    show mob seinen1 at xy(X=-240) with dissolve #700

    young_man_a "I'm the sacrifice! {w} I won't allow anyone else to take the fall!"

    show mob seinen1 at xy(X=-80) as mob2 with dissolve #701

    young_man_b "No, no!{w} Nobody is more qualified than I to be the sacrifice!"

    show mob seinen1 at xy(X=80) as mob3 with dissolve #702

    young_man_c "I've skipped working for years!{w}\nDue to my laziness, I must be the one to be sacrificed!"

    show mob seinen1 at xy(X=240) as mob4 with dissolve #703

    young_man_d "Leave it to me! I'll protect everyone by doing it!"

    hide mob2
    hide mob3
    hide mob4
    show mob rouzin1 at center
    with dissolve #700

    village_chief "As you can see, they're still arguing.{w}\nYou're going to be the sacrifice...{w} But how do I tell them...?"
    village_chief "Now then...{w} How am I going to do this...?"
    l "............"
    "Everything about this scene seems pathetic."

    scene bg 088 with blinds
    return


label city19_main0:
    $ BG = "bg 086"
    play sound "audio/se/asioto2.ogg"
    scene bg 088 with blinds2
    play music "audio/bgm/yamatai.ogg"


label city19_main:
    call city_null
    $ city_bg = "bg 088"
    $ city_button01 = "Elder" #601
    $ city_button02 = "Youth" #602
    $ city_button03 = "Village Girl" #603
    $ city_button04 = "Old Woman" #604
    $ city_button05 = "Old Man" #605

    if ivent01 == 1:
        $ city_button06 = "Goblin Girl" #606

    $ city_button10 = "Cat Shrine" #610
    $ city_button11 = "Shirohebi's Shrine" #611
    $ city_button12 = "Fox Shrine" #612
    $ city_button13 = "Meeting Hall" #613
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city

        if result == 1:
            call city19_01
        elif result == 2:
            call city19_02
        elif result == 3:
            call city19_03
        elif result == 4:
            call city19_04
        elif result == 5:
            call city19_05
        elif result == 6:
            call city19_06
        elif result == 10:
            call city19_10
        elif result == 11:
            call city19_11
        elif result == 12:
            call city19_12
        elif result == 13:
            call city19_13
        elif result == 19:
            return


label city19_01:
    show mob rouzin1 with dissolve #700

    elder "When I was younger, I had an affair with an Ogre Girl...{w}\nAfter my wife found out, she looked even scarier than the Ogre Girl did..."
    l "............."

    return


label city19_02:
    show mob seinen1 with dissolve #700

    youth "After we learned that Yamata no Orochi was sealed, all of the boys became depressed.{w}\nI tried to convince them that there's a wider world out there, and they seemed to perk up."

    return


label city19_03:
    show mob musume1 with dissolve #700

    village_girl "I won't lose to some seductive monster whore!{w}\nI think I'll go to the Elf Village and see if I can train to become a kunoichi..."

    return


label city19_04:
    show mob rouba with dissolve #700

    old_woman "A long time ago, the men of this village adhered to a strict code of \"Bushido\".{w}\nBut now the only ones who follow it are the female elves of the nearby village."
    old_woman "It's quite strange..."

    return


label city19_05:
    show mob ozisan3 with dissolve #700

    old_man "I was thinking of building a shrine for that \"Ill-ass\" Goddess you westerners believe in.{w} What do you think?"
    l "I think she would get angry at that..."
    old_man "Sheesh, you people and your vengeful gods."

    return


label city19_06:
    show gob st23 at xy(Y=70) with dissolve #700

    goblin_girl "Out of the way, out of the way!.{w} This package is heavy! {image=note}{w}\n...Ah! Mr. Hero guy from before!"
    l "Eh...?{w} Why are you here in this village?"
    goblin_girl "I took a job in shipping.{w}\nI'm delivering this package from Iliasburg to Yamatai!"
    l "I see... You're working awfully hard, aren't you?{w}\nBut to think we would meet again here..."
    l "How did you get up here as fast as I did?{w}\nI thought I was hurrying along pretty fast, too..."
    goblin_girl "I've already made five trips between here and Iliasburg!"
    l "Uwa!?{w} Wh...What kind of legs do you have!?"
    goblin_girl "I'm descended from a high-class Goblin who had the \"Idaten\" something ability.{w}\nSo I'm much better than just some normal Goblin!"
    l "I...I see..."
    "It looks like she's really working hard.{w}\nThat makes me really happy for some reason..."

    return


label city19_10:
    scene bg 159 with blinds
    show nekomata st04 at xy(Y=50)
    with dissolve #700

    nekomata "...Unya. {image=note}"
    "The Nekomata that lives here in the shrine seems to be in good spirits."

    scene bg 088 with blinds
    return


label city19_11:
    scene bg 159 with blinds
    show sirohebi st11 with dissolve #700

    shirohebi "The sealed Yamata no Orochi is being kept in this shrine."
    shirohebi "My friends and I put her in a small, clear container.{w} Complete with a rock and a couple sticks and leaves to simulate her old cave."
    shirohebi "We'll keep her in there so she can reflect on her bad deeds."
    shirohebi "...Well, she was quite greedy, but her crimes weren't too serious.{w}\nI'm sure she'll be released before long."
    l "I hope she really does reflect on her bad deeds..."

    scene bg 088 with blinds
    return


label city19_12:
    scene bg 159 with blinds
    show youko st01 with dissolve #700

    kitsune "Ah, it's you!{w}\nThe Hero from before!"
    l "The kitsune from that cave...?{w} What are you doing here?"
    kitsune "I'm here to get some fried tofu."
    l "............"
    "As long as she isn't doing anything bad, I guess it's fine..."

    show alice st01b at xy(X=-110) #700
    show youko st03 at xy(X=160)
    with dissolve #701

    a "Kitsune..."
    "Alice starts to pull on her tails and ears."
    kitsune "Wahwahwah!{w} Stop it!"
    l "Oi, oi... Don't bully the Kitsunes..."
    "Alice yanks the fried tofu from the Kitsune's hand and shoves it into her mouth."
    a "*Munch*{w} *Munch*{w}{nw}"

    show alice st02b at xy(X=-110)
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nDelicious."
    kitsune "Nooo!{w} My fried tofu!"
    "She really hates kitsunes..."
    l "Do you dislike Tamamo too?"

    show alice st01b at xy(X=-110) with dissolve #700

    a "That girl is always treating me like a child..."
    "...From her tone, it doesn't seem like she really dislikes Tamamo.{w}\nI guess she's in that rebellious age where she just doesn't like any parental figures."
    kitsune "My fried tofu..."

    scene bg 088 with blinds
    return


label city19_13:
    scene bg 089 with blinds
    show mob seinen1 at xy(X=-240) with dissolve #700

    young_man_a "I'm on a journey to the south.{w}\nMy goal is to have fun with a harpy in the Harpy Village."

    show mob seinen1 at xy(X=-80) as mob2 with dissolve #701

    young_man_b "I'm on a journey to Port Natalia.{w}\nMy goal is to make out with a cute Mermaid."

    show mob seinen1 at xy(X=80) as mob3 with dissolve #702

    young_man_c "I'm on a journey to the Forest of Spirits.{w}\nMy goal is to be played with by the elves and fairies..."

    show mob seinen1 at xy(X=240) as mob4 with dissolve #703

    young_man_d "I'm going to the Succubus Village.{w}\nMy goal is what every man dreams of..."
    l "............"

    hide mob2
    hide mob3
    hide mob4
    show mob rouzin1 at center
    with dissolve #700

    village_chief "As you can see, they have shifted their attention to the world at large.{w}\nOur young men will take flight and spread awareness of Yamatai Village to the world at large.{w} And crush us in shame."
    l "It looks like the difficult times aren't over yet..."

    scene bg 088 with blinds
    return


label city20_main0:
    $ BG = "bg 095"
    play sound "audio/se/asioto2.ogg"
    scene bg 095 with blinds2
    play music "audio/bgm/plansect.ogg"


label city20_main:
    call city_null
    $ city_bg = "bg 095"
    $ city_button01 = "Alraune" #601
    $ city_button02 = "Dryad" #602
    $ city_button03 = "Alra Arum" #603
    $ city_button04 = "Alra Rooty" #604
    $ city_button05 = "Alra Mash" #605
    $ city_button06 = "Alra Priestess" #606
    $ city_button10 = "Tarantula Girl" #610
    $ city_button11 = "Moth Girl" #611
    $ city_button12 = "Mosquito Girl" #612
    $ city_button13 = "Caterpillar Girl" #613
    $ city_button14 = "Silkworm Girl" #614
    $ city_button15 = "Hornet Girl" #615
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city

        if result == 1:
            call city20_01
        elif result == 2:
            call city20_02
        elif result == 3:
            call city20_03
        elif result == 4:
            call city20_04
        elif result == 5:
            call city20_05
        elif result == 6:
            call city20_06
        elif result == 10:
            call city20_10
        elif result == 11:
            call city20_11
        elif result == 12:
            call city20_12
        elif result == 13:
            call city20_13
        elif result == 14:
            call city20_14
        elif result == 15:
            call city20_15
        elif result == 19:
            return


label city20_01:
    show mob mamono2 with dissolve #700

    alraune "This is the peaceful village of Plansect.{w}\nAll humans and monsters are welcome."

    return


label city20_02:
    show mob mamono7 with dissolve #700

    dryad "Monsters fighting each other is truly horrible.{w}\nI'm really happy peace has finally returned."

    return


label city20_03:
    show a_alm st01 at xy(X=200) with dissolve #700

    alra_arum "Those sealed are only released after acknowledging the war is over.{w}\nThat is, only those who take a vow of peace are unsealed."
    alra_arum "There are a few who are still emotional and are sealed...{w}\nWe have to be careful with those stupid enough to repeat the war."

    return


label city20_04:
    show a_looty st01 at xy(X=220, Y=130) with dissolve #700

    alra_rooty "I decided I won't ever absorb an insect's juices again.{w}\nInstead, I'll suck on human semen."
    alra_rooty "So hey, you!{w} Let me suck out some semen!{w}\nCome on, just a little!"
    l "Ah... Sorry..."

    return


label city20_05:
    show a_mash st01 at xy(X=260, Y=70) with dissolve #700

    alra_mash "The Canaan Sisters are still sealed...{w}\nThey reject the peace, so they'll stay sealed..."

    return


label city20_06:
    show a_emp st01 with dissolve #700

    alra_priestess "You saw our village in quite an embarrassing state.{w}\nWe were overly emotional, and acted too brashly."
    alra_priestess "After experiencing this peace, we're finally able to understand our errors."
    l "I have to apologize too.{w}\nI went into a rage and acted without thinking."
    alra_priestess "You have no reason to apologize.{w}\nThose sealed by you were all belligerent and violent."
    alra_priestess "It was divine retribution, so to speak.{w}\nAt any rate, we must never repeat the mistakes of the past."

    if item12 != 1 or item15 != 1:
        return

    alra_priestess "...Oh?{w} It seems as though you have something with some magic power there."
    "Alra Priestess closely inspects my Fairy Acorn and Nekomata's Bell."
    alra_priestess "Can I combine my magic with those magic items?{w}\nI'm sure I can create a beautiful jewel."
    l "But I got this as a proof of friendship from a fairy...{w}\nI can't just use it as a material..."
    alra_priestess "It's fine, I'm sure the completed jewel will feel even more like a proof of friendship."
    l "Then, OK..."
    "I hand her the two items."
    alra_priestess "Allow me to extract the radiance of friendship in these items..."
    "Magic power radiates around Alra Priestess...{w}{nw}"

    play sound "audio/se/item.ogg"
    with flash

    extend "{w=.7}\nShe forces the two items together, creating a beautiful jewel!"

    play sound "audio/se/fanfale.ogg"
    $ item12 = 2
    $ item15 = 0

    "\"Jewel of Friendship\" obtained!"
    l "Wh...What is this?"
    alra_priestess "When you grab that jewel, the deep feelings of the givers will resonate within you."
    l "Like this...?"
    "I clutch the Jewel, and hold it close to my chest.{w}\nInside my mind, a strange vision appears before me."

    scene bg 061
    show fairys_a st01 at xy(X=220, Y=100) #700
    show fairys_b st11 at xy(Y=50) #701
    show fairys_c st21 at xy(X=440, Y=50) #702
    with Dissolve(1.5)

    fairy "Wah, big bro!{image=note}{w}\nCan you see me all the way over there?"

    scene bg 159
    show nekomata st03 #700
    with Dissolve(1.5)

    nekomata "Unya!?"

    show nekomata st04 with dissolve #700

    nekomata "...Nyaa. {image=note}"

    scene bg 095
    show a_emp st01 #700
    with Dissolve(1.5)

    l "Amazing... I can see them!"
    alra_priestess "That jewel lets you communicate with them.{w}\nIt's the proof of the friendships you've formed with the monsters you've come across."

    return


label city20_10:
    show mob mamono3 with dissolve #700

    tarantula_girl "Hating the war, we've been hiding in the forest for ten long years...{w}\nDuring the war, we were treated as traitors..."

    return


label city20_11:
    show moss st01 at xy(X=230, Y=20) with dissolve #700

    moth_girl "Ara, it's you...{w}\nYou sealed us before we could even say anything."
    moth_girl "But I don't have a grudge against you.{w}\nYou sealed those Canaan Sisters after all."
    moth_girl "I hope they stay sealed forever."

    return


label city20_12:
    show mosquito st01 at xy(X=220, Y=70) with dissolve #700

    mosquito_girl "Well... Truly peace is best.{w}\nBut those plant monsters annoy me sometimes..."
    mosquito_girl "But I know, I won't make waves."

    return


label city20_13:
    show imomusi st01 at xy(X=180, Y=130) with dissolve #700

    caterpillar_girl "Ah!{w} The human from before!{w}\nThank you for helping me!{image=note}"

    return


label city20_14:
    show kaiko st01 with dissolve #700

    silkworm_girl "After being sealed, I now know what it's like to be wrapped up and helpless.{w}\nI'll stop doing that..."

    return


label city20_15:
    show suzumebati st01 with dissolve #700

    hornet_girl "QUEEN STILL SEALED.{w}\nSTILL NOT ACCEPTING.{w}\nEVERYONE WILL PERSUADE."

    return


label city21_main0:
    $ BG = "bg 041"
    play sound "audio/se/asioto2.ogg"
    scene bg 100 with blinds2
    play music "audio/bgm/city1.ogg"


label city21_main:
    call city_null
    $ city_bg = "bg 100"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Woman" #602
    $ city_button03 = "Old Man" #603
    $ city_button04 = "Old Woman" #604
    $ city_button05 = "Ant Girl" #605
    $ city_button06 = "Mud Golem Girl" #606
    $ city_button07 = "Information Seller" #607
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Magic Laboratory" #612
    $ city_button13 = "Grangold Castle" #613
    $ city_button19 = "Inn" #619

    while True:
        if hanyo[1] == 0:
            $ city_act07 = 1
        elif hanyo[1] == 1:
            $ city_act07 = 0

        if hanyo[0] + hanyo[1] + hanyo[2] + hanyo[3] == 4:
            $ city_act19 = 1
        elif hanyo[0] + hanyo[1] + hanyo[2] + hanyo[3] < 4:
            $ city_act19 = 0

        call cmd_city

        if result == 1:
            call city21_01
        elif result == 2:
            call city21_02
        elif result == 3:
            call city21_03
        elif result == 4:
            call city21_04
        elif result == 5:
            call city21_05
        elif result == 6:
            call city21_06
        elif result == 7:
            call city21_07
        elif result == 10:
            call city21_10
        elif result == 11:
            call city21_11
        elif result == 12:
            call city21_12
        elif result == 13:
            jump city21b_main0
        elif result == 19:
            return


label city21_01:
    show mob seinen1 with dissolve #700

    youth "The Ant Girls do all the work.{w}\nMan, I love this town."
    youth "Thanks to them, I get to screw around all day."

    return


label city21_02:
    show mob musume2 with dissolve #700

    woman "Those Ant Girls do all the cooking and cleaning, so I get to take it easy all day."
    woman "My wife duties only include having children.{w}\nOnce they're born, the Ant Girls raise them for me."

    return


label city21_03:
    show mob ozisan2 with dissolve #700

    old_man "Even though there are a lot of monsters here, Ilias Kreuz can't do anything."
    old_man "Outside is the Golem Girl, and inside are the Ant Girls.{w}\nThey wouldn't dare commit terrorism here."
    l "But people who hate monsters would hate this town, too..."
    old_man "There are no people here who hate monsters.{w}\nAll of them moved to the north, to Gold Port."
    old_man "Thanks to that, it became the central place for those who hate monsters, though."
    l "Gold Port, eh..."
    "The northernmost port on this continent.{w}\nIt's also the headquarters of Ilias Kreuz..."
    old_man "There are a bunch of thugs and bullies there, in addition to the monster haters.{w}\nGood folk would do well to stay away from there."
    l "Yes... Thank you for the warning."
    "But I have to go there.{w}\nShips left from that port to the northernmost continent."
    "They aren't going there any longer, but I have to find a ship somehow..."

    return


label city21_04:
    show mob rouba with dissolve #700

    old_woman "A little bit west of here is the {i}Succubus Village{/i}.{w}\nI came from there."
    l "Ehh!?{w}\nThen... You're a Succubus!?"
    old_woman "Fool! I'm a human!"
    l "...Ah... Of course..."
    "I grab my chest and breathe a sigh of relief.{w}\nIf this old woman was a Succubus, I think people's sense of romance would break."
    old_woman "It's often misunderstood, but the {i}Succubus Village{/i} doesn't have any Succubi living there.{w}\nAll of the residents are normal humans."
    l "Then why do they call it the {i}Succubus Village{/i}?"
    old_woman "That's because... Every 100 years, a group of Succubi attack the village.{w}\nI don't know why, but they're pretty punctual with their attacks."
    l "Every 100 years Succubi attack the village?{w}\nWhy do they do such a strange thing...?"

    show alice st11b at xy(X=-93) #700
    show mob rouba at xy(X=160)
    with dissolve #701

    a "...It's probably due to the influence of the purple moon.{w}\nIt's on a 100 year cycle."
    a "When it's a full moon, erotic type monsters have powerful magic and fertility.{w}\nThat night is called the Purple Sabbath... That is, it's the best breeding time for Succubi."
    a "They come together as a group and begin their... activities."
    old_woman "My word! Young lady, you are quite knowledgeable!{w}\nEven someone who lived in the village like me didn't know that!"
    l "Don't tell me that Purple Sabbath is soon..."
    old_woman "That's correct. It's one week from now.{w}\nThat's why I've come here to seek shelter.{w}\nI'm worried for those that are still in the village..."
    l "Won't the remaining villagers all be attacked by the Succubi!?{w}\nIf that's the case, I should go...!"

    show alice st12b at xy(X=-93) with dissolve #700

    a "Hmph. Are you going there so you can be attacked by the Succubi?"
    l "No! I'm going to defeat them!"

    $ hanyo[0] = 1
    return


label city21_05:
    show ant st01 with dissolve #700

    ant_girl "I AM WORKING. TALK LATER."
    "The Ant Girls look like they're awfully busy..."

    return


label city21_06:
    show madgolem st01 with dissolve #700

    mud_golem_girl "CURRENTLY WORKING.{w}\nIT'S DANGEROUS. STEP BACK."
    "The huge Mud Golem uses her massive size to carry large earthworks around..."

    return


label city21_07:
    l "\"Information Seller\"?{w} Could it be..."
    q "Of course, it is I..."

    stop music fadeout 1
    play music "audio/bgm/battlestart.ogg" noloop
    show amira st01 with Dissolve(1.5) #700

    "Unfortunate Lamia appears!"

    pause 0.3
    pause 4.0
    play music "audio/bgm/amira.ogg"

    amira "The reason I appeared before you?{w}\nWhy it's love of course!"
    amira "Love!{image=note}{w}\n*Censored* love! {image=note}{w}\nMy hot, steamy *Censored* love song! {image=note}"
    l "...I don't want to know what was censored in that \"song\"."
    amira "I'd take a bow like a singer should, but it would be quite difficult for me, as you can see."
    l "Then don't sing in the first place...{w}\nAnyway, I'd like to hear any information you managed to find."
    amira "My information is free of charge to you.{w}\nYou wanted information about Salamander, right?"
    amira "I asked the nearby Medusa, and learned quite a lot.{w}\nSalamander appears to reside in the southwest \"Fire Spirit's Volcano\"."
    l "A volcano, eh...?"
    "Fire Spirit's Volcano...{w} Not a very creative name for the place where Salamander is."
    "While a volcano is a fitting place for her, it sounds like it will be a pretty difficult place to get into..."
    amira "That appears to be the place, but there have been no human eyewitnesses.{w}\nBut among monsters, it appears to be a famous place to meet Salamander."
    amira "Thanks to that, getting the information was quite easy."
    l "Famous?{w} I wonder why..."
    "The other spirits seemed to live quietly, almost in solitude.{w}\nI wonder why Salamander's location is famous among monsters?"
    l "Anyway... Thank you, Amira.{w}\nThat's very useful information."
    amira "What do you want me to look into next?"
    l "Ah, well... Nothing in particular, really..."

    show alice st11b at xy(X=-130) #700
    show amira st01 at xy(X=200)
    with dissolve #701

    a "Can you look into the Orb treasures?"
    l "Alice, do you want the Orbs?"

    show alice st13b at xy(X=-130) with dissolve #700

    a "You idiot, it's for your adventure!{w}{nw}"

    show alice st14b at xy(X=-130)
    $ renpy.transition(dissolve, layer="master") #700

    extend "...It's for when they may be needed."

    hide alice
    show amira st01 at center
    with Dissolve(1.5) #700

    amira "I will do anything for my Hero.{w}\nI'll collect information about the Orb's locations."
    l "Ah, yes... Please do."
    "I don't really get it, but if Alice wants them, then there must be a reason."
    amira "Then I must go-amira.{w}\nAs many wise characters have done before me, I shall attach \"amira\" to the end of my sentences to endear me to the audience."
    l "I don't think cheesy character tricks like that will make peo..."
    amira "Now farewell-desu!{w} I mean-amira."
    amira "One more time.{w} Farewellamia!"

    play sound "audio/se/escape.ogg"
    hide amira with Dissolve(1.5)
    play music "audio/bgm/city1.ogg"

    l "...She said Lamia at the end, didn't she?"

    show alice st11b with dissolve #700

    a "I was just ignoring her like usual, I don't know."
    "At any rate, I know where Salamander is now.{w}\nFire Spirit's Volcano...{w} It sounds like another troublesome location..."

    $ hanyo[1] = 1
    return


label city21_10:
    scene bg 151 with blinds
    show ant st01 with dissolve #700

    ant_girl "WELCOME."
    l "Oh come on, they're even making Ant Girls be the shopkeepers?"
    "I take a look behind the counter, and see a man reading a book with a bored look on his face.{w}\nThey really are just making the Ant Girls do everything..."
    ant_girl "DO YOU NEED SOMETHING?"
    l "No, not really..."
    "I can't bring myself to go shopping here...{w}\nI quickly leave the shop."

    scene bg 100 with blinds
    return


label city21_11:
    scene bg 156 with blinds
    show ant st01 with dissolve #700

    ant_girl "I SHALL PRAY FOR YOU."
    l "Oh come on, an Ant Girl is the acting priest!?"
    "Just what is the priest here thinking?{w}\nI think Ilias would strongly object to this..."
    ant_girl "PRAY..."
    l "Ah, yes... Let's pray."
    "I pray to Ilias with the Ant Girl, then leave the church."

    scene bg 100 with blinds
    return


label city21_12:
    scene bg 161 with blinds

    l "Uwa, this place is huge..."
    "I walk into a huge room, with a ceiling higher than any I've ever seen before.{w}\nIt's big enough to fit that Golem Girl from before."

    show mob madou with dissolve #700

    magic_engineer "Ara, here for an inspection?{w}\nThere are a lot of things I can't show you, so I can't particularly welcome you."
    l "Is this where the Golem Girl was made?"
    magic_engineer "Yes, this is where we refine our Magic Science.{w}\nGrangold is the most advanced in all the world."

    show alice st11b at xy(X=-93) #700
    show mob madou at xy(X=160)
    with dissolve #701

    a "...I'd like to ask a question.{w}\nHow do you issue orders to the Ant Girls?"
    magic_engineer "That's classified, so I can't say too much...{w}\nBut they basically recognize human orders as coming from the Queen Ant."
    a "You can do that...?{w}\nAnyway, where is their true leader, the Queen Ant, located at?"
    magic_engineer "Hehe, you want to know?{w}\nThe Queen Ant is in the basement of the castle."

    show alice st13b at xy(X=-93) with dissolve #700

    a "Is she sealed...?{w}\nCan humans do such a thing?"
    magic_engineer "How it was done is classified.{w}\nBut since it was done, the Ant Girls have become quite useful."

    show alice st14b at xy(X=-93) with dissolve #700

    a "Such absurdity...{w}\nA Queen class monster was sealed by normal human's power..."
    "Alice goes silent after muttering that to herself."
    magic_engineer "I'm awfully curious...{w}\nAre you perhaps a spy from another country?"
    magic_engineer "If so, you can't duplicate it in other countries.{w}\nBecause it's only possible using the cutting edge in Magic Science."
    l "..............."
    "It seems like they really are the most advanced in the world.{w}\nAlice doesn't seem to like it, either..."

    scene bg 100 with blinds
    $ hanyo[2] = 1
    return


label city21b_main0:
    $ BG = "bg 041"
    play sound "audio/se/asioto2.ogg"
    scene bg 101 with blinds2
    play music "audio/bgm/castle4.ogg"


label city21b_main:
    call city_null
    $ city_bg = "bg 101"
    $ city_button01 = "Minister A" #601
    $ city_button02 = "Minister B" #602
    $ city_button03 = "Diplomat" #603
    $ city_button04 = "Ant Girls" #604
    $ city_button05 = "Ant Girl Guard" #605
    $ city_button10 = "Audience Hall" #610
    $ city_button11 = "Guardroom" #611
    $ city_button19 = "Return to Grangold Town" #619

    while True:
        call cmd_city

        if result == 1:
            call city21b_01
        elif result == 2:
            call city21b_02
        elif result == 3:
            call city21b_03
        elif result == 4:
            call city21b_04
        elif result == 5:
            call city21b_05
        elif result == 10:
            call city21b_10
        elif result == 11:
            call city21b_11
        elif result == 19:
            play sound "audio/se/asioto3.ogg"
            scene bg 100 with blinds2
            play music "audio/bgm/city1.ogg"
            jump city21_main


label city21b_01:
    show mob daizin with dissolve #700

    minister_a "We leave all the financial stuff and document filing to the Ant Girls.{w}\nIt's been a long time since we've actually had to do anything."
    minister_a "But even still, my salary is huge.{w}\nHahaha, I love it!{w} I can't stop laughing, hahaha!"

    return


label city21b_02:
    show mob daizin with dissolve #700

    minister_b "I work hard."
    minister_b "I've counted all the trees outside my window today.{w}\nOh, and every time a leaf comes through my window, I see how long I can blow on it to keep it in the air."

    return


label city21b_03:
    show mob daizin with dissolve #700

    diplomat "I leave most of the work to the Ant Girls.{w}\nBut since using Ant Girls as diplomats won't work in a lot of places, I still have to work."
    diplomat "I just got back from Lady's Village on the tip of the continent the other day."
    l "Lady's Village...?"
    diplomat "Just like the name implies, it's a village where a bunch of beautiful women live.{w}\nBut there's been a strange rumor...{w} So I went to see if it was true or not."
    l "A strange rumor...?{w} What was it?"
    diplomat "The male adventurers that go to Lady's Village never come back."
    diplomat "Women don't have any issues, but men don't ever seem to leave.{w}\nIt's a strange rumor."
    l "That seems awfully suspicious..."
    "If only men are vanishing, it has to be the work of monsters..."
    diplomat "I couldn't leave the issue alone, with people vanishing.{w}\nSo I went as a diplomat..."
    diplomat "It was quite a nice village, very lovely.{w}\nAnd the female Lord, Cassandra, was quite lovely herself."

    show alice st61b at xy(X=-93) #700
    show mob daizin at xy(X=160)
    with dissolve #701

    a "Cassandra... Was it?"
    "Alice's eyes snap open all of a sudden."
    l "What's wrong?"

    show alice st14b at xy(X=-93) with dissolve #700

    a "Ah, nothing..."
    "Alice goes quiet, and lowers her gaze.{w}\nDid the name remind her of something?"
    diplomat "In the end, it turned out to be just a rumor after all.{w}\nThere are a lot of powerful monsters around the village, it seems like they were just attacked by them."
    l "Is that really it...?{w}\nI want to go there and see if there's something else..."

    show alice st11b at xy(X=-93) with dissolve #700

    a "...No.{w} Don't go close to that village."
    "Alice suddenly speaks up again.{w}\nJudging from her tone, it seems like there's a reason besides it just being troublesome that she doesn't want to go."
    l "...Why not?{w}\nYou know something, don't you?"
    a "It doesn't matter, it's just my advice.{w}\nDon't go near that village."
    "I don't know why, but she seems to feel pretty strongly about this.{w}\nAt any rate, I'll see if I can learn more about Lady's Village..."

    $ hanyo[3] = 1
    return


label city21b_04:
    show ant st01 at xy(X=-160) #700
    show ant st01 at xy(X=160) as ant2
    with dissolve #701

    ant_girl_a "NORTHWEST DISTRICT TAX COLLECTION IS BAD.{w}\nDISPATCH NEW TAX OFFICIAL."
    ant_girl_b "ORANGE FARMERS PETITION ARRIVED.{w}\nSABASA PRODUCE IDEAL TARIFF IS..."
    "The Ant Girls are chattering all over about various government issues...{w}\nUnlike the humans, they seem to be working really hard."

    return


label city21b_05:
    show ant st01 with dissolve #700

    ant_girl_guard "INTRUDER.{w} ATTACK!"
    l "I...I'm not an intruder!"
    ant_girl_guard "THEN I WON'T ATTACK."
    "They're even making them guard the royal palace?"

    return


label city21b_10:
    scene bg 045 with blinds
    show grangold st01 with dissolve #700

    grangold_king "Oh, if it isn't the Hero Luka!{w}\nDid you come to tell me more stories?"

    show grangold st02 with dissolve #700

    grangold_king "I'm so bored every day, it feels like I'm going to die from it.{w}\nIt has even gotten so bad, I almost thought about doing some political stuff before."
    l "............."
    "Is it really alright for someone like this to be the King?"

    scene bg 101 with blinds
    return


label city21b_11:
    scene bg 060 with blinds
    show mob heisi1 with dissolve #700

    guard "The Ant Girls are the soldiers, and the Golem Girl is the gatekeeper...{w}\nThere isn't any other country that has such reassuring military power, is there?"
    guard "We're more like decorations here.{w}\nI just drink tea and eat snacks in here..."
    "...He looks a little lonely."

    scene bg 101 with blinds
    return


label city22_main0:
    $ BG = "bg 041"
    play sound "audio/se/asioto2.ogg"
    scene bg 100 with blinds2
    play music "audio/bgm/city1.ogg"


label city22_main:
    call city_null
    $ city_bg = "bg 100"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Woman" #602
    $ city_button03 = "Old Man" #603
    $ city_button04 = "Old Woman" #604
    $ city_button05 = "Ant Girl" #605
    $ city_button06 = "Mud Golem Girl" #606
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Magic Laboratory" #612
    $ city_button13 = "Grangold Castle" #613
    $ city_button14 = "Assembly Hall" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city

        if result == 1:
            call city22_01
        elif result == 2:
            call city22_02
        elif result == 3:
            call city22_03
        elif result == 4:
            call city22_04
        elif result == 5:
            call city22_05
        elif result == 6:
            call city22_06
        elif result == 10:
            call city22_10
        elif result == 11:
            call city22_11
        elif result == 12:
            call city22_12
        elif result == 13:
            call city22_13
        elif result == 14:
            call city22_14
        elif result == 19:
            return


label city22_01:
    show mob seinen1 with dissolve #700

    youth "We forced the Ant Girls to work so hard...{w}\nI always thought they were happy to do it, but I see that I was wrong."
    youth "I won't let anything like that happen again.{w}\nThe Ant Girls are citizens of Grangold, just like me."

    return


label city22_02:
    show mob musume2 with dissolve #700

    woman "I'll let the Ant Girls do all the cooking and cleaning while I take it easy."
    woman "The only housewife duties I need to do are make children.{w}\nAnd the Ant Girls will even take care of the children after I give birth..."
    woman "...Is what I said to my husband.{w}\n\"Well then, I guess I should marry an Ant Girl and get rid of you\" he replied."
    woman "So then he divorced me.{w}\nI hate monsters!"

    return


label city22_03:
    show mob ozisan2 with dissolve #700

    old_man "I'm going to leave this town soon."
    old_man "We did such horrible things to Ant Girls...{w}\nThere's no way trust can develop after such awful things happened."
    old_man "I wonder where I can go... Maybe Gold Port?"

    return


label city22_04:
    show mob rouba with dissolve #700

    old_woman "After running from Succubus Village here for refuge, I almost ended up spending the rest of my life as a slave..."
    old_woman "At least it ended peacefully here..."

    $ hanyo[0] = 1
    return


label city22_05:
    show ant st01 with dissolve #700

    ant_girl "I DON'T MIND THIS KIND OF WORK."
    "The Ant Girls are working hard..."

    return


label city22_06:
    show madgolem st01 with dissolve #700

    mud_golem_girl "WE'RE CURRENTLY DOING CONSTRUCTION WORK.{w}\nIT'S DANGEROUS HERE, SO PLEASE STEP BACK."
    "The Mud Golem Girl seems to be talking a little bit more normally...{w}\nI wonder if they're turning more human like?"

    return


label city22_10:
    scene bg 151 with blinds
    show mob ozisan1 at xy(X=-160) #700
    show ant st01 at xy(X=160)
    with dissolve #701

    shopkeeper "Welcome to the restored Tool Shop!"
    ant_girl "WELCOME..."
    shopkeeper "I'm trying my best to get everything right...{w}\nBut I'm still relying on the Ant Girl a lot to help me."
    l "I...I see..."
    "It seems like the humans who were taking it easy for so long are having some difficulty working again.{w}\nThey'll need to break their old habits before they can live together in peace..."

    scene bg 100 with blinds
    return


label city22_11:
    scene bg 156 with blinds
    show ant st01 with dissolve #700

    ant_girl "I SHALL PRAY FOR YOU..."
    l "Oh come on, the priest is still leaving it all up the Ant Girls!?"

    show ant st01 at xy(X=-160) #700
    show mob rouzin1 at xy(X=160)
    with dissolve #701

    elder "Ah, you see...{w}\nThe priest that used to be here was suddenly struck by lightning."
    elder "He was probably punished by Ilias..."
    l "I...I see..."
    "It looks like the church is left to the Ant Girls after all..."

    hide mob
    show ant st01 at center
    with dissolve #700

    ant_girl "LET'S PRAY..."
    l "Ah, yeah... Let's pray..."
    "I pray to Ilias with the Ant Girl, then leave the church."

    scene bg 100 with blinds
    return


label city22_12:
    scene bg 161 with blinds
    show mob madou with dissolve #700

    magic_engineer "Our present work is only in maintaining the artificial monsters.{w}\nThey don't have any will, so they won't complain if we overwork them.{w} ...Probably."
    l "Are you sure they don't have a will...?"
    "If the artificial monsters have feelings, they're going to revolt next..."
    magic_engineer "Also... Promestein stopped communicating with us.{w}\nShe remained a mystery to the very end."
    l "I see..."
    "Why did she give them all this technology...?{w}\nWho on earth is she?"

    scene bg 100 with blinds
    $ hanyo[2] = 1
    return


label city22_13:
    l "It looks like nobody is allowed past here..."
    "The castle is still being repaired.{w}\nIt doesn't look like they're letting anyone in."

    return


label city22_14:
    scene bg 105 with blinds
    show grangold st04 with dissolve #700

    grangold_king "Hey, how are you today?{w}\nWe're still working out plans for repairing the town."
    grangold_king "I don't have any free time like before, but I feel a lot happier.{w}\nI'm only now feeling like a King, even though I've sat on the throne for a long time..."

    show grangold st04 at xy(X=-160) #700
    show queenant st01 at xy(X=160)
    with dissolve #701

    queen_ant "I'm cooperating with planning the town's repairs, too."
    queen_ant "This is the place my children will live.{w}\nWe must join forces with the humans to build it."

    scene bg 100 with blinds
    $ hanyo[2] = 1
    return


label city23_main:
    call city_null
    $ city_bg = "bg 103"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Prostitute" #602
    $ city_button03 = "Sisters" #603
    $ city_button04 = "Soldier A" #604
    $ city_button05 = "Soldier B" #605
    $ city_button06 = "Soothsayer" #606
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Bar" #612
    $ city_button13 = "Village Chief's House" #613
    $ city_button19 = "Head to the Inn" #619

    while True:
        if hanyo[0] + hanyo[1] == 2:
            $ city_act19 = 1
        elif hanyo[0] + hanyo[1] < 2:
            $ city_act19 = 0

        call cmd_city

        if result == 1:
            call city23_01
        elif result == 2:
            call city23_02
        elif result == 3:
            call city23_03
        elif result == 4:
            call city23_04
        elif result == 5:
            call city23_05
        elif result == 6:
            call city23_06
        elif result == 10:
            call city23_10
        elif result == 11:
            call city23_11
        elif result == 12:
            call city23_12
        elif result == 13:
            call city23_13
        elif result == 19:
            return


label city23_01:
    show mob seinen1 with dissolve #700

    youth "I won't run away from the village.{w}\nI'll defend my hometown, and confront the Succubi!{w}\nOh... Oh, please don't look at me like that! I'm not thinking of anything lewd!"
    youth "Aha. Ahahahaha!"

    show alice st11b at xy(X=-110) #700
    show mob seinen1 at xy(X=160)
    with dissolve #701

    a "......Damn pervert."

    return


label city23_02:
    show maccubus st21 at xy(X=175) with dissolve #700

    prostitute "Oh, excuse me.{w}\nIt's a little cold today, so I'm not working today."
    l "Err, that's not what I'm after... I just wanted to talk to people.{w}\nBut there sure aren't too many people around..."
    prostitute "All the people with money fled to the other villages and towns.{w}\nThose remaining here are the poor. Or the ones with other reasons..."
    l "I see..."
    "Naturally, the village should have been abandoned...{w}\nI wonder why she didn't leave..."
    prostitute "Most people left... But a few people came here on purpose.{w}\nA couple are brave Heroes to defend the village...{w}\nBut the rest are all perverts who want to be attacked by a Succubus."
    l "I... I came to defend the village!"
    prostitute "Hahaha... That's what they all say!"
    l "................."

    show alice st11b at xy(X=-110) #700
    show maccubus st21 at xy(X=335)
    with dissolve #701

    a "......Damn pervert."
    l "No! I told you that's not it!"

    return


label city23_03:
    show minccubus st11 at xy(X=335)
    show renccubus st11 at xy(X=15) #700
    with dissolve #701

    young_girl "You came here to defend this village as well, didn't you?{w}\nPlease, give it your all..."
    girl "Good luck, big bro!"
    l "Yes, I'll do my best!{w}\nBy the way, why haven't you two evacuated?"
    young_girl "We don't have enough money...{w}\nIf we lost our farm here, we would have to live out on the streets."
    young_girl "We have no way to escape..."
    l "That's horrible..."
    "Two young sisters like this can't live on the streets like that...{w}\nEven if they know it's dangerous, they aren't even able to leave..."
    l "Leave it to me! I'll defend the village for sure!"
    "I swear it in my mind."

    show alice st11b at xy(X=-160) #700
    show renccubus st11 at xy(X=195) #701
    show minccubus st11 at xy(X=415)
    with dissolve #702

    a "......Damn pervert."
    l "No! I told you that's not it!"

    return


label city23_04:
    show mob sensi1 with dissolve #700

    soldier_a "I came to defend this village!{w}\nEven if I'm left completely exhausted, I won't regret it!"

    show alice st11b at xy(X=-110) #700
    show mob sensi1 at xy(X=160)
    with dissolve #701

    a "......Damn pervert."

    return


label city23_05:
    show mob sensi2 with dissolve #700

    soldier_b "I came here to have sex with a Succubus!{w}\nIf I'm left completely exhausted, that will be my reward!"

    show alice st11b at xy(X=-110) #700
    show mob sensi2 at xy(X=160)
    with dissolve #701

    a "......Damn honest."

    return


label city23_06:
    show witchs st21 at xy(Y=50) with dissolve #700

    soothsayer "Young man over there, would you like your fortune told?{w}\nSince the village is in such a horrid state, I'll do it for free!"
    l "Sure..."

    show witchs st22 at xy(Y=50) with dissolve #700

    soothsayer "Hmmmm... I sense issues with women.{w}\nThere is surely going to be trouble with women ahead for you..."
    soothsayer "Well? Did I get it right?"
    l "Amazing! That's exactly right!{w}\nHow do I avoid it!?"

    show witchs st23 at xy(Y=50) with dissolve #700

    soothsayer "That's just your fate. Surrender to it."
    l "What? That's awful...{w}\nAnyway, why haven't you evacuated?"

    show witchs st21 at xy(Y=50) with dissolve #700

    soothsayer "Even if trouble is coming, I won't turn tail and run.{w}\nIf the Succubi come, I'll just offer them my trade..."
    l "You have some guts..."

    show witchs st24 at xy(Y=50) with dissolve #700

    soothsayer "By the way, do you know why the Succubi attack the people here?"
    l "Err... Because tonight their magic will be at their most powerful.{w}\nSo they all group together and attack at the same time... Right?"
    soothsayer "...Too bad, but you're a little off.{w}\nIf it was an ordinary Purple Sabbath, you would be correct."
    soothsayer "But there's a different purpose this time...{w}\nThey're grouping up to fill... another purpose."
    l "Another purpose...?{w}\nWhat is it?"

    show witchs st23 at xy(Y=50) with dissolve #700

    soothsayer "Hehe... That's a secret.{w}\nWhen tonight comes, you'll understand..."
    l "How do you know such a thing...?{w}\n...You must be a really powerful soothsayer!"

    show witchs st22 at xy(Y=50) with dissolve #700

    soothsayer "Th...thanks..."

    show alice st11b at xy(X=-110) #700
    show witchs st22 at xy(X=160, Y=50)
    with dissolve #701

    a "......Damn idiot."

    $ hanyo[0] = 1
    return


label city23_10:
    scene bg 151 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "I've been keeping this tool shop going since my great grandfather started it!{w}\nIf the Succubi come, is this shop going to close!?"
    shopkeeper "I have my pride as a businessman to think of!{w}\nIf you're going to attack, then attack damn it!"
    shopkeeper "Attack me!{w} I'll show you all!{w}\nUuuuu....{w} Uwaaaaa!!!"
    l "P...Please cheer up..."
    "Why do I have to comfort him..."

    scene bg 103 with blinds
    return


label city23_11:
    scene bg 156 with blinds

    l "What...? There's no priest...?"
    "On the desk is a letter.{w}\n\"I'll be gone for a while.{w} - Gold West Parish Priest.\""
    l "Even the priest ran away...?"

    scene bg 103 with blinds
    return


label city23_12:
    scene bg 107 with blinds
    show mob gorotuki3 at xy(X=-270) #700
    show mob seinen2 as mob2 #701
    show mob sensi1 at xy(X=270) as mob3
    with dissolve #702

    bully "What's up, kid?{w}\nThis bar ain't a damn kid's playground!"
    soldier_a "The beer is tasting like crap because of you. Get out!"
    soldier_b "Boy, get out of this village quickly.{w}\nMonsters are going to attack soon..."
    l "Y...Yes... Sorry..."
    "It looks like I'm not welcome in here.{w}\nI sheepishly back out of the bar."
    "Honestly, it didn't really look like they were looking for a fight.{w}\nI don't mean to badmouth them, but they look pretty weak..."

    scene bg 103 with blinds
    return


label city23_13:
    scene bg 106 with blinds
    show succubus st11 at xy(Y=50) with dissolve #700

    village_chief "Welcome, Hero.{w}\nI'm the chief of this village."
    l "He...Hello..."
    "Unexpectedly, the chief is a beautiful young woman.{w}\nShe scans me slowly from head to toe."
    village_chief "You know, right...{w}\nEvery 100 years, Succubi attack this village in a large group."
    village_chief "It's called the Purple Sabbath. And it's going to happen tonight."
    l "Yes. I came here to defend the village against that!"

    show succubus st14 at xy(Y=50) with dissolve #700

    village_chief "Hehe... That will be a big help.{w}\nWe can always do with more strong men..."
    l "Ye...Yes... Thank you..."
    village_chief "I'll be looking forward to it, Mr. Cute Hero."
    l "Le...Leave it to me..."
    "...I get a very odd feeling from her."

    $ hanyo[1] = 1
    scene bg 103 with blinds
    return


label city24_main0:
    $ BG = "bg 041"
    play sound "audio/se/asioto2.ogg"
    scene bg 103 with blinds2
    play music "audio/bgm/mura2.ogg"


label city24_main:
    call city_null
    $ city_bg = "bg 103"
    $ city_button01 = "Youth" #601
    $ city_button02 = "Prostitute" #602
    $ city_button03 = "Sisters" #603
    $ city_button04 = "Soldier" #604
    $ city_button05 = "Yamatai Youth" #605
    $ city_button06 = "Soothsayer" #606
    $ city_button10 = "Tool Shop" #610
    $ city_button11 = "Church" #611
    $ city_button12 = "Bar" #612
    $ city_button13 = "Village Chief" #613
    $ city_button14 = "Ceremony Room" #614
    $ city_button19 = "Leave" #619

    while True:
        call cmd_city

        if result == 1:
            call city24_01
        elif result == 2:
            call city24_02
        elif result == 3:
            call city24_03
        elif result == 4:
            call city24_04
        elif result == 5:
            call city24_05
        elif result == 6:
            call city24_06
        elif result == 10:
            call city24_10
        elif result == 11:
            call city24_11
        elif result == 12:
            call city24_12
        elif result == 13:
            call city24_13
        elif result == 14:
            call city24_14
        elif result == 19:
            return


label city24_01:
    show mob seinen1 with dissolve #700

    livestock_a "Th...This is the... Succubus Village...{w}\nW...Welcome to... Ah.... Heaven..."
    "He looks weak..."

    return


label city24_02:
    show maccubus st11 at xy(X=175) with dissolve #700

    maccubus "It's hard to do business in this village now.{w}\nThere are so many Succubi, who would ever buy a prostitute."
    maccubus "I wonder if I should immigrate to Sabasa, or the Noah Region?{w}\nBut I'd have to hide my true nature in other towns..."
    l "That aside...{w} Why are you standing around naked?"

    return


label city24_03:
    show renccubus st01 at xy(X=15) #700
    show minccubus st01 at xy(X=335, Y=50)
    with dissolve #701

    lencubus "Being a Succubus is wonderful.{w}\nEven without money, I can live a comfortable life."
    lencubus "Whenever I'm hungry, all I have to do is suck the nearest man's energy...{w}\nI don't even need clothes!"
    minccubus "The men are all so nice to me, too!{w}\nIsn't this great, sister!? {image=note}"
    l "............"

    return


label city24_04:
    show mob sensi1 with dissolve #700

    livestock_b "Haa... H... Ahhh...{w} Ahhhn...{w}\nH...ell..{w} Ahhh!{w} Hell... He..."
    l "..........."
    "Is he trying to greet me or something?"

    return


label city24_05:
    show mob seinen1 with dissolve #700

    if ivent09 == 0:
        youth "I'm a traveler from Yamatai.{w}\nI heard this is a place where a man's dreams can come true."
    else:
        youth "Hey, do you remember me from Yamatai?{w}\I spent all my money on a carriage here, and managed to get here safely somehow."

    youth "I'm so full of hope at the new life that awaits me here!{w}\nMy penis is so hard in expectation, it feels like I could burst any moment!"
    l "Damn pervert..."

    show alice st13b with dissolve #700

    a "Don't copy me, you idiot!"

    return


label city24_06:
    show witchs st11 at xy(Y=50) with dissolve #700

    succubus_witch "Ara, welcome...{w}\nDo you want your fortune again?"
    succubus_witch "Or perhaps you want something a little more... Special?"
    l "Err... I'll take the fortune..."
    succubus_witch "Just as before, I see issues with women ahead for you...{w}\nIt seems that no matter what path you take, that will never change for you..."
    l "Can I not do anything to change my fate?"
    succubus_witch "There is a way to avoid the women trouble, actually...{w}\nYou just need to live in this village."
    l "Wouldn't that just make my women trouble even worse...?"
    succubus_witch "Well, besides that, you'd be safe from monsters...{w}\nEspecially from falling in love with powerful, high ranking monsters...{w}\nThat's the most dangerous of them all."
    l "Eh...?{w} Why...?"

    show witchs st14 at xy(Y=50) with dissolve #700

    succubus_witch "When high level monsters fall in love, the desire to mate with their love is intense...{w}\nTheir hot passion burns seemingly endlessly, indulging in their man in an almost bottomless lust."
    succubus_witch "A normal man cannot tolerate that kind of desire.{w}\nWill they lose their mind, or perhaps die of weakness?"
    succubus_witch "At the very least, they'll probably never walk again..."
    l "Urhg..."
    succubus_witch "That's why many powerful monster clans have rules about only marrying men stronger than they are."
    succubus_witch "Because a weaker man will generally just die as they mate.{w}\nDue to that, it's difficult for a marriage of love to ever work out."
    succubus_witch "But the foolproof way to protect yourself is to...{w}{nw}"

    show witchs st13 at xy(Y=50)
    $ renpy.transition(dissolve, layer="master") #700

    extend "{w=.7}\nLive here instead."
    l "N...No thanks... I'll be careful..."
    "I'm not sure what I should be careful about, though...{w}\nI'll keep that in mind, though."

    return


label city24_10:
    scene bg 151 with blinds
    show mob ozisan1 with dissolve #700

    shopkeeper "All during that \"Purple Sabbath\" I was raped over and over...{w}\nI... I...{w} Ahhhh! Waaah!"
    l "P...Please cheer up..."
    shopkeeper "I loved it!{w} I'm so happy... I... I can't stop the tears!"

    scene bg 103 with blinds
    return


label city24_11:
    scene bg 156 with blinds

    l "Eh...?{w} There's no priest...?"
    "Oh, there's a note on the desk..."
    "\"The Church is closed.{w} The priest has retired and taken a new career as a domestic animal.\""
    l "................"

    scene bg 103 with blinds
    return


label city24_12:
    "I stop in front of the bar as I walk by.{w}\nIt seems like something strange is happening inside..."
    succubus_a "Haha, you love this, don't you!?{w}\nCome as much as you want!"
    male_voice_a "Ahhhhhh!!"
    succubus_b "If I pump you like this...{w}\nYou can't endure it, right!?"
    male_voice_b "Ahhhhhhh!!"
    l "..........."
    "I slowly back away from the bar, without going inside."

    return


label city24_13:
    scene bg 106 with blinds
    show succubus st01 at xy(Y=50) with dissolve #700

    village_chief "Oh, welcome.{w}\nWe've been flooded with immigrant applications from all over the world."
    village_chief "Hehe... It's an all you can eat buffet..."
    l "Uhm... Nobody is dying, right...?"
    village_chief "Of course, we would never allow that.{w}\nWe simply squeeze them right to the edge.{w} Slowly...{w} Carefully..."
    village_chief "Stroking...{w} Milking..."
    l ".............."
    village_chief "Why don't you join them for a little while, too?{w}\nI'm sure you've built up a large supply of semen during your long journey.{w} How about we milk it all out for you?"
    l "N...No thanks!"

    scene bg 103 with blinds
    return


label city24_14:
    scene bg 110 with blinds
    show lilith st01 at xy(X=138) with dissolve #700

    lilith "All of the love that Alice the Eighth had 500 years ago...{w}\nThat hateful Hero destroyed it."
    lilim "Not just us, but Her Majesty as well...{w}\nSister, I hate that Hero..."
    l "Uhm... The name of that Hero...{w}\nDo you know what it was...?"
    lilith "I don't want to remember his name.{w}\nBut his energy did look quite delicious..."
    lilim "Just like yours.{w} Teehee."

    scene bg 103 with blinds
    return


label city25_main:
    call city_null
    $ city_bg = "bg 111"
    $ city_button01 = "Lady on a Stroll" #601
    $ city_button02 = "Umbrella Lady" #602
    $ city_button03 = "Maid-Like Woman" #603
    $ city_button04 = "Refined Girl" #604
    $ city_button05 = "Female Adventurer" #605
    $ city_button10 = "Lord's Mansion" #610
    $ city_button19 = "Leave" #619
    $ city_act19 = 0

    while True:
        if hanyo[0] == 0:
            $ city_act03 = 1
        elif hanyo[0] == 1:
            $ city_act03 = 0

        if hanyo[1] == 0:
            $ city_act04 = 1
        elif hanyo[1] == 1:
            $ city_act04 = 0

        if hanyo[0] + hanyo[1] == 2:
            $ city_act10 = 1
        elif hanyo[0] + hanyo[1] < 2:
            $ city_act10 = 0

        call cmd_city

        if result == 1:
            call city25_01
        elif result == 2:
            call city25_02
        elif result == 3:
            call city25_03
        elif result == 4:
            call city25_04
        elif result == 5:
            call city25_05
        elif result == 10:
            return


label city25_01:
    show madaminsect st11 with dissolve #700

    lady_on_a_stroll "Oh my... A traveler?{w}\nThis is a wonderful village for just relaxing.{w}\nPlease enjoy your stay here, and may the fatigue from your travels melt away."
    l "Excuse me, I'd like to ask you something...{w}\nHave any other male travelers visited this village recently?"
    lady_on_a_stroll "Of course... This is an open village, after all.{w}\nThough most of them seem to leave right away."
    lady_on_a_stroll "Is there something wrong?"
    l "No..."
    "So are they being attacked after they leave...?"

    return


label city25_02:
    show madamumbrella st11 with dissolve #700

    umbrella_lady "Oh my, how rare.{w} A traveler?{w}\nEven more, such a young boy, too!"
    "The lady carefully sizes me up, head to toe.{w}\nHer gaze is sort of uncomfortable..."
    l "Excuse me, I'd like to ask you something...{w}\nHave any other male travelers visited this village recently?"
    umbrella_lady "My, not that I've seen.{w}\nNot many travelers would choose to come to such a remote region as this."
    l "I...Is that so...?"
    "Nobody is coming here in the first place?{w}\nDoes that mean they're being attacked before they get here?"

    return


label city25_03:
    show maidscyulla st11 with dissolve #700

    maid "My name is Ran.{w} I'm a maid that serves the Lord here, Cassandra."
    ran "I'm currently shopping in preparation for tonight's meal...{w}\nDid you need something from me?"
    l "Excuse me, I'd like to ask you something..."
    ran "I'm sorry, but I must be going.{w}\nI can't neglect my Master's order."
    "Saying that, the maid named Ran quickly hurries off."

    $ hanyo[0] = 1
    return


label city25_04:
    show emily st23 with dissolve #700

    refined_girl "How rude.{w}\nIntroduce yourself before approaching me out of nowhere!"
    l "...Sorry."
    "I step back at her harsh scolding."
    l "I'm the Hero Apprentice Luka.{w}\nErr... Who are you?"

    show emily st21 with dissolve #700

    refined_girl "I'm Emily, Lord Cassandra's only daughter."
    emily "Did you come to meet my mother as well?"
    l "Before that, I'd like to ask you something.{w}\nIt's about the rumors going around about this village..."

    show emily st22 with dissolve #700

    emily "Ah, that stupid rumor?{w}\nSomething about male travelers never returning, or something?"
    l "...You've heard of it?"
    emily "Wahaha!{w} You believed it, and came all the way here!?{w}\nWhat a stupid urban legend!"
    "The girl loudly laughs.{w}\nUrban legend...?{w} So it was just a rumor after all?"

    show emily st21 with dissolve #700

    emily "Since you came all this way, do you want to meet with mother?{w}\nI feel sorry for you, having wasted all that effort traveling here on a wild goose chase."
    l "Oh... That would be good..."
    emily "Alright then, follow Emily!"

    play sound "audio/se/asioto1.ogg"
    hide emily with dissolve

    "Emily says that, then runs off."
    "...So it wasn't true after all?{w}\nMight as well ask the Lord since she offered."

    $ hanyo[1] = 1
    return


label city25_05:
    show mob sensi3 with dissolve #700

    female_adventurer "I came here because of that strange rumor going around, but there doesn't seem to be anything strange going on here.{w}\nIt just seems like a country resort for rich women to laze around in."
    female_adventurer "I wish I could settle down in a place like this and just lounge around, too..."

    return


label city26_main0:
    $ BG = "bg 099"
    play sound "audio/se/asioto2.ogg"
    scene bg 119 with blinds2
    play music "audio/bgm/gordport.ogg"


label city26_main:
    call city_null
    $ city_bg = "bg 119"
    $ city_button01 = "Youth" #601
    $ city_button02 = "City Girl" #602
    $ city_button03 = "Mysterious Woman" #603
    $ city_button04 = "Ruffian" #604
    $ city_button05 = "Hunter" #605
    $ city_button06 = "Soldier" #606

    if ivent06 == 1 and check03 < 3:
        $ city_button07 = "Officer" #607

    $ city_button10 = "Weapon Shop" #610
    $ city_button11 = "Tool Shop" #611
    $ city_button12 = "Lottery" #612
    $ city_button13 = "Ilias Kreuz HQ" #613
    $ city_button14 = "Port" #614
    $ city_button15 = "Hero's Shrine" #615
    $ city_button19 = "Leave" #619

    while True:
        if check03 == 0 or (check03 == 2 and hanyo[1] + hanyo[2] == 2) or check03 == 3:
            $ city_act19 = 1
        elif check03 == 1 or (check03 == 2 and hanyo[1] + hanyo[2]) < 2:
            $ city_act19 = 0

        call cmd_city

        if result == 1:
            call city26_01
        elif result == 2:
            call city26_02
        elif result == 3 and check03 < 2:
            call city26_03a
        elif result == 3 and check03 == 2 and hanyo[0] == 0:
            call city26_03a
        elif result == 3 and check03 == 2 and hanyo[0] == 1:
            call city26_03b
        elif result == 3 and check03 == 3:
            return_to lb_0262
        elif result == 4:
            call city26_04
        elif result == 5:
            call city26_05
        elif result == 6:
            call city26_06
        elif result == 7 and check03 < 2:
            call city26_07a
        elif result == 7 and check03 == 2:
            call city26_07b
        elif result == 10:
            call city26_10
        elif result == 11:
            call city26_11
        elif result == 12:
            call city26_12
        elif result == 13:
            call city26_13
        elif result == 14 and check03 == 0:
            call city26_14a
        elif result == 14 and check03 == 1:
            return
        elif result == 14 and check03 == 2:
            call city26_14b
        elif result == 14 and check03 == 3:
            call city26_14c
        elif result == 15 and item17 == 1:
            call city26_15c
        elif result == 15 and check05 == 0:
            call city26_15a
        elif result == 15 and check05 == 1 and item12 == 2 and item13 == 3 and item18 == 1:
            call city26_15b
        elif result == 15:
            call city26_15a
        elif result == 19:
            return


label city26_01:
    show mob seinen1 with dissolve #700

    youth "Hey traveler, welcome to Gold Port!"
    youth "Our town has a bad reputation as a place for lowlifes...{w}\nBut it's actually a nice place!"
    youth "Since we're open to all people, lots of thugs and lowlifes seem to have flocked here, though...{w}\nOf course, we won't accept monsters."

    return


label city26_02:
    show mob musume1 with dissolve #700

    city_girl "I immigrated here from Grangold.{w}\nThere were too many Ant Girls back there..."
    city_girl "I can't calm down when I'm near monsters.{w}\nEvery time they were near me, I just got the chills..."

    return


label city26_03a:
    show serene st01 with dissolve #700

    woman "You seem to be quite skilled.{w}\nCould I ask a favor of you...?"
    l "If it's something I can do..."
    woman "...Ah, there's an ill wind blowing today.{w}\nMaybe some other time."
    l "O...Ok..."
    "What the heck was that?{w}\nWhat a strange woman..."

    return


label city26_03b:
    show serene st01 with dissolve #700

    woman "You've heard of Captain Selene, haven't you?{w}\nAre you interested in learning more about that legendary pirate?"
    l "Err, I'm looking for the Purple Orb..."
    woman "Purple Orb...{w} That's the treasure that Captain Selene loved the most.{w}\nEven on her last voyage, she never let it leave her side."
    l "Do you know something about it by any chance...?"
    woman "Hmm... Give me a little time, then come back to see me.{w}\nI have some preparations to make..."
    l "O...Ok..."
    "...I knew it, there's something really strange about this woman."
    "But it looks like she knows something about Captain Selene...{w}\nI should visit her again."

    $ hanyo[2] = 1
    return


label city26_04:
    show mob seinen2 with dissolve #700

    ruffian "I used to be part of Ilias Kreuz, but I fell out with them.{w}\nThey got kind of... Ya know..."
    ruffian "Before with Marcellus leading us, his whole mantra and method resonated with me, ya know?"
    ruffian "But now, Lazarus...{w}\nHe's kind of crazy."
    ruffian "He just throws bombs all over the place, whether there are humans there or not.{w}\nMe and a lot of other guys just couldn't deal with that."

    return


label city26_05:
    show mob gorotuki1 with dissolve #700

    hunter "Despite how I look, I'm a famous treasure hunter."
    hunter "Gold Port here was a stronghold for the famous pirate Captain Selene.{w}\nI'm looking for documents or maps with any information about any possible treasure she left behind..."
    hunter "...But as I thought, it isn't easy at all.{w}\nEven her ship's log is a priceless treasure, I can't imagine how much any of her actual treasure would be worth..."

    return


label city26_06:
    show mob sensi1 with dissolve #700

    soldier "The monsters around here are really strong.{w}\nI guess that's to be expected, since we're so close to that dangerous continent."
    soldier "It takes everything I got just to run away when I see a monster."

    return


label city26_07a:
    show mob sensi2 with dissolve #700

    officer "I'm the resident naval officer for Gold Port.{w}\nI got an urgent request to search for someone from a higher up."
    officer "\"Look for the daughter of a noble\" he said.{w}\nDidn't say where from, nor who I'm even looking for."
    officer "Apparently it's a state secret that she's gone, or something.{w}\nHow the hell do they expect me to find someone without any information...?"

    return


label city26_07b:
    show mob sensi2 with dissolve #700

    sabasa_officer "Ah, the height of the one I'm looking for!{w}\nWould you happen to be Sir Luka?"
    l "Th...That's me..."
    "Why was he looking for me...?"
    sabasa_officer "Yes!{w} I found you at last!{w}\nSomething horrible has happened in Sabasa!"
    sabasa_officer "Princess Sara has been kidnapped by monsters!"
    l "Ehh!?"
    "Again, Sara...?{w}\nDid she run away again?"
    l "What happened?"
    sabasa_officer "I was just dispatched to locate either you or Sara.{w}\nI wasn't given any more details from the King than that."
    l "I...I see... Ok..."
    sabasa_officer "Please, save Sara!{w}\nI'll return and report that I made contact with you, Sir Luka!"
    sabasa_officer "...Farewell!"

    play sound "audio/se/asioto1.ogg"
    hide mob with dissolve

    "The officer runs off."
    l "What is she doing this time?{w}\nDid she run away from the castle to chase after Granberia?"

    show alice st14b with dissolve #700

    a "...How stupid, just forget it."
    l "Even if you say that, what if she really was kidnapped by a monster...?{w}\nMaybe I should visit Sabasa to find out more..."
    "I need to hurry to the Monster Lord's castle...{w}\nBut I can't just ignore a request from the Sabasa King."

    $ hanyo[1] = 1
    return


label city26_10:
    scene bg 152 with blinds
    show mob ozisan1 with dissolve #700

    weapon_shop_owner "Welcome!{w}\nI'm the weapon shop closest to the final area, so I naturally have the highest quality goods!"
    l "..............."
    "I take a quick look over the weapons, but nothing is better than what I have.{w}\nThere can't possibly be a better weapon than Angel Halo for me, and I like this Enrikan Shirt."

    scene bg 119 with blinds
    return


label city26_11:
    scene bg 154 with blinds
    show mob ozisan2 with dissolve #700

    shopkeeper "This town is full of treasure hunters, so I deal mostly in buying treasure."
    shopkeeper "So if you got some treasure to sell, bring it here."
    l "I don't have anything at the moment..."

    if item13 == 1:

        shopkeeper "Oh... What about that lamp there?{w}\nCould I buy that?"
        l "Eh...?{w} This?"
        "This is that lamp with the monster in it.{w}\nI've been carrying it around for a while, but..."
        l "This lamp has a monster inside of it.{w}\nIt's too dangerous to let go of..."
        shopkeeper "I know how to handle those types of things.{w}\nWon't you let me buy it?"
        l "Alright then..."

        $ item13 = 0

        "Thus, I sell the lamp to the shopkeeper."
        shopkeeper "Here you are, 3500G!{w}\nI also threw in a lottery ticket."
        l "Ah... Thanks..."

        $ item13 = 2

        "A large amount of money and a Lottery Ticket was obtained!"

    scene bg 119 with blinds
    return


label city26_12:
    show mob musume1 with dissolve #700

    lottery_girl "If you have a ticket...{image=note}{w}\nThis is where you play!{image=note}"

    if item13 != 2:
        "Lottery Ticket...? I don't have one."
    else:
        "Lottery Ticket...? Ah, that's right, I got one from selling that lamp.{w}\nShould I give it a try?"
        l "I have one, I'll give it a try."
        lottery_girl "Cooooming up!"
        "I turn the wheel holding lots of multicolored balls.{w}\nI'm not expecting much...{w} But a golden ball pops out!"
        lottery_girl "Waah!{w} First prize!{w}\nYou won the Jewel of Fortune!"
        l "Eh!?"
        "The girl hands me a beautiful jewel."

        play sound "audio/se/fanfale.ogg"
        $ item13 = 3

        "Jewel of Fortune obtained!"
        l "I...I won something?{w}\nBut what does this Jewel do?"

        hide mob
        show alice st11b
        with dissolve #700

        a "It feels like it was filled with the power of luck at one point, but it's almost all gone.{w}\nIt isn't that useful of an item."

        show alice st14b with dissolve #700

        a "You lost out, Luka.{w} If you got fifth place, you could have gotten that Yamatai rice cracker set..."
        "Yeah, right.{w} Like she would have given me the chance to eat those myself.{w} At least this way I actually get something."

    return


label city26_13:
    scene bg 117 with blinds
    show alice st11b with dissolve #700

    a "Hmm... Is this a church?{w}\nI smell something unpleasant..."
    l "This is Ilias Kreuz's headquarters."
    "The group formed by my father, Lazarus and friends...{w}\nThis is their base."
    a "This isn't a nice place to be.{w}\nDo you have a reason to be here?"
    l "No, not really..."
    "I don't want to be here either.{w}\nWe hurry away."

    scene bg 119 with blinds
    return


label city26_14a:
    scene bg 118 with blinds
    show alice st11b with dissolve #700

    a "You haven't even gotten Salamander's power yet, and you want to go to the next continent?{w}\nAre you suicidal?"
    l "Th...That's right..."
    "I need all four spirits before I could hope to survive there.{w}\nI leave the port."

    scene bg 119 with blinds
    return


label city26_14b:
    scene bg 118 with blinds

    l "Excuse me... Do you know Captain Selene?"

    show mob sentyou as mob2 with dissolve #700

    captain "Hmmm?{w} You after treasure?"
    l "Yes, sort of.{w}\nI want to know where Captain Selene's ship sank..."
    captain "So would all of the treasure hunters of the world.{w}\nIt's still sitting at the bottom of the sea somewhere, waiting for someone to find it."
    l ".............."
    "I get the feeling that this Captain was a treasure hunter once.{w}\nHe seems pretty enthusiastic..."
    captain "But still... There's a rumor going around about how her ship never actually sank."
    captain "Captain Selene's ship is still sailing round the seas, the wood slowly decaying as it journeys on..."

    show mob sentyou at xy(X=-160) #700
    show mob hunanori at xy(X=160) as mob2
    with dissolve #701

    sailor_a "About that... I've actually seen a ghost ship like that."
    "A sailor from another ship pipes into our conversation."
    sailor_a "It was around last year... I had the night watch.{w}\nA ship in tatters was sailing by off in the distance..."
    sailor_a "And flapping off the mast...{w} Was the black flag of Captain Selene!{w}\nNobody believed me at the time, though..."

    show mob sentyou at xy(X=-220) #700
    show mob hunanori at center as mob2 #701
    show mob hunanori at xy(X=220) as mob3
    with dissolve #702

    sailor_b "That crap again?{w} You were half asleep, man."
    sailor_a "Shut up!{w} I saw it!"

    show mob hunanori at xy(X=-220) #700
    show mob hunanori at center as mob2 #701
    show mob hunanori at xy(X=220) as mob3
    with dissolve #702

    sailor_c "Actually, I saw it once too..."
    sailor_d "Ah shut it.{w} You were just freaking out and thought that little fishing boat was more than it was."
    sailor_c "The hell was that, you punk!?{w} How about my fists let you show how \"freaked out\" I was!"
    "More and more sailors start gathering around.{w} Uh oh, I think this might turn into a brawl soon..."
    l "E...Excuse me..."
    sailor_b "I know it exists!{w}\nCaptain Selene's ship is still sailing on the seas!"
    sailor_e "I bet ya Captain Selene was seduced by them Sirens.{w}\nI heard they were in the western sea, where she was heading off to."
    sailor_a "They don't exist, you idiot.{w}\nBesides, someone like Captain Selene would have counter-measures for things like that."
    l "I guess I'll be going now..."
    "The sailors continue to yell at each other about random legendary creatures and ghost ships as I slip away."
    "I did find out one thing, though."
    "Even after all this time, the legend around Captain Selene is enough to get men of the sea fired up."
    "Perhaps I should talk to the people around town some more..."

    $ hanyo[0] = 1

    scene bg 119 with blinds
    return


label city26_14c:
    scene bg 118 with blinds
    show mob sentyou with dissolve #700

    captain "Should I head out to search for treasure...?{w}\nThe sea is calling me..."

    scene bg 119 with blinds
    return


label city26_15a:
    scene bg 092 with blinds
    show mob sinkan at xy(X=220) as mob3
    show mob sinkan as mob2 #701
    show mob sinkan at xy(X=-220) #700
    with dissolve #702

    sage_a "This is a shrine for Heroes!"
    sage_b "And we are the three sages!"
    sage_c "We stay here and give a trial for Heroes!"
    l "Th...This shrine is pretty small..."
    "That reminds me, the San Ilia King mentioned the three sages, and passing their trial or something..."
    "So this is where they are?{w}\nI kind of expected them to be in something more grand... Not this dinky little shrine in a port town."
    sage_a "Oh, Hero!{w} Bring me the Jewel of Victory!"
    sage_b "Then I want the Jewel of Fortune!"
    sage_c "I wish for the Jewel of Friendship!"
    three_sages "Hero, give us expensive jewels!"
    l ".............."
    "Alice already destroyed that sword, so there's no reason for me to bother with them."
    "Thank you, Alice..."

    $ check05 = 1
    scene bg 119 with blinds
    return


label city26_15b:
    scene bg 092 with blinds
    show mob sinkan at xy(X=220) as mob3
    show mob sinkan as mob2 #701
    show mob sinkan at xy(X=-220) #700
    with dissolve #702

    sage_a "Oooohhh!{w} This is it, the Jewel of Victory!"
    sage_b "What great fortune!"
    sage_c "This... This feeling of friendship!{w} It's overflowing!!!"
    l "Uhm... I don't want to give them to you.{w}\nThese are important to me..."
    "That Jewel of Friendship most of all.{w}\nI got it from that Nekomata and Fairy, I would never hand it over to these smelly old men."
    sage_a "You did it!"
    sage_b "I...It's not like we were going to sell them or anything!"
    sage_c "Yes, we are sages, not crooks!{w} Ha. ha. ha."
    three_sages "Yes, you are a true Hero!{w}\nTake this to show all that you have the Three Sage's blessing!"
    "The three sages hold out a huge gold medal."

    play sound "audio/se/fanfale.ogg"
    $ item17 = 1
    $ persistent.yuusyaakasi = 1

    "Hero's Proof obtained!"
    l "Uhm... I'm not a real Hero...{w}\nI'm not baptized, either..."
    sage_a "M...My word!"
    sage_b "How could we have handed the proof of a Hero to someone who wasn't a Hero!?"
    sage_c "I DIDN'T HEAR ANYTHING, DID YOU GUYS!?"
    sage_a "I SAW NOTHING!"
    sage_b "I HEARD NOTHING!"
    sage_c "I SPOKE NOTHING!"
    three_sages "THE THREE SAGES HAVE FORGOTTEN THE LAST THREE MINUTES!"
    l "..............."

    hide mob
    hide mob2
    hide mob3
    show alice st11b
    with dissolve #700

    a "Oho?{w} Was that your faith I heard crumbling?"
    l "That aside...{w} I don't know what to think at the moment.{w}\nI would have leaped for joy to get this proof of a Hero a while ago."
    "I turn over the golden medal in my hand.{w}\nIt's the \"Proof of a Hero\" from the three sages, but what exactly does it prove?"

    show alice st12b with dissolve #700

    a "Hmm... It looks like you're still changing.{w}\nAre you almost done playing Hero?"
    l "...Who knows.{w}\nI'm me, and I'll do what I need to do."
    "Some little medal doesn't prove who I am.{w}\nI leave the shrine with the determination to stay true to myself."

    scene bg 119 with blinds
    return


label city26_15c:
    scene bg 092 with blinds
    show mob sinkan at xy(X=220) as mob3
    show mob sinkan as mob2 #701
    show mob sinkan at xy(X=-220) #700
    with dissolve #702

    sage_a "Oh, Hero!{w} We are the three sages!"
    sage_b "Today's music day, so we brought our flutes and drums to the shrine!"
    sage_c "We're also registered as qualified midwives, so if you know someone giving birth, send them our way."
    l "............."
    "I don't want people like this to show me the path of a Hero.{w}\nThank you, Alice.{w} Thank you so much..."

    scene bg 119 with blinds
    return
